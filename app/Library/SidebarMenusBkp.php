<?php
namespace App\Library;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\{
    MasterComponent,
    MasterModule,
    MasterService,
    MasterMenu
};

class SidebarMenusBkp
{
    public static function getMenus($assignedMenuIds, $isSuperAdmin = false)
    {
        try {    
            $query = new MasterMenu();

            if (!$isSuperAdmin) {
                $query = $query->whereIn('id', $assignedMenuIds);
            }

            $allMenus = $query->select('id',
                        'menu_name',
                        'menu_name_bn',
                        'url',
                        'component_id',
                        'module_id',
                        'service_id',
                        'status'
                        )
                        ->orderBy('module_id', 'asc')
                        ->orderBy('service_id', 'asc')
                        ->orderBy('sorting_order', 'asc')
                        ->get();

            $moduleIds = [];
            $serviceIds = [];
            $componentIds = [];

            foreach ($allMenus as $menu) {
                $componentIds[] = $menu->component_id;
                $moduleIds[] = $menu->module_id;
                $serviceIds[] = $menu->service_id;
            }

            $components = MasterComponent::select(
                'id',
                'component_name',
                'component_name_bn'
                )->whereIn('id', $componentIds)
                ->orderBy('sorting_order', 'asc')
                ->get();

            $modules = MasterModule::select(
                'id',
                'module_name',
                'module_name',
                'module_name_bn',
                'component_id',
            )->whereIn('id', $moduleIds)
            ->orderBy('sorting_order', 'asc')
            ->get();

        $services = MasterService::whereIn('id', $serviceIds)
                        ->select('id', 'service_name', 'service_name_bn', 'component_id', 'module_id', 'status')
                        ->orderBy('sorting_order', 'asc')
                        ->get();

        } catch (\Exception $ex) {
            return [
                'success' => false,
                'data' => [
                    'components' => [],
                    'modules' => [],
                    'services' => [],
                    'menus' => []
                ],
                'message' => "Failed get menu due to server error." . exception_message()
            ];
        }

        return [
            'success' => true,
            'data' => [
                'components' => $components,
                'modules' => $modules,
                'services' => $services,
                'menus' => $allMenus
            ]
        ];
    }

    public function getAssignedMenuIdsByRole()
    {
        
        $baseUrl = config('app.base_url.auth_service');
        $assignedMenus = \App\Library\RestService::getData($baseUrl, "/role/role-menus/{$roleId}");
        $assignedMenus =json_decode($assignedMenus, true);
        
        if (count($assignedMenus) <= 0) {
            return response([
                'success' => false,
                'data' => [
                    'components' => [],
                    'modules' => [],
                    'services' => [],
                    'menus' => []
                ]
            ]);
        }

        if (empty($assignedMenus)) {
            return response([
                'success' => false,
                'data' => [$role_id, 1],
                'message' => "No menu assigned to this role"
            ]);
        }

        $assignedMenuIds = [];

        foreach ($assignedMenus as $key => $value) {
            $assignedMenuIds[] = $value['master_menu_id'];
        }
    }
}