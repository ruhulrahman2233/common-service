<?php
namespace App\Http\Validations\Complain;

use Validator;

class ComplainApproveValidation
{
    /**
     * complain approve validate
     */
    public static function validate ($request)
    {         
        $validator = Validator::make($request->all(), [
            'complain_id'   => 'required',
            'approve_note'  => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}