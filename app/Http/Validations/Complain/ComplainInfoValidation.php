<?php 
namespace App\Http\Validations\Complain;

use Validator;
use Illuminate\Validation\Rule;

class ComplainInfoValidation 
{
    /**
     * Master complain info validate
     */
    public static function validate ($request ,$id=0)
    {         
        $validator = Validator::make($request->all(), [
            'com_title'     => 'required',
            'description'   => 'required',
            'org_id'        => 'required',
            'service_id'    => 'required',
            'division_id'   => 'required',
            'district_id'   => 'required',
            'upazilla_id'   => 'required',
            'union_id'      => 'required',
            'mobile_no'     => 'required|min:11|max:15',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}