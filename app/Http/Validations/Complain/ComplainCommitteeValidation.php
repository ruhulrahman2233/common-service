<?php 
namespace App\Http\Validations\Complain;

use Validator;

class ComplainCommitteeValidation
{
    /**
     * complain committee validate
     */
    public static function validate ($request)
    {         
        $validator = Validator::make($request->all(), [
            'complain_id.*'   => 'required',
            'role_id.*'       => 'required',
            'user_id.*'       => 'required',
            'designation_id.*'=> 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}