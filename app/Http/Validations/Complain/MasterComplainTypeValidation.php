<?php 
namespace App\Http\Validations\Complain;

use Validator;

class MasterComplainTypeValidation
{
    /**
     * Master complain type validate
     */
    public static function validate ($request ,$id=0)
    { 
        $validator = Validator::make($request->all(), [
            'com_type_name' => 'required|unique:master_complain_types,com_type_name,'.$id,
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}