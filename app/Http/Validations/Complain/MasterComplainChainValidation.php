<?php
namespace App\Http\Validations\Complain;

use Validator;
use Illuminate\Validation\Rule;

class MasterComplainChainValidation
{
    /**
     * Master complain designation validate
     */
    public static function validate ($request ,$id=0)
    { 
        $org_id = $request->org_id;
        $validator = Validator::make($request->all(), [
            'org_id' => 'required',
            'sorting_order' => 'required|unique:master_complain_chains,sorting_order,'.$id,
            'designation_id' => [
                'required',
                Rule::unique('master_complain_chains')->where(function ($query) use($org_id ,$id) {
                    $query->where('org_id', $org_id);
                    if ($id) {
                        $query =$query->where('id', '!=', $id);
                    }
                    return $query;             
                }),
            ]
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}