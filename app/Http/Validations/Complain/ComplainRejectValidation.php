<?php
namespace App\Http\Validations\Complain;

use Validator;

class ComplainRejectValidation
{
    /**
     * complain reject validate
     */
    public static function validate ($request)
    {         
        $validator = Validator::make($request->all(), [
            'complain_id'   => 'required',
            'reject_note'  => 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}