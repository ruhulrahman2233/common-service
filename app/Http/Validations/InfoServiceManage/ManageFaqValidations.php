<?php 
namespace App\Http\Validations\InfoServiceManage;

use Validator;
use Illuminate\Validation\Rule;

class ManageFaqValidations
{
    /**
     * Farmers Ratings validate
     */
    // public static function validate ($request , $id = 0)
    // { 
    //     $componentId = $request->component_id;
    //     $serviceId =  $request->service_id;
    //     $validator = Validator::make($request->all(), [
    //         'question' => [
    //                         'required',
    //                         Rule::unique('master_faqs')->where(function ($query) use($id,  $componentId, $serviceId) {
	//                             $query =$query->where('component_id', $componentId) 
    //                                           ->where('service_id', $serviceId);   
    //                             if ($id) {
	//                                     $query =$query->where('id', '!=' ,$id);
	//                                 }
    //                             	return $query;             
    //                         }),
    //                     ],
                        
    //         'answer' => [
    //                         'required',
    //                         Rule::unique('master_faqs')->where(function ($query) use($id,  $componentId, $serviceId) {
    //                             $query =$query->where('component_id', $componentId) 
    //                                            ->where('service_id', $serviceId); 
	//                                 if ($id) {
	//                                     $query =$query->where('id', '!=' ,$id);
	//                                 }
    //                             	return $query;             
    //                         }),
    //                     ] 
    //     ]);

    //     if ($validator->fails()) {
    //         return ([
    //             'success' => false,
    //             'errors' => $validator->errors()
    //         ]);
    //     }

    //     return ['success'=> 'true'];
    // }
    public static function validate ($request , $id = 0)
    { 
        $validator = Validator::make($request->all(), [
            'component_id' 	               => 'required',
            'service_id' 	               => 'required',
        ]);
    
        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
    
        return ['success'=> 'true'];
    }
    
}