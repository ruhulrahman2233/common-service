<?php 
namespace App\Http\Validations\InfoServiceManage;

use Validator;
use Illuminate\Validation\Rule;

class ContentEntryValidations
{
    /**
     * Farmers Ratings validate
     */
    public static function validate ($request , $id = 0)
    { 
        $validator = Validator::make($request->all(), [
            'component_id' 	               => 'required',
            'service_id' 	               => 'required',
            'description' 	               => 'required',	
            'eligibility_criteria_id'      => 'required'  
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}