<?php 
namespace App\Http\Validations\InfoServiceManage;

use Validator;
use Illuminate\Validation\Rule;

class ServiceEligibilityTypeEntryValidations
{
    /**
     * Farmers Ratings validate
     */
    public static function validate ($request , $id = 0)
    { 
        $validator = Validator::make($request->all(), [
            'type_name' => [
                            'required',
                            Rule::unique('master_eligibility_types')->where(function ($query) use($id) {
	                                if ($id) {
	                                    $query =$query->where('id', '!=' ,$id);
	                                }
                                	return $query;             
                            }),
                        ] 
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}