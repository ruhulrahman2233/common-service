<?php
namespace App\Http\Validations\Pauroshoba;

use Validator;

class MasterPauroshobasValidations
{
    /**
     * MasterNotificationValidator;
    */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'pauroshoba_name'     	=>'required|unique:master_pauroshobas,pauroshoba_name,'.$id,
            'pauroshoba_name_bn'    =>'required',
            'division_id'     		=>'required',
            'district_id'     		=>'required',
            'upazilla_id'     		=>'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];

    }
}

