<?php
namespace app\Http\Validations\Notification;
use Illuminate\Validation\Rule;

use Validator;

class MasterNotificationSettingValidation
{
    /**
     * Master Notification Validation
     */
    public static function  validate($request, $id = 0)
    {
        $component_id  = $request->component_id;
        $module_id     = $request->module_id;
        $menu_id       = $request->menu_id;
        $notification_type_id   = $request->notification_type_id;

        $validator = Validator::make($request->all(), [
            'component_id' => [
              'required',
              Rule::unique('master_notification_settings')->where(function ($query) use($notification_type_id, $component_id, $module_id, $menu_id, $id) {
                  $subQuery = $query->where('component_id', $component_id)
                               ->where('notification_type_id', $notification_type_id)
                               ->where('module_id', $module_id)
                               ->where('menu_id', $menu_id);
                  if ($id) {
                    $subQuery = $subQuery->where('id','!=',$id);
                  }

                  return $subQuery;
              }),
          ],
            'notification_type_id' =>'required',
            'module_id'            =>'required',
            'menu_id'              =>'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'errors' => $validator->errors()
            ];
        }
        return ['success' => true];
    }
        
}
