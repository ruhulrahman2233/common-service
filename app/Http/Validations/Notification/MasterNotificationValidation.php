<?php
namespace app\Http\Validations\Notification;

use Validator;

class MasterNotificationValidation
{
    /**
     * MasterNotificationValidator;
    */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'not_type_name'     =>'required|unique:master_notification_types,not_type_name,'.$id,
            'not_type_name_bn'     =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];

    }
}