<?php
namespace app\Http\Validations\Notification;
use Illuminate\Validation\Rule;

use Validator;

class NotNotificationTemplateValidation
{

    /**
     *Not Notification Template Validation
     */
    public static function   validate ($request, $id =0)
    {
        $notification_type_id = $request->notification_type_id;
        $title                = $request->title;
        $component_id         = $request->component_id;
        $module_id            = $request->module_id;
        $menu_id              = $request->menu_id;

        $validator = Validator::make($request->all(), [
           'notification_type_id' =>'required',
           'component_id'         =>'required',
           'module_id'            =>'required',
           'menu_id'              =>'required',
            'title' => [
              'required',
              Rule::unique('not_notification_templates')->where(function ($query) use($title, $notification_type_id, $component_id, $module_id, $menu_id, $id) {
                  $subQuery = $query->where('title', $title)
                               ->where('notification_type_id', $notification_type_id)
                               ->where('component_id', $component_id)
                               ->where('module_id', $module_id)
                               ->where('menu_id', $menu_id);
                  if ($id) {
                    $subQuery = $subQuery->where('id','!=',$id);
                  }

                  return $subQuery;
              }),
          ],
          'title_bn'        => 'required',
          'message'         =>'required',
          'message_bn'      =>'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'errors' => $validator->errors()
            ];
        }
        return ['success' => true];
    }
}
