<?php
namespace app\Http\Validations\Notification;

use Validator;

class NotCirculateNoticeValidation 
{

    /**
     *Not Notification Template Validation 
     */
    public static function   validate ($request)
    {
        $validator = Validator::make($request->all(), [
            'notice_title'     			=>'required',
            'description'               =>'required',
            'notice_for'                =>'required',
            'org_id'                   	=>'required',
            'notification_type_id'      =>'required',                  
            'notice_date'      			=>'required',  
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}


