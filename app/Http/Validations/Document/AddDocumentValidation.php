<?php
namespace app\Http\Validations\Document;

use Validator;

class AddDocumentValidation
{
    /**
     * Add Document Validation 
     */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'doc_title'         =>'required|unique:doc_doc_infos,doc_title,'.$id,
            'doc_title_bn'      =>'required',
            'attachment'        =>'required',
            'org_id'            =>'required',
            'category_id'       =>'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }

}
