<?php
namespace App\Http\Validations\Document;

use \Validator;


class MasterDocumentCategoryValidation 
{
    /**
     * Master Document Category Validation
     */
    public static function validate ($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'category_name'     =>'required|unique:master_document_categories,category_name,'.$id,
            'sorting_order'     =>'required|unique:master_document_categories,sorting_order,'.$id,
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];


    }
}