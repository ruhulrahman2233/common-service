<?php 
 
namespace App\Http\Validations\OrgProfile;

use Validator;

class  MasterFiscalValidation 
{
    /**
     * Master Country
     */
    public static function validate($request ,$id =0)
    {
        $validator = Validator::make($request->all(), [
            'year_name' => 'required|unique:master_fiscal_year,year_name,'.$id,
            'year_name_bn' => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
        return ['success' => true];

    }
} 