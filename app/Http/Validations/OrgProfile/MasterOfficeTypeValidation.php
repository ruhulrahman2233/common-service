<?php

namespace app\Http\Validations\OrgProfile;

use Illuminate\Validation\Rule;
use Validator;

class MasterOfficeTypeValidation
{
    /**
     * master office type validation
     */
    // public static function validate($request, $id = 0)
    // {
        
    //     $org_id        = $request->org_id;
    //     $office_type_name    = $request->office_type_name;
    //     $validator = Validator::make($request->all(), [
    //     'office_type_name' => [
    //         'required',
    //         Rule::unique('master_office_types')->where(function ($query) use($office_type_name, $org_id , $id) {
    //             $subQuery = $query->where('office_type_name', $office_type_name)
    //                                ->where('org_id', $org_id);
    //             if ($id) {
    //                 $subQuery = $subQuery->where('id','!=',$id);
    //             }

    //             return $subQuery;
    //         }),
    //     ],
    //     'office_type_name_bn'  => 'required',
    //     'org_id'               => 'required',
    //     ]);

    //     if ($validator->fails()) {
    //         return ([
    //             'success' => false,
    //             'errors'  => $validator->errors()
    //         ]);
    //     }
    //     return ['success'=>'true'];
    // }
    public static function validate ($request , $id = 0)
    { 
        $org_id   = $request->org_id;
        $office_type_name    = $request->office_type_name;

        $validator = Validator::make($request->all(), [
            'office_type_name' => [
                'required',
                Rule::unique('master_office_types')->where(function ($query) use ($office_type_name, $org_id, $id) {
                    $query->where('office_type_name', $office_type_name)
                            ->where('org_id', $org_id);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],
            "office_type_name_bn" =>'required',
            'org_id' => 'required'

        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}
