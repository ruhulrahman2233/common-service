<?php

namespace app\Http\Validations\OrgProfile;

use Illuminate\Validation\Rule;
use Validator;

class  MasterDivisionValidation 
{
    /**
     * division validation
     */
    public static function validate($request ,$id =0)
    {
        $validator = Validator::make($request->all(), [
            'division_name'     => 'required|unique:master_divisions,division_name,'.$id,
            'division_name_bn'  => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
        return ['success'=>true]; 
        
        

    }
}