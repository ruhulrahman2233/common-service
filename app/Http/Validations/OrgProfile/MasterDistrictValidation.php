<?php

namespace App\Http\Validations\OrgProfile;

use Illuminate\Validation\Rule;
use Validator;

class MasterDistrictValidation 
{
    /**
     * Master district validate
     */
    public static function validate ($request ,$id=0)
    {
        $district_name = $request->district_name;
        $division_id   = $request->division_id;
        $validator = Validator::make($request->all(), [
            'district_name' => [
                'required',
                Rule::unique('master_districts')->where(function ($query) use($district_name, $division_id ,$id) {
                    $query->where('district_name', $district_name)
                                 ->where('division_id', $division_id);
                    if ($id) {
                        $query =$query->where('id', '!=' ,$id);
                    }
                    return $query;             
                }),
            ],
            'district_name_bn'  => 'required',
            'division_id'       => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
        return ['success'=> 'true'];

    }
}