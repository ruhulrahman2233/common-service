<?php

namespace App\Http\Validations\OrgProfile;

use Illuminate\Validation\Rule;
use Validator;

class  MasterUpazilaValidation
{
   /**
   * Upazilla Validation
   */
   public static function validate($request, $id = 0)
   {

    $upazilla_name = $request->upazilla_name;
    $district_id    = $request->district_id;

    $validator = Validator::make($request->all(), [
        'upazilla_name' => [
            'required',
            Rule::unique('master_upazillas')->where(function ($query) use($upazilla_name, $district_id ,$id) {
                $query->where('upazilla_name', $upazilla_name)
                             ->where('district_id', $district_id);
                if ($id) {
                $query = $query->where('id','!=',$id);
                }
                return $query;             
            }),
        ],
        'upazilla_name_bn'  => 'required',
        'district_id'        => 'required',
    ]);

    if ($validator->fails()) {
        return [
            'success' => false,
            'errors'  => $validator->errors()
        ];
    }
    return ['success' => true];
   }


}




