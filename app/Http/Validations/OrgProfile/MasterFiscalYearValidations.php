<?php
namespace app\Http\Validations\OrgProfile;

use Validator;



class MasterFiscalYearValidations
{
    /**
     * Master Fiscal Year Validation
    */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'year' => 'required',
            'start_date' => 'required',
            'end_date'  => 'required',
            'sorting_order'  => 'nullable|integer',
        ]);

        if ($validator->fails()) {
            return([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        return ['success'=>true];
    }
}
