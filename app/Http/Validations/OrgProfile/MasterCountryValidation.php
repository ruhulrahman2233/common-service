<?php 
 
namespace App\Http\Validations\OrgProfile;

use Validator;

class  MasterCountryValidation 
{
    /**
     * Master Country
     */
    public static function validate($request ,$id =0)
    {
        $validator = Validator::make($request->all(), [
            'country_name' => 'required|unique:master_countries,country_name,'.$id,
            'country_name_bn' => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
        return ['success' => true];

    }
} 