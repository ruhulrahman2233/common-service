<?php
namespace App\Http\Validations\OrgProfile;

use Illuminate\Validation\Rule;
use Validator;


class MasterUnionValidation
{
  /**
   * Master Union Validator
   */
  public static function validate($request, $id = 0)
  {
    $union_name = $request->union_name;
    $upazilla_id   = $request->upazilla_id;
    
    $validator = Validator::make($request->all(), [
      'union_name' => [
          'required',
          Rule::unique('master_unions')->where(function ($query) use($union_name, $upazilla_id , $id) {
              $subQuery = $query->where('union_name', $union_name)
                           ->where('upazilla_id', $upazilla_id);
              if ($id) {
                $subQuery = $subQuery->where('id','!=',$id);
              }

              return $subQuery;
          }),
      ],
      'union_name_bn'  => 'required',
      'upazilla_id'    =>'required',
    ]);

    if ($validator->fails()) {
        return [
            'success' => false,
            'errors' => $validator->errors()
        ];
    }
    return ['success' => true];
  }
}