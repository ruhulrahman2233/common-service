<?php
namespace App\Http\Validations\OrgProfile;

use Illuminate\Validation\Rule;
use Validator;

class MasterBranchValidation
{
    /**
     * Master Branch Validation
    */
    public static function validate($request, $id =0)
    {
        $branch_name    = $request->branch_name;
        $bank_id        = $request->bank_id;   
        $validator = Validator::make($request->all(), [
        'branch_name' => [
            'required',
            Rule::unique('master_branchs')->where(function ($query) use($branch_name, $bank_id , $id) {
                $subQuery = $query->where('branch_name', $branch_name)
                            ->where('bank_id', $bank_id);
                if ($id) {
                    $subQuery = $subQuery->where('id','!=',$id);
                }

                return $subQuery;
            }),
        ],
        'branch_name_bn'  => 'required',
        'address'         => 'required',
        'address_bn'      => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'errors' => $validator->errors()
            ];
        }
        return ['success' => true];

    }
    
}