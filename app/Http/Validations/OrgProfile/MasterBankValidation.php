<?php
namespace App\Http\Validations\OrgProfile;

use Validator;

class  MasterBankValidation
{
    /**
    * Master Bank Validation
    */
    public static function  validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'bank_name'     =>'required|unique:master_banks,bank_name,'.$id,
            'bank_name_bn'  => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
    
}