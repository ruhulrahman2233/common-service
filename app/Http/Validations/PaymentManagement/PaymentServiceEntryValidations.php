<?php
namespace App\Http\Validations\PaymentManagement;

use Validator;

class PaymentServiceEntryValidations
{
    /**
     * complain approve validate
     */
    public static function validate ($request)
    {         
        $validator = Validator::make($request->all(), [
            'org_id'   		=> 'required',
            'component_id'  => 'required',
            'module_id'  	=> 'required',
            'service_id'  	=> 'required',
            'amount'  		=> 'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}