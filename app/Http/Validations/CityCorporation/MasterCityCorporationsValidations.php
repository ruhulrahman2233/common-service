<?php
namespace App\Http\Validations\CityCorporation;

use Validator;

class MasterCityCorporationsValidations
{
    /**
     * MasterCityCorporationsValidations;
    */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'city_corporation_name'    =>'required|unique:master_city_corporations,city_corporation_name,'.$id,
            'city_corporation_name_bn' =>'required',
            'division_id'     		   =>'required',
            'district_id'     		   =>'required'
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];

    }
}

