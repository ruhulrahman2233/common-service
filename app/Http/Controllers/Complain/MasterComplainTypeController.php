<?php

namespace App\Http\Controllers\Complain;

use App\Http\Controllers\Controller;
use App\Http\Validations\Complain\MasterComplainTypeValidation;
use App\Models\Complain\MasterComplainType;
use Illuminate\Http\Request;
use DB;

class MasterComplainTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master complain type
     */
    public function index(Request $request)
    {
        $query = MasterComplainType::select("*");

        if ($request->com_type_name) {
            $query = $query->where('com_type_name', 'like', "{$request->com_type_name}%")
                            ->orWhere('com_type_name_bn', 'like', "{$request->com_type_name}%");
        } 

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('com_type_name', 'ASC')->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Complain list',
            'data' => $list
        ]);
    }

    /**
     * master complain type store
     */
    public function store(Request $request)
    {
        $validationResult = MasterComplainTypeValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $mct = new MasterComplainType();
            $mct->com_type_name     = $request->com_type_name;
            $mct->com_type_name_bn  = $request->com_type_name_bn;
            $mct->status            = (int)$request->status;
            $mct->created_by        = (int)user_id();
            $mct->updated_by        = (int)user_id();
            $mct->save();

            save_log([
                'data_id'    => $mct->id,
                'table_name' => 'master_complain_types'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $mct
        ]);
    }

    /**
     * master complain type update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterComplainTypeValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $mct = MasterComplainType::find($id);

        if (!$mct) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $mct->com_type_name    = $request->com_type_name;
            $mct->com_type_name_bn = $request->com_type_name_bn;
            $mct->status           = (int)$request->status;
            $mct->updated_by       = (int)user_id();
            $mct->update();

            save_log([
                'data_id'       => $mct->id,
                'table_name'    => 'master_complain_types',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $mct
        ]);
    }

    /**
     * master complain type status update
     */
    public function toggleStatus($id)
    {
        $mct = MasterComplainType::find($id);

        if (!$mct) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mct->status = $mct->status ? 0 : 1;
        $mct->update();

        save_log([
            'data_id'       => $mct->id,
            'table_name'    => 'master_complain_types',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mct
        ]);
    }

    /**
     * master complain type destroy
     */
    public function destroy($id)
    {
        $mct = MasterComplainType::find($id);

        if (!$mct) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mct->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_complain_types',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
