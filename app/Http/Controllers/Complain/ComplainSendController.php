<?php

namespace App\Http\Controllers\Complain;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Complain\ComplainSend;
use App\Http\Validations\Complain\ComplainSendValidation;

class ComplainSendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master complain type
     */
    public function index(Request $request)
    {  
        $query = DB::table('com_complain_cycle')                        
                        ->join('com_complain_infos','com_complain_cycle.complain_id', '=','com_complain_infos.id')
                        ->join('master_services','com_complain_infos.service_id', '=','master_services.id')
                        ->select('com_complain_cycle.*',
                            'com_complain_infos.service_id',
                            'master_services.service_name', 'master_services.service_name_bn',
                            'com_complain_infos.com_title','com_complain_infos.com_title_bn',
                            'com_complain_infos.description','com_complain_infos.description_bn',
                            'com_complain_infos.status as complain_status');
        // $query = DB::table('com_complain_cycle')                        
        //                 ->join('com_complain_infos','com_complain_cycle.complain_id', '=','com_complain_infos.id')
        //                 ->join('master_designations','com_complain_cycle.receiver_id', '=','master_designations.id')
        //                 ->select('com_complain_cycle.*',
        //                     'com_complain_infos.com_title','com_complain_infos.com_title_bn',
        //                     'com_complain_infos.description','com_complain_infos.description_bn',
        //                     'com_complain_infos.status as complain_status',
        //                     'master_designations.designation as receiver_designation','master_designations.designation_bn as receiver_designation_bn');
        
        if ($request->com_title) {
            $query = $query->where('com_complain_infos.com_title', 'like', "{$request->com_title}%")
                            ->orWhere('com_complain_infos.com_title_bn', 'like', "{$request->com_title}%");
        }
        if ($request->note) {
            $query = $query->where('com_complain_cycle.note', 'like', "{$request->note}%")
                            ->orWhere('com_complain_cycle.note_bn', 'like', "{$request->note}%");
        }

        if ($request->complain_id) {
            $query = $query->where('com_complain_cycle.complain_id', $request->complain_id);
        }

        if ($request->receiver_id) {
            $query = $query->where('com_complain_cycle.receiver_id', $request->receiver_id);
        }

        if ($request->sender_id) {
            $query = $query->where('com_complain_cycle.sender_id', $request->sender_id);
        }

        if ($request->status) {
            $query = $query->where('com_complain_cycle.status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Complain Info list',
            'data' => $list
        ]);
    }

    /**
     * get all master complain type
     */
    public function detailList($complain_id)
    {      
        $query = DB::table('com_complain_infos')
                        ->leftJoin('com_complain_cycle','com_complain_infos.id', '=','com_complain_cycle.complain_id')                        
                        ->leftJoin('master_services','com_complain_infos.service_id', '=','master_services.id')
                        ->select('com_complain_cycle.*',
                            'com_complain_infos.service_id',
                            'master_services.service_name', 'master_services.service_name_bn',
                            'com_complain_infos.com_title','com_complain_infos.com_title_bn',
                            'com_complain_infos.description','com_complain_infos.description_bn',
                            'com_complain_infos.status as complain_status')
                        ->where('com_complain_infos.id',$complain_id)
                        ->get(); 
     
        return response([
            'success' => true,
            'message' => 'Complain Info list',
            'data' => $query
        ]);
    }

    /**
     * master complain type store
     */
    public function store(Request $request)
    {
        DB::table('com_complain_infos')
        ->where('id', '=', $request->complain_id)
        ->update(['status' =>  6]);

        $validationResult = ComplainSendValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $compl_send = new ComplainSend();
            $compl_send->complain_id      = $request->complain_id;
            $compl_send->sender_id   = $request->sender_id;
            $compl_send->receiver_id    = $request->receiver_id;
            $compl_send->note = $request->note;
            $compl_send->note_bn         = $request->note_bn;
            $compl_send->created_by     = (int)user_id();
            $compl_send->updated_by     = (int)user_id();
            $compl_send->save();

            save_log([
                'data_id'    => $compl_send->id,
                'table_name' => 'com_complain_cycle'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $compl_send
        ]);
    }

    /**
     * master complain type update
     */
    public function update(Request $request, $id)
    {
        $validationResult = ComplainSendValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $compl_send = ComplainSend::find($id);

        if (!$compl_send) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $compl_send->complain_id      = $request->complain_id;
            $compl_send->sender_id   = $request->sender_id;
            $compl_send->receiver_id    = $request->receiver_id;
            $compl_send->note = $request->note;
            $compl_send->note_bn         = $request->note_bn;
            $compl_send->updated_by     = (int)user_id();
            $compl_send->update();

            save_log([
                'data_id'       => $compl_send->id,
                'table_name'    => 'com_complain_cycle',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $compl_send
        ]);
    }

    /**
     * master complain type status update
     */
    public function toggleStatus($id)
    {
        $compl_send = ComplainSend::find($id);

        if (!$compl_send) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $compl_send->status = $compl_send->status ? 0 : 1;
        $compl_send->update();

        save_log([
            'data_id'       => $compl_send->id,
            'table_name'    => 'com_complain_cycle',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $compl_send
        ]);
    }

    /**
     * master complain type destroy
     */
    public function destroy($id)
    {
        $compl_send = ComplainSend::find($id);

        if (!$compl_send) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $compl_send->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'com_complain_cycle',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}