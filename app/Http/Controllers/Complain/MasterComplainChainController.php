<?php

namespace App\Http\Controllers\Complain;

use App\Http\Controllers\Controller;
use App\Http\Validations\Complain\MasterComplainChainValidation;
use Illuminate\Http\Request;
use App\Models\Complain\MasterComplainChain;
use DB;

class MasterComplainChainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master complain chain
     */
    public function index(Request $request)
    { 
        $query = DB::table('master_complain_chains')                                                
                        ->leftjoin('master_org_profiless','master_complain_chains.org_id', '=','master_org_profiless.id')                      
                        ->leftjoin('master_designations','master_complain_chains.designation_id', '=','master_designations.id')
                        ->select('master_complain_chains.*',
                            'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                            'master_designations.designation','master_designations.designation_bn');
        
        if ($request->org_id) {
            $query = $query->where('master_complain_chains.org_id', $request->org_id);
        }

        if ($request->designation_id) {
            $query = $query->where('master_complain_chains.designation_id', $request->designation_id);
        }

        if ($request->status) {
            $query = $query->where('master_complain_chains.status', $request->status);
        }

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_designations.designation', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Complain Info list',
            'data' => $list
        ]);
    }

    /**
     * master complain chain store
     */
    public function store(Request $request)
    {
        $validationResult = MasterComplainChainValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $compl_chain = new MasterComplainChain();
            $compl_chain->org_id         = (int)$request->org_id;
            $compl_chain->designation_id = (int)$request->designation_id;
            $compl_chain->sorting_order  = $request->sorting_order;
            $compl_chain->created_by     = (int)user_id();
            $compl_chain->updated_by     = (int)user_id();
            $compl_chain->save();

            save_log([
                'data_id'    => $compl_chain->id,
                'table_name' => 'master_complain_chains'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $compl_chain
        ]);
    }

    /**
     * master complain chain update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterComplainChainValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $compl_chain = MasterComplainChain::find($id);

        if (!$compl_chain) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $compl_chain->org_id         = (int)$request->org_id;
            $compl_chain->designation_id = (int)$request->designation_id;
            $compl_chain->sorting_order  = $request->sorting_order;
            $compl_chain->updated_by     = (int)user_id();
            $compl_chain->update();

            save_log([
                'data_id'       => $compl_chain->id,
                'table_name'    => 'master_complain_chains',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $compl_chain
        ]);
    }

    /**
     * master complain chain status update
     */
    public function toggleStatus($id)
    {
        $compl_chain = MasterComplainChain::find($id);

        if (!$compl_chain) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $compl_chain->status = $compl_chain->status ? 0 : 1;
        $compl_chain->update();

        save_log([
            'data_id'       => $compl_chain->id,
            'table_name'    => 'master_complain_chains',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $compl_chain
        ]);
    }

    /**
     * master complain chain destroy
     */
    public function destroy($id)
    {
        $compl_chain = MasterComplainChain::find($id);

        if (!$compl_chain) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $compl_chain->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_complain_chains',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
