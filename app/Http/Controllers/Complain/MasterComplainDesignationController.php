<?php

namespace App\Http\Controllers\Complain;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Complain\MasterComplainDesignation;
use App\Http\Validations\Complain\MasterComplainDesignationValidation;

class MasterComplainDesignationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master complain type
     */
    public function index(Request $request)
    {
        $query = DB::table('master_complain_designations')                           
                        ->leftjoin('master_org_profiless','master_complain_designations.org_id', '=','master_org_profiless.id')
                        ->leftjoin('master_designations','master_complain_designations.designation_id', '=','master_designations.id')
                        ->select('master_complain_designations.*',
                            'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                            'master_designations.designation','master_designations.designation_bn'
                        );

        if ($request->org_id) {
            $query = $query->where('master_complain_designations.org_id', $request->org_id);
        }

        if ($request->designation_id) {
            $query = $query->where('master_complain_designations.designation_id', $request->designation_id);
        }

        if ($request->status) {
            $query = $query->where('master_complain_designations.status', $request->status);
        }

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_designations.designation', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Master Complain Designation list',
            'data' => $list
        ]);
    }

    /**
     * master complain type store
     */
    public function store(Request $request)
    {
        $validationResult = MasterComplainDesignationValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $mcd = new MasterComplainDesignation();
            $mcd->org_id        = $request->org_id;
            $mcd->designation_id= $request->designation_id;
            $mcd->status        = (int)$request->status;
            $mcd->created_by    = (int)user_id();
            $mcd->updated_by    = (int)user_id();
            $mcd->save();

            save_log([
                'data_id'    => $mcd->id,
                'table_name' => 'master_complain_designations'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $mcd
        ]);
    }

    /**
     * master complain type update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterComplainDesignationValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $mcd = MasterComplainDesignation::find($id);

        if (!$mcd) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $mcd->org_id        = $request->org_id;
            $mcd->designation_id= $request->designation_id;
            $mcd->status        = (int)$request->status;
            $mcd->updated_by    = (int)user_id();
            $mcd->update();

            save_log([
                'data_id'       => $mcd->id,
                'table_name'    => 'master_complain_designations',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $mcd
        ]);
    }

    /**
     * master complain type status update
     */
    public function toggleStatus($id)
    {
        $mcd = MasterComplainDesignation::find($id);

        if (!$mcd) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mcd->status = $mcd->status ? 0 : 1;
        $mcd->update();

        save_log([
            'data_id'       => $mcd->id,
            'table_name'    => 'master_complain_designations',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mcd
        ]);
    }

    /**
     * master complain type destroy
     */
    public function destroy($id)
    {
        $mcd = MasterComplainDesignation::find($id);

        if (!$mcd) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mcd->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_complain_designations',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
