<?php

namespace App\Http\Controllers\OrgProfile;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterBranch;
use App\Http\Validations\OrgProfile\MasterBranchValidation;

class MasterBranchController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  master Branch
     */
    public function index(Request $request)
    {
        $query = DB::table('master_branchs')
                    ->join('master_banks','master_branchs.bank_id', '=','master_banks.id')
                    ->select('master_branchs.*',
                            'master_banks.id as bank_id','master_banks.bank_name','master_banks.bank_name_bn', 'master_banks.org_id',);

        if ($request->branch_name) {
            $query = $query->where('branch_name', 'like', "{$request->branch_name}%")
                            ->orWhere('branch_name_bn', 'like', "{$request->branch_name}%");
        } 

        if ($request->address) {
            $query = $query->where('address', 'like', "{$request->address}%")
                            ->orWhere('address_bn', 'like', "{$request->address}%");
        } 

        if ($request->bank_id) {
            $query = $query->where('master_branchs.bank_id', $request->bank_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('master_banks.bank_name', 'ASC')
                        ->orderBy('master_branchs.branch_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Branch list',
            'data' => $list
        ]);
    }

    /**
     * Master Branch store
     */
    public function store(Request $request)
    {
        $validationResult = MasterBranchValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $branch                   = new MasterBranch();
            $branch->branch_name      = $request->branch_name;
            $branch->branch_name_bn   = $request->branch_name_bn;
            $branch->address          = $request->address;
            $branch->address_bn       = $request->address_bn;
            $branch->bank_id          = $request->bank_id;
            $branch->created_by       = (int)user_id();
            $branch->updated_by       = (int)user_id();
            $branch->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id'    => $branch->id,
                'table_name' => 'master_branchs',
            ]);
            
        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $branch
        ]);
    }

    /**
     * Master Branch update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterBranchValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $branch = MasterBranch::find($id);

        if (!$branch) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $branch->branch_name      = $request->branch_name;
            $branch->branch_name_bn   = $request->branch_name_bn;
            $branch->address          = $request->address;
            $branch->address_bn       = $request->address_bn;
            $branch->bank_id          = $request->bank_id;
            $branch->updated_by      = (int)user_id();
            $branch->update();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $branch->id,
                'table_name' => 'master_branchs',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $branch
        ]);
    }

    /**
     * Master Branch status update
     */
    public function toggleStatus($id)
    {
        $branch = MasterBranch::find($id);

        if (!$branch) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $branch->status = $branch->status ? 0 : 1;
        $branch->update();

        save_log([
            'data_id' => $branch->id,
            'table_name' => 'master_branchs',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $branch
        ]);
    }

    /**
     * Master Branch destroy
     */
    public function destroy($id)
    {
        $branch = MasterBranch::find($id);

        if (!$branch) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $branch->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_branchs',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
