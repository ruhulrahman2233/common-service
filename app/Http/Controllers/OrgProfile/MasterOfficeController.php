<?php

namespace App\Http\Controllers\OrgProfile;

use App\Models\OrgProfile\MasterOfficeType;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterOffice;
use App\Models\Organogram\AssignDesignation;
use App\Models\OrgProfile\MasterDivision;
use App\Models\OrgProfile\MasterDistrict;
use App\Http\Validations\OrgProfile\MasterOfficeValidation;

class MasterOfficeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master office
     */
    public function organogram(Request $request)
    {
        $query= MasterOffice::
                select([
                    'id',
                    'office_name',
                    'office_name_bn',
                    'org_id',
                    'parent_office_id',
                    'parent_office_type_id'
                ])
                ->where('parent_office_id',0);
        if ($request->org_id ) {
            $query = $query->where('org_id', $request->org_id);
        }
        // $query = $query->with(['allChildrenOffice' => function ($query1) use ($request) {
        //     $query1->select([
        //         'id',
        //         'office_name',
        //         'office_name_bn',
        //         'org_id',
        //         'parent_office_id',
        //         'parent_office_type_id'
        //     ]);
        //     if ($request->org_id ) {
        //         $query1 = $query1->where('org_id', $request->org_id);
        //     }
        // }]);
        $query = $query->with(['allChildrenOffice']);
        $masterOffice= $query->orderBy('office_name', 'asc')->get();
        return response([
            'success' => true,
            'message' => 'Master Office list',
            'data' =>$masterOffice
        ]);
    }
    public function officeWisePeople(Request $request)
    {
        $data = $request->all();
        $baseUrl = config('app.base_url.auth_service');
        $users = \App\Library\RestService::getData($baseUrl,'/user/get-designations-wise-people');
        $users =json_decode($users, true);
        $AssignDesignation = AssignDesignation::select(['id','office_id','org_id','designation_id'])->where('office_id', $data['office_id'])->with(['designation:id,org_id,designation,designation_bn'])->get()->each(function ($designation) use ($request, $users) {
            $total= 0;
            if(!empty($users)){
                foreach($users as $key=>$value){
                    if($designation->designation_id == $value['designation_id']) {
                        $total = $value['total'];
                    }
                }
            }
            $designation->total = $total;
            return $designation;
        });

        return response([
            'success' => true,
            'message' => 'Org Wise People',
            'data' =>$AssignDesignation
        ]);
    }
    public function index(Request $request)
    {
        $query = DB::table('master_offices')
                    ->leftJoin('master_org_profiless','master_offices.org_id','=','master_org_profiless.id')
                    ->join('master_office_types','master_offices.office_type_id','=','master_office_types.id')
                    ->join('master_divisions','master_offices.division_id','=','master_divisions.id')
                    ->join('master_districts','master_offices.district_id','=','master_districts.id')
                    ->leftJoin('master_ward_details','master_offices.ward_id','=','master_ward_details.id')
                    ->select("master_offices.*",
                        'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                        'master_office_types.office_type_name','master_office_types.office_type_name_bn',
                        'master_divisions.division_name','master_divisions.division_name_bn',
                        'master_districts.district_name','master_districts.district_name_bn',
                        'master_ward_details.ward_name','master_ward_details.ward_name_bn',
                    )
                    ->orderBy('master_offices.id','DESC');

        if ($request->office_name) {
            $query = $query->where('master_offices.office_name', 'like', "{$request->office_name}%")
                            ->orwhere('master_offices.office_name_bn', 'like', "{$request->office_name}%");
        }

        if ($request->office_code) {
            $query = $query->where('master_offices.office_code', 'like', "{$request->office_code}%");
        }

        if ($request->org_id) {
            $query = $query->where('master_offices.org_id', $request->org_id);
        }

        if ($request->area_type_id) {
            $query = $query->where('master_offices.area_type_id', $request->area_type_id);
        }

        if ($request->office_type_id) {
            $query = $query->where('master_offices.office_type_id', $request->office_type_id);
        }

        if ($request->division_id) {
            $query = $query->where('master_offices.division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query = $query->where('master_offices.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('master_offices.upazilla_id', $request->upazilla_id);
        }

        if ($request->union_id) {
            $query = $query->where('master_offices.union_id', $request->union_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Master Office list',
            'data' => $list
        ]);
    }
    public function regionWiseDivisionOrDistrict(Request $request, $type)
    {
        if($type == 'district') {
            $query = MasterDistrict::select(['id','district_name','district_name_bn'])->oldest('district_name');
        } else if ($type == 'division') {
            $query = MasterDivision::select(['id','division_name','division_name_bn'])->oldest('division_name');
        }
        if ($request->id) {
            if ($request->id != 0) {
                $query = $query->whereHas('office', function ($q) use($request) {
                    $q->where('id', $request->id);
                    $q->where('office_type_id', 72);
                });
            }
        }
        $query = $query->with(['office' => function ($query1) use ($request) {
            $query1 = $query1->where('office_type_id', 72);
        }]);
        $datas = $query->get();
        return response([
            'success' => true,
            'message' => 'Region Wise District',
            'data' =>$datas
        ]);
    }

    public function region(Request $request)
    {
        // $query = MasterOfficeType::select("*");
        $query = MasterOffice::select(['id','office_type_id','office_name','office_name_bn'])
                ->where('status',0)
                ->where('office_type_id',72);
        if ($request->id) {
            $query = $query->where('id', $request->id);
        }
        $list = $query->orderBy('office_name', 'ASC')->get();
        return response([
            'success' => true,
            'message' => 'Region List',
            'data' => $list
        ]);
    }
    /**
     * master office store
     */
    public function store(Request $request)
    {
        $validationResult = MasterOfficeValidation::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $mo = new MasterOffice();
            $mo->org_id                = $request->org_id;
            $mo->office_type_id        = $request->office_type_id;
            $mo->country_id            = $request->country_id;
            $mo->division_id           = $request->division_id;
            $mo->district_id           = $request->district_id;
            $mo->upazilla_id           = $request->upazilla_id;
            $mo->union_id              = $request->union_id;
            $mo->office_name           = $request->office_name;
            $mo->office_name_bn        = $request->office_name_bn;
            $mo->office_code           = $request->office_code;
            $mo->parent_office_type_id = $request->parent_office_type_id;
            $mo->parent_office_id      = $request->parent_office_id;
            $mo->area_type_id          = $request->area_type_id??null;
            $mo->city_corporation_id   = $request->city_corporation_id??null;
            $mo->pauroshoba_id         = $request->pauroshoba_id??null;
            $mo->ward_id               = $request->ward_id??null;
            $mo->created_by            = (int)user_id();
            $mo->updated_by            = (int)user_id();
            $mo->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id' => $mo->id,
                'table_name' => 'master_offices',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $mo
        ]);
    }

    /**
     * master office update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterOfficeValidation::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $mo = MasterOffice::find($id);

        if (!$mo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $mo->org_id                = $request->org_id;
            $mo->office_type_id        = $request->office_type_id;
            $mo->country_id            = $request->country_id;
            $mo->division_id           = $request->division_id;
            $mo->district_id           = $request->district_id;
            $mo->upazilla_id           = $request->upazilla_id;
            $mo->union_id              = $request->union_id;
            $mo->office_name           = $request->office_name;
            $mo->office_name_bn        = $request->office_name_bn;
            $mo->office_code           = $request->office_code;
            $mo->parent_office_type_id = $request->parent_office_type_id;
            $mo->parent_office_id      = $request->parent_office_id;
            $mo->area_type_id          = $request->area_type_id??null;
            $mo->city_corporation_id   = $request->city_corporation_id??null;
            $mo->pauroshoba_id         = $request->pauroshoba_id??null;
            $mo->ward_id               = $request->ward_id??null;
            $mo->updated_by            = (int)user_id();
            $mo->update();

            Cache::forget('commonDropdown');

            save_log([
                'data_id' => $mo->id,
                'table_name' => 'master_offices',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $mo
        ]);
    }

    /**
     * master office status update
     */
    public function toggleStatus($id)
    {
        $mo = MasterOffice::find($id);

        if (!$mo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mo->status = $mo->status ? 0 : 1;
        $mo->update();

        Cache::forget('commonDropdown');

        save_log([
            'data_id' => $mo->id,
            'table_name' => 'master_offices',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mo
        ]);
    }

    /**
     * master office destroy
     */
    public function destroy($id)
    {
        $mo = MasterOffice::find($id);

        if (!$mo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mo->delete();

        Cache::forget('commonDropdown');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_offices',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * get parent office by org_id, parent_office_type_id
     */
    public function parentOffice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'org_id' => 'required',
            'parent_office_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        try {
            $mo = MasterOffice::select('id','office_name','office_name_bn')
                                ->where('org_id', $request->org_id)
                                ->where('id', $request->parent_office_type_id)
                                ->get();

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => $ex->getMessage()
            ]);

        }

        return response([
            'success' => true,
            'message' => 'Parent Office List',
            'data'    => $mo
        ]);
    }

    public function getOfficeDetail($officeId)
    {
        $result = [
            'success' => true,
            'message' => '',
            'data' => null
        ];
        if (!$officeId) {
            $result['success'] = false;
            return response($result);
        }
        
        $model = MasterOffice::find($officeId);
        
        if (!$model) {
            $result['success'] = false;
        }

        $result['data'] = $model;

        return response($result);
    }

    /**
     * Create or Update a resource
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createOrUpdate(Request $request)
    {
        $masterOfficeType = MasterOfficeType::whereOrgId(13)
            ->where(DB::raw('lower(office_type_name)'), 'like', '%' . strtolower('Warehouse') . '%')
            ->firstOrFail();

        $updatedData = [
            'org_id' => 13,
            'area_type_id' => 3,
            'country_id' => 1,
            'office_type_id' => $masterOfficeType->id,
            'office_name' => $request->warehouse_name,
            'office_name_bn' => $request->warehouse_name_bn,
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'upazilla_id' => $request->upazilla_id,
            'union_id' => $request->union_id,
            'created_by' => 1,
            'updated_by' => 1,
        ];

        $office = MasterOffice::updateOrCreate(
            ['id' => $request->office_id],
            $updatedData
        );

        if (!$office) {
            return response([
                'success' => false,
                'message' => 'Data not found!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data found!',
            'data' => $office
        ]);
    }
}
