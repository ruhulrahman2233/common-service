<?php

namespace App\Http\Controllers\OrgProfile;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterService;
use App\Http\Validations\OrgProfile\MasterServiceValidation;

class MasterServiceController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Service
     */
    public function index(Request $request)
    {
        // $query = MasterService::select("*");
        $query = DB::table('master_services')
                    ->join('master_components','master_services.component_id', '=','master_components.id')
                    ->join('master_modules','master_services.module_id', '=','master_modules.id')
                    ->select('master_services.*',
                                'master_components.component_name','master_components.component_name_bn',
                                'master_modules.module_name','master_modules.module_name_bn');

        if ($request->service_name) {
            $query = $query->where('service_name', 'like', "{$request->service_name}%")
                           ->orWhere('service_name_bn', 'like', "{$request->service_name}%") ;
        }

        if ($request->component_id) {
            $query = $query->where('master_services.component_id', $request->component_id);
        }

        if ($request->module_id) {
            $query = $query->where('master_services.module_id', $request->module_id);
        }

        if ($request->status) {
            $query = $query->where('master_services.status', $request->status);
        }

        $list = $query->orderBy('master_components.component_name', 'ASC')
                        ->orderBy('master_modules.module_name', 'ASC')
                        ->orderBy('master_services.service_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Service data  list',
            'data' => $list
        ]);
    }

    /**
     * master service store
     */
    public function store(Request $request)
    {
        $validationResult =MasterServiceValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $service = new MasterService();
            $service->service_name        = $request->service_name;
            $service->service_name_bn     = $request->service_name_bn;
            $service->component_id        = $request->component_id;
            $service->module_id           = $request->module_id;
            $service->sorting_order           = $request->sorting_order;
            $service->created_by          = (int)user_id();
            $service->updated_by          = (int)user_id();

            $service->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $service->id,
                'table_name' => 'master_services',2
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Service Data save successfully',
            'data'    => $service
        ]);
    }

    /**
     * master Service update
     */
    public function update(Request $request, $id)
    {
        $validationResult =MasterServiceValidation:: validate($request, $id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $service = MasterService::find($id);

        if (!$service) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $service->service_name    = $request->service_name;
            $service->service_name_bn = $request->service_name_bn;
            $service->component_id    = $request->component_id;
            $service->module_id       = $request->module_id;
            $service->sorting_order           = $request->sorting_order;
            $service->updated_by      = (int)user_id();
            $service->save();
            
            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $service->id,
                'table_name' => 'master_services',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Service Data update successfully',
            'data'    => $service
        ]);
    }

    /**
     * master service toggle status
     */
    public function toggleStatus($id)
    {
        $service = MasterService::find($id);

        if (!$service) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $service->status = $service->status ? 0 : 1;
        $service->save();

        save_log([
            'data_id' => $service->id,
            'table_name' => 'master_services',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Service Data updated successfully',
            'data'    => $service
        ]);
    }

    /**
     * master service destroy
     */
    public function destroy($id)
    {
        $service = MasterService::find($id);

        if (!$service) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $service->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_services',
            'execution_type' => 2   
        ]);

        return response([
            'success' => true,
            'message' => 'Service Data deleted successfully'
        ]);
    }
}
