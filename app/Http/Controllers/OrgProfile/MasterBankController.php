<?php

namespace App\Http\Controllers\OrgProfile;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrgProfile\MasterBank;
use Illuminate\Support\Facades\Cache;
use App\Http\Validations\OrgProfile\MasterBankValidation;

class MasterBankController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  master Bank
     */
    public function index(Request $request)
    {
        $query = DB::table('master_banks')
                    ->join('master_org_profiless','master_banks.org_id', '=','master_org_profiless.id')
                    ->join('master_components','master_banks.component_id', '=','master_components.id')
                    ->select('master_banks.*',
                            'master_org_profiless.id as org_id','master_org_profiless.org_name','master_org_profiless.org_name_bn',
                            'master_components.id as component_id','master_components.component_name','master_components.component_name_bn');


        if ($request->bank_name) {
            $query = $query->where('bank_name', 'like', "{$request->bank_name}%")
                            ->orWhere('bank_name_bn', 'like', "{$request->bank_name}%");
        }

        if ($request->org_id) {
            $query = $query->where('master_banks.org_id', $request->org_id);
        }

        if ($request->component_id) {
            $query = $query->where('master_banks.component_id', $request->component_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_components.component_name', 'ASC')
                        ->orderBy('master_banks.bank_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Bank list',
            'data' => $list
        ]);
    }
    /**
     * get all  master Bank listAll without pagination
     */
    public function listAll(Request $request)
    {
        $query = DB::table('master_banks')->get();
        return response([
            'success' => true,
            'message' => 'Bank list',
            'data' => $query
        ]);
    }

    /**
     * master bank store
     */
    public function store(Request $request)
    {
        $validationResult = MasterBankValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $bank                   = new MasterBank();
            $bank->bank_name        = $request->bank_name;
            $bank->bank_name_bn     = $request->bank_name_bn;
            $bank->org_id           = $request->org_id;
            $bank->component_id     = $request->component_id;
            $bank->created_by       = (int)user_id();
            $bank->updated_by       = (int)user_id();
            $bank->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $bank->id,
                'table_name' => 'master_banks',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $bank
        ]);
    }

    /**
     * master bank update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterBankValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $bank = MasterBank::find($id);

        if (!$bank) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $bank->bank_name        = $request->bank_name;
            $bank->bank_name_bn     = $request->bank_name_bn;
            $bank->org_id           = $request->org_id;
            $bank->component_id     = $request->component_id;
            $bank->updated_by      = (int)user_id();
            $bank->update();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $bank->id,
                'table_name' => 'master_banks',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $bank
        ]);
    }

    /**
     * master bank toggle status
     */
    public function toggleStatus($id)
    {
        $bank = MasterBank::find($id);

        if (!$bank) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $bank->status = $bank->status ? 0 : 1;
        $bank->update();

        save_log([
            'data_id' => $bank->id,
            'table_name' => 'master_banks',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $bank
        ]);
    }

    /**
     * Master bank destroy
     */
    public function destroy($id)
    {
        $bank = MasterBank::find($id);

        if (!$bank) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $bank->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_banks',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
