<?php

namespace App\Http\Controllers\OrgProfile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterUnion;
use App\Http\Validations\OrgProfile\MasterUnionValidation;

class UnionController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * get All union data
     */
    public function index(Request $request)
    {
        $query = DB::table('master_unions')
                    ->join('master_upazillas','master_unions.upazilla_id', '=','master_upazillas.id')
                    ->join('master_districts','master_upazillas.district_id', '=','master_districts.id')
                    ->join('master_divisions','master_districts.division_id', '=','master_divisions.id')
                    ->select('master_unions.*',
                            'master_upazillas.id as upazilla_id','master_upazillas.upazilla_name','master_upazillas.upazilla_name_bn',
                            'master_districts.id as district_id','master_districts.district_name','master_districts.district_name_bn',
                            'master_divisions.id as division_id','master_divisions.division_name','master_divisions.division_name_bn');

        if ($request->union_name) {
            $query = $query->where('union_name', 'like', "{$request->union_name}%")
                            ->orWhere('union_name_bn', 'like', "{$request->union_name}%");
        }

        if ($request->upazilla_id) {
            $query = $query->where('master_upazillas.id', $request->upazilla_id);
        }

        if ($request->district_id) {
            $query = $query->where('master_districts.id', $request->district_id);
        }

        if ($request->division_id) {
            $query = $query->where('master_divisions.id', $request->division_id);
        }

        if ($request->status) {
            $query = $query->where('master_unions.status', $request->status);
        }

        $list = $query->orderBy('master_divisions.division_name', 'ASC')
                        ->orderBy('master_districts.district_name', 'ASC')
                        ->orderBy('master_upazillas.upazilla_name', 'ASC')
                        ->orderBy('master_unions.union_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success'   => true,
            'message'   => 'union',
            'data'      => $list
        ]);
    }

    /**
     *Union store
     */
    public function store(Request $request)
    {
        $validationResult = MasterUnionValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $union = new MasterUnion();

            $union->union_name    = $request->union_name;
            $union->union_name_bn = $request->union_name_bn;
            $union->upazilla_id   = $request->upazilla_id;
            $union->created_by    = (int)user_id();
            $union->updated_by    = (int)user_id();
            $union->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id' => $union->id,
                'table_name' => 'master_unions',
            ]);


        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Country saved successfully',
            'data'    => $union
        ]);
    }

    /**
     * Union Update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterUnionValidation:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $union = MasterUnion::find($id);

        if (!$union) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $union->union_name     = $request->union_name;
            $union->union_name_bn  = $request->union_name_bn;
            $union->upazilla_id    = $request->upazilla_id;
            $union->updated_by     = (int)user_id();

            $union->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $union->id,
                'table_name'     => 'master_unions',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Union updated successfully',
            'data'    => $union
        ]);
    }

    /**
     * Union toggle status
     */
    public function toggleStatus($id)
    {
        $union = MasterUnion::find($id);

        if (!$union) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $union->status = $union->status ? 0 : 1;
        $union->save();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $union->id,
            'table_name'     => 'master_unions',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Union data updated successfully',
            'data'    => $union
        ]);
    }

    public function destroy($id)
    {
        $union = MasterUnion::find($id);

        if (!$union) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $union->delete();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $id,
            'table_name'     => 'master_unions',
            'execution_type' => 2
        ]);


        return response([
            'success' => true,
            'message' => 'Union deleted successfully'
        ]);
    }

}
