<?php

namespace App\Http\Controllers\OrgProfile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterDivision;
use App\Http\Validations\OrgProfile\MasterDivisionValidation;


class DivisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all countries
     */
    public function index(Request $request)
    {
        $query = DB::table('master_divisions')
                    ->leftJoin('master_countries','master_divisions.country_id','=','master_countries.id')
                    ->select("master_divisions.*",'master_countries.country_name');

        if ($request->division_name) {
            $query = $query->where('division_name', 'like', "{$request->division_name}%")
                            ->orWhere('division_name_bn', 'like', "{$request->division_name}%");
        }

        if ($request->country_id ) {
            $query = $query->where('country_id', $request->country_id );
        }

        if ($request->status) {
            $query = $query->where('master_divisions.status', $request->status);
        }

        $list = $query->orderBy('master_divisions.division_name', 'ASC')->paginate(request('per_page', config('app.per_page')));
        
        return response([
            'success' => true,
            'message' => 'Country list',
            'data' => $list
        ]);
    }

    /**
     * division store
     */
    public function store(Request $request)
    {
        $validationResult =MasterDivisionValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $division = new MasterDivision();
            $division->division_name    = $request->division_name;
            $division->division_name_bn = $request->division_name_bn;
            $division->country_id       = 1;
            $division->created_by       = (int) user_id();
            $division->updated_by       = (int) user_id();
            $division->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $division->id,
                'table_name'     => 'master_divisions',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $division
        ]);
    }

    /**
     * division update
     */
    public function update(Request $request, $id)
    {
        $validationResult =MasterDivisionValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $division = MasterDivision::find($id);

        if (!$division) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $division->division_name    = $request->division_name;
            $division->division_name_bn = $request->division_name_bn;
            $division->country_id       = 1;
            $division->updated_by       = (int) user_id();
            $division->update();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $division->id,
                'table_name'     => 'master_divisions',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $division
        ]);
    }

    /**
     * division status update
     */
    public function toggleStatus($id)
    {
        $division = MasterDivision::find($id);

        if (!$division) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $division->status = $division->status ? 0 : 1;
        $division->update();

        save_log([
            'data_id'        => $division->id,
            'table_name'     => 'master_divisions',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $division
        ]);
    }

    /**
     * division destroy
     */
    public function destroy($id)
    {
        $division = MasterDivision::find($id);

        if (!$division) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $division->delete();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $id,
            'table_name'     => 'master_divisions',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * Show division
     */
    public function show($id)
    {
        $division = MasterDivision::find($id);

        if (!$division) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data found!',
            'data'    => $division
        ]);
    }
}
