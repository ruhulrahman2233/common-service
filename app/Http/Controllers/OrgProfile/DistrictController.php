<?php

namespace App\Http\Controllers\OrgProfile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterDistrict;
use App\http\Validations\OrgProfile\MasterDistrictValidation;

class DistrictController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all countries
     */
    public function listAll(Request $request)
    {
        $districts = MasterDistrict::select(['id','district_name'])->oldest('district_name')->get();
        return response([
            'success' => true,
            'message' => 'District List',
            'data'    => $districts
        ]);
    }

    public function index(Request $request)
    {
        $query = DB::table('master_districts')
                    ->join('master_divisions','master_districts.division_id','=','master_divisions.id')
                    ->select("master_districts.*",'master_divisions.division_name','master_divisions.division_name_bn');

        if ($request->district_name) {
            $query = $query->where('district_name', 'like', "{$request->district_name}%")
                            ->orwhere('district_name_bn', 'like', "{$request->district_name}%");
        }

        if ($request->division_id ) {
            $query = $query->where('division_id', $request->division_id );
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }
        
        $list = $query->orderBy('master_divisions.division_name', 'ASC')
                        ->orderBy('master_districts.district_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'District list',
            'data' => $list
        ]);
    }

    /**
     * district store
     */
    public function store(Request $request)
    {
        $validationResult =MasterDistrictValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
 
        try {
            $district = new MasterDistrict();
            $district->district_name    = $request->district_name;
            $district->district_name_bn = $request->district_name_bn;
            $district->division_id      = $request->division_id;
            $district->created_by       = (int) user_id();
            $district->updated_by       = (int) user_id();
            $district->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $district->id,
                'table_name'     => 'master_districts',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $district
        ]);
    }

    /**
     * district update
     */
    public function update(Request $request, $id)
    {
        $validationResult =MasterDistrictValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $district = MasterDistrict::find($id);

        if (!$district) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $district->district_name    = $request->district_name;
            $district->district_name_bn = $request->district_name_bn;
            $district->division_id      = $request->division_id;
            $district->updated_by       = (int)user_id();
            $district->update();

            Cache::forget('commonDropdown');
            
            save_log([
                'data_id'        => $district->id,
                'table_name'     => 'master_districts',
                'execution_type' => 1,
            ]);
            
        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $district
        ]);
    }

    /**
     * division status update
     */
    public function toggleStatus($id)
    {
        $district = MasterDistrict::find($id);

        if (!$district) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $district->status = $district->status ? 0 : 1;
        $district->update();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $district->id,
            'table_name'     => 'master_districts',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $district
        ]);
    }

    /**
     * district destroy
     */
    public function destroy($id)
    {
        $district = MasterDistrict::find($id);

        if (!$district) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $district->delete();

        Cache::forget('commonDropdown');
          
        save_log([
            'data_id'        => $district->id,
            'table_name'     => 'master_districts',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * district show
     */
    public function show($id)
    {
        $district = MasterDistrict::find($id);

        if (!$district) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data found!',
            'data' => $district
        ]);
    }
}
