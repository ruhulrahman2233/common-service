<?php

namespace App\Http\Controllers\OrgProfile;

use App\Http\Controllers\Controller;
use App\Models\OrgProfile\MasterOrganizationProfile;
use App\Http\Validations\OrgProfile\MasterOrganizationProfileValidation;
use Illuminate\Http\Request;
use Validator;

class MasterOrganizationProfileController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master ogranization profile
     */
    public function index(Request $request)
    {
        $query = MasterOrganizationProfile::select("*");

        if ($request->org_name) {
            $query = $query->where('org_name', 'like', "{$request->org_name}%")
                           ->orWhere('org_name_bn', 'like', "{$request->org_name}%");     
        }

        if ($request->establishment_year) {
            $query = $query->where('establishment_year', $request->establishment_year);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('org_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Master Office Type list',
            'data' => $list
        ]);
    }

    /**
     * master ogranization profile store
     */
    public function store(Request $request)
    {
        $validationResult =MasterOrganizationProfileValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $mop = new MasterOrganizationProfile();
            $mop->org_name       = $request->org_name;
            $mop->org_name_bn    = $request->org_name_bn;
            $mop->establishment_year = (int)$request->establishment_year;
            $mop->about_org      = $request->about_org;
            $mop->about_org_bn   = $request->about_org_bn;
            $mop->mission        = $request->mission;
            $mop->mission_bn     = $request->mission_bn;
            $mop->vision         = $request->vision;
            $mop->vision_bn      = $request->vision_bn;
            $mop->hierarchy      = (int)$request->hierarchy;
            $mop->created_by     = (int)user_id();
            $mop->updated_by     = (int)user_id();
            if($request->logo){
                $logo          = $request->file('logo');
                $logo_name     = time().".".$logo->getClientOriginalExtension();
                $directory     = 'common-service/uploads/org-profile/';
                $logo->move($directory, $logo_name);
                $mop->logo = $directory.$logo_name;
            }
            $mop->save();

            save_log([
                'data_id' => $mop->id,
                'table_name' => 'master_org_profiless',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $mop
        ]);
    }

    /**
     * master ogranization profile update
     */
    public function update(Request $request, $id)
    {
        $validationResult =MasterOrganizationProfileValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $mop = MasterOrganizationProfile::find($id);

        if (!$mop) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $mop->org_name       = $request->org_name;
            $mop->org_name_bn    = $request->org_name_bn;
            $mop->establishment_year = (int)$request->establishment_year;
            $mop->about_org      = $request->about_org;
            $mop->about_org_bn   = $request->about_org_bn;
            $mop->mission        = $request->mission;
            $mop->mission_bn     = $request->mission_bn;
            $mop->vision         = $request->vision;
            $mop->vision_bn      = $request->vision_bn;
            $mop->hierarchy      = (int)$request->hierarchy;
            $mop->updated_by     = (int)user_id();
            if($request->logo){
                if($mop->logo != null && file_exists($mop->logo)){
                    unlink($mop->logo);
                }
                $logo          = $request->file('logo');
                $logo_name     = time().".".$logo->getClientOriginalExtension();
                $directory     = 'common-service/uploads/org-profile/';
                $logo->move($directory, $logo_name);
                $mop->logo = $directory.$logo_name;
            }
            $mop->update();

            save_log([
                'data_id' => $mop->id,
                'table_name' => 'master_org_profiless',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $mop
        ]);
    }

    /**
     * master ogranization profile status update
     */
    public function toggleStatus($id)
    {
        $mop = MasterOrganizationProfile::find($id);

        if (!$mop) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mop->status = $mop->status ? 0 : 1;
        $mop->update();

        save_log([
            'data_id' => $mop->id,
            'table_name' => 'master_org_profiless',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $mop
        ]);
    }

    /**
     * master ogranization profile destroy
     */
    public function destroy($id)
    {
        $mop = MasterOrganizationProfile::find($id);

        if (!$mop) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $mop->delete();

        save_log([
            'data_id' => $id,
            'table_name' => 'master_org_profiless',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
