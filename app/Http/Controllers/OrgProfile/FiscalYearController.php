<?php

namespace App\Http\Controllers\OrgProfile;

use App\Models\OrgProfile\MasterFiscalYear;
use App\Http\Validations\OrgProfile\MasterFiscalYearValidations;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Cache;
use DB;

class FiscalYearController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function changeSerialOrder(Request $request,$model)
    {
        $datas = $request->all();
        if(!empty($datas)){
            foreach($datas as $key=>$value){
                $modelName = 'App\Models\OrgProfile\\' .$model;
                $UpdateData=$modelName::where('id',$value['id'])->first();
                $UpdateData->sorting_order= $value['sorting_order'];
                $UpdateData->save();
            }
        }
        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $datas
        ]);
    }

    /**
     * Get all Master Fiscal Year with pagination
     */
    public function index(Request $request)
    {

        $query = MasterFiscalYear::query()->orderby('sorting_order', 'asc');

        if ($request->year) {
            $query = $query->where('id', $request->year);
        }

        $list = $query->paginate(request('per_page', env('PER_PAGE', 10)));

        return response([
            'success' => true,
            'message' => 'Notification Setting list',
            'data' => $list
        ]);
    }

    /**
     * Get all Master Fiscal Year 
     */
    public function listAll(Request $request)
    {
        $query = MasterFiscalYear::query();

        $list = $query->get();

        return response([
            'success' => true,
            'message' => 'Notification Setting list',
            'data' => $list
        ]);
    }

    /**
     * Master Fiscal Year store
     */
    public function prevFascalYear($id) {
        $MasterFiscalYear = MasterFiscalYear::find($id);
        $datas = MasterFiscalYear::where('start_date','<',$MasterFiscalYear->start_date)->orderBy('start_date','asc')->get();
        $MasterFiscal = array();
        foreach($datas as $key=>$value) {
            $MasterFiscal  = $value;
        }
        return $MasterFiscal;
    }

    /**
     * get fiscal year by date
     */
    public function getFiscalYearByDate(Request $request) {
        $fiscalYear = MasterFiscalYear::where('start_date','<=',$request->PriceDate)
                                ->where('end_date','>=',$request->PriceDate)
                                ->first();
        if (!$fiscalYear) {
            return response([
                'success' => false,
                'message' => 'Data not found',
                'data'    => []
            ]);
        }
        return response([
            'success' => true,
            'data'    => $fiscalYear
        ]);
    }

    public function store(Request $request)
    {
        $validationResult = MasterFiscalYearValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        DB::beginTransaction();

        try {
            $requestAll = $request->all();
            $requestAll['created_by'] = (int)user_id();
            $requestAll['updated_by'] = (int)user_id();

            $masterFiscalYear = new MasterFiscalYear($requestAll);
            $masterFiscalYear->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'    => $masterFiscalYear->id,
                'table_name' => 'master_fiscal_years'
            ]);

            DB::commit();

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterFiscalYear
        ]);
    }

    /**
     * Master Fiscal Year update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterFiscalYearValidations::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterFiscalYear = MasterFiscalYear::find($id);

        if (!$masterFiscalYear) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        DB::beginTransaction();

        try {
            $requestAll = $request->all();
            $requestAll['updated_by'] = (int)user_id();

            $masterFiscalYear->fill($requestAll);
            $masterFiscalYear->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'       => $masterFiscalYear->id,
                'table_name'    => 'master_fiscal_years',
                'execution_type'=> 1
            ]);

            DB::commit();

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterFiscalYear
        ]);
    }

    /**
     * Master Fiscal Year status update
     */
    public function toggleStatus($id)
    {
        $masterFiscalYear = MasterFiscalYear::find($id);

        if (!$masterFiscalYear) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterFiscalYear->status = $masterFiscalYear->status ? 0 : 1;
        $masterFiscalYear->update();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'       => $masterFiscalYear->id,
            'table_name'    => 'master_fiscal_years',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterFiscalYear
        ]);
    }

    /**
     * Master Fiscal Year destroy
     */
    public function destroy($id)
    {
        $masterFiscalYear = MasterFiscalYear::find($id);

        if (!$masterFiscalYear) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterFiscalYear->delete();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_fiscal_years',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * Master Fiscal Year show
     */
    public function show($id)
    {
        $masterFiscalYear = MasterFiscalYear::find($id);

        if (!$masterFiscalYear) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data found',
            'data' => $masterFiscalYear
        ]);
    }
}
