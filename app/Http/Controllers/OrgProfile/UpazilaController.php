<?php

namespace App\Http\Controllers\OrgProfile;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterUpazila;
use App\Http\Validations\OrgProfile\MasterUpazilaValidation;

class UpazilaController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all upazila list
     */
    public function index(Request $request)
    {
        $query = DB::table('master_upazillas')
                        ->Join('master_districts','master_upazillas.district_id','=','master_districts.id')
                        ->Join('master_divisions','master_districts.division_id','=','master_divisions.id')
                        ->select("master_upazillas.*",'master_districts.district_name','master_districts.district_name_bn',
                                   'master_divisions.id as division_id','master_divisions.division_name','master_divisions.division_name_bn');

        if ($request->upazilla_name) {
            $query = $query->where('upazilla_name', 'like', "{$request->upazilla_name}%")
                            ->orwhere('upazilla_name_bn', 'like', "{$request->upazilla_name}%");
        }

        if ($request->district_id) {
            $query = $query->where('master_upazillas.district_id', $request->district_id);
        }

        if ($request->division_id) {
            $query = $query->where('master_divisions.id', $request->division_id);
        }

        if ($request->district_id) {
            $query = $query->where('master_districts.id', $request->district_id);
            $query = $query->where('master_upazillas.district_id', 'like', "{$request->district_id }%");            ;
        }

        if ($request->status) {
            $query = $query->where('master_upazillas.status', $request->status);
        }

        $list = $query->orderBy('master_divisions.division_name', 'ASC')
                        ->orderBy('master_districts.district_name', 'ASC')
                        ->orderBy('master_upazillas.upazilla_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Upazilla List',
            'data'     => $list
        ]);
    }

    /**
     * upazila store
     */
    public function store(Request $request)
    {
        $validationResult = MasterUpazilaValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $upazila = new MasterUpazila();

            $upazila->upazilla_name    = $request->upazilla_name;
            $upazila->upazilla_name_bn = $request->upazilla_name_bn;
            $upazila->district_id      = $request->district_id;
            $upazila->created_by       = (int)user_id();
            $upazila->updated_by       = (int)user_id();

            $upazila->save();

            Cache::forget('commonDropdown');

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Upazila saved successfully',
            'data'    => $upazila
        ]);
    }

    /**
     * upazila update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterUpazilaValidation:: validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $upazila = MasterUpazila::find($id);

        if (!$upazila) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $upazila->upazilla_name    = $request->upazilla_name;
            $upazila->upazilla_name_bn = $request->upazilla_name_bn;
            $upazila->district_id      = $request->district_id;
            $upazila->updated_by       = (int)user_id();

            $upazila->save();

            Cache::forget('commonDropdown');

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Upazila updated successfully',
            'data'    => $upazila
        ]);
    }

    /**
     * upazila toggleStatus
     */
    public function toggleStatus($id)
    {
        $upazila = MasterUpazila::find($id);

        if (!$upazila) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $upazila->status = $upazila->status ? 0 : 1;
        $upazila->save();

        Cache::forget('commonDropdown');

        return response([
            'success' => true,
            'message' => 'Upazila status updated successfully',
            'data' => $upazila
        ]);
    }
     /**
      * Upazilla delete
      */
    public function destroy($id)
    {
        $upazila = MasterUpazila::find($id);

        if (!$upazila) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $upazila->delete();

        Cache::forget('commonDropdown');

        return response([
            'success' => true,
            'message' => 'Upazila deleted successfully'
        ]);
    }

}
