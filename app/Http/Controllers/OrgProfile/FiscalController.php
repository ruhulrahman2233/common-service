<?php

namespace App\Http\Controllers\OrgProfile;

use App\Http\Controllers\Controller;
use App\Models\OrgProfile\MasterFiscal;
use App\http\Validations\OrgProfile\MasterFiscalValidation;
use Illuminate\Http\Request;
use Validator;

class FiscalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all countries
     */
    public function index(Request $request)
    {
        $query = MasterFiscal::select("*");

        if ($request->year_name) {
            $query = $query->where('year_name', 'like', "{$request->year_name}%")
                            ->orwhere('year_name_bn', 'like', "{$request->year_name}%");;
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Country list',
            'data' => $list
        ]);
    }

    /**
     * country store
     */
    public function store(Request $request)
    {
        $validationResult = MasterFiscalValidation:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $country = new MasterFiscal();
            $country->year_name    = $request->year_name;
            $country->year_name_bn = $request->year_name_bn;
            $country->created_by      =(int) user_id();
            $country->updated_by      = (int) user_id();
            $country->save();

            save_log([
                'data_id'        => $country->id,
                'table_name'     => 'master_fiscal_year',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $country
        ]);
    }

    /**
     * country update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterFiscalValidation:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $country = MasterFiscal::find($id);

        if (!$country) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $country->year_name    = $request->year_name;
            $country->year_name_bn = $request->year_name_bn;
            $country->updated_by      = (int) user_id();
            $country->update();

            save_log([
                'data_id'        => $country->id,
                'table_name'     => 'master_fiscal_year',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $country
        ]);
    }

    /**
     * country status update
     */
    public function toggleStatus($id)
    {
        $country = MasterFiscal::find($id);

        if (!$country) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $country->status = $country->status ? 0 : 1;
        $country->update();

        save_log([
            'data_id'        => $country->id,
            'table_name'     => 'master_fiscal_year',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $country
        ]);
    }

    /**
     * country destroy
     */
    public function destroy($id)
    {
        $country = MasterFiscal::find($id);

        if (!$country) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $country->delete();

        save_log([
            'data_id'        => $id,
            'table_name'     => 'master_fiscal_year',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
