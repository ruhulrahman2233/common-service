<?php

namespace App\Http\Controllers\OrgProfile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterModule;
use App\Models\OrgProfile\MasterOrgModule;;
use App\Http\Validations\OrgProfile\MasterModuleValidation;

class MasterModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Modules
     */
    public function index(Request $request)
    {
        $query=MasterModule::with(['org','component']);

        if ($request->module_name) {
            $query = $query->where('module_name', 'like', "{$request->module_name}%")
                            ->orWhere('module_name_bn', 'like', "{$request->module_name}%");
        }

        if ($request->component_id) {
            $query = $query->where('component_id', 'like', "{$request->component_id}%");
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('module_name', 'ASC')->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Module list',
            'data' => $list
        ]);
    }

    /**
     * master module get org data list
     */
    public function getlist()
    {
        $query = DB::table('master_modules')
                    ->join('master_org_modules','master_modules.id', '=','master_org_modules.module_id')
                    ->join('master_org_profiless','master_org_modules.org_id', '=','master_org_profiless.id')
                    ->select('master_modules.*',
                             'master_org_modules.*',
                             'master_org_profiless.id','master_org_profiless.org_name');



        $list = $query->paginate($request->per_page);

        return response()->json([
            'success'=>true,
            'data'   =>$list
        ]);
    }
    /**
     * Module store
     */
    public function store(Request $request)
    {
        $validationResult = MasterModuleValidation::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        DB::beginTransaction();

        try {
            $module = new MasterModule();
            $module->module_name     = $request->module_name;
            $module->module_name_bn  = $request->module_name_bn;
            $module->component_id    = $request->component_id;
            $module->sorting_order    = $request->sorting_order;
            $module->created_by      = (int)user_id();
            $module->updated_by      = (int)user_id();

            $module->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $module->id,
                'table_name' => 'master_modules',
            ]);

            $morgs = $request->org_id;
            for ($i = 0; $i < count($morgs); $i++) {
                $masterOrgModule               = new MasterOrgModule;
                $masterOrgModule->org_id       = $morgs[$i];
                $masterOrgModule->module_id    = $module->id;
                $masterOrgModule->save();

                DB::commit();

            }
        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Module data save successfully',
            'data'    => $module,
        ]);
    }

    /**
     * Module update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterModuleValidation::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $module = MasterModule::find($id);

        if (!$module) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        DB::beginTransaction();

        try {
            $module->module_name     = $request->module_name;
            $module->module_name_bn  = $request->module_name_bn;
            $module->component_id    = $request->component_id;
            $module->sorting_order    = $request->sorting_order;
            $module->updated_by      = (int)user_id();
            $module->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $module->id,
                'table_name' => 'master_modules',
                'execution_type' => 1
            ]);

            $morgs = $request->org_id;
            MasterOrgModule::where('module_id', $id)->delete();
            for ($i = 0; $i < count($morgs); $i++) {
                $masterOrgModule                = new MasterOrgModule;
                $masterOrgModule->org_id        = $morgs[$i];
                $masterOrgModule->module_id     = $module->id;
                $masterOrgModule->save();

                DB::commit();

            }
        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Module Data update successfully',
            'data'    => $module
        ]);
    }

    /**
     * Module status update
     */
    public function toggleStatus($id)
    {
        $module = MasterModule::find($id);

        if (!$module) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $module->status = $module->status ? 0 : 1;
        $module->save();

        save_log([
            'data_id' => $module->id,
            'table_name' => 'master_modules',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Module Data updated successfully',
            'data'    => $module
        ]);
    }

    /**
     * Modules destroy
     */
    public function destroy($id)
    {
        $module = MasterModule::find($id);

        if (!$module) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $module->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_modules',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Module Data deleted successfully'
        ]);
    }
}
