<?php

namespace App\Http\Controllers\OrgProfile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\OrgProfile\MasterOfficeType;
use App\Http\Validations\OrgProfile\MasterOfficeTypeValidation;

class MasterOfficeTypeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master office type
     */
    public function index(Request $request)
    {
        // $query = MasterOfficeType::select("*");
        $query = DB::table('master_office_types')
                    ->join('master_org_profiless','master_office_types.org_id', '=','master_org_profiless.id')
                    ->select('master_office_types.*',
                            'master_org_profiless.id as org_id','master_org_profiless.org_name','master_org_profiless.org_name_bn',);

        if ($request->office_type_name) {
            $query = $query->where('office_type_name', 'like', "{$request->office_type_name}%")
                            ->orWhere('office_type_name_bn', 'like', "{$request->office_type_name}%");
        }

        if ($request->id) {
            $query = $query->where('id', $request->id);
        }

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_office_types.office_type_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Master Office Type list',
            'data' => $list
        ]);
    }

    /**
     * master office type store
     */
    public function store(Request $request)
    {
        $validationResult =MasterOfficeTypeValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $master_office_type = new MasterOfficeType();
            $master_office_type->office_type_name    = $request->office_type_name;
            $master_office_type->office_type_name_bn = $request->office_type_name_bn;
            $master_office_type->org_id              = $request->org_id;
            $master_office_type->created_by          = (int)user_id();
            $master_office_type->updated_by          = (int)user_id();

            $master_office_type->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id' => $master_office_type->id,
                'table_name' => 'master_office_types',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $master_office_type
        ]);
    }

    /**
     * master office type update
     */
    public function update(Request $request, $id)
    {
        $validationResult =MasterOfficeTypeValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $master_office_type = MasterOfficeType::find($id);

        if (!$master_office_type) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $master_office_type->office_type_name    = $request->office_type_name;
            $master_office_type->office_type_name_bn = $request->office_type_name_bn;
            $master_office_type->org_id              = $request->org_id;
            $master_office_type->updated_by          = (int)user_id();
            $master_office_type->update();

            Cache::forget('commonDropdown');

            save_log([
                'data_id' => $master_office_type->id,
                'table_name' => 'master_office_types',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $master_office_type
        ]);
    }

    /**
     * master office type status update
     */
    public function toggleStatus($id)
    {
        $master_office_type = MasterOfficeType::find($id);

        if (!$master_office_type) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $master_office_type->status = $master_office_type->status ? 0 : 1;
        $master_office_type->update();

        Cache::forget('commonDropdown');

        save_log([
            'data_id' => $master_office_type->id,
            'table_name' => 'master_office_types',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $master_office_type
        ]);
    }

    /**
     * master office type destroy
     */
    public function destroy($id)
    {
        $master_office_type = MasterOfficeType::find($id);

        if (!$master_office_type) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $master_office_type->delete();

        Cache::forget('commonDropdown');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_office_types',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * Show warehouse office type
     *
     * @return \Illuminate\Http\Response
     */
    public function getWraehouseOfficeType()
    {
        $masterOfficeTypeId = MasterOfficeType::whereOrgId(13)
            ->where(DB::raw('lower(office_type_name)'), 'like', '%' . strtolower('Warehouse') . '%')
            ->value('id');

        if (!$masterOfficeTypeId) {
            return response([
                'success' => false,
                'message' => 'Data Not Found!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data Found!',
            'data' => $masterOfficeTypeId
        ]);
    }

}
