<?php

namespace App\Http\Controllers\OrgProfile;

use App\Http\Controllers\Controller;
use App\Models\OrgProfile\MasterCountry;
use App\http\Validations\OrgProfile\MasterCountryValidation;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all countries
     */
    public function index(Request $request)
    {
        $query = MasterCountry::select("*");

        if ($request->country_name) {
            $query = $query->where('country_name', 'like', "{$request->country_name}%")
                            ->orWhere('country_name_bn', 'like', "{$request->country_name}%");
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Country list',
            'data' => $list
        ]);
    }

    /**
     * country store
     */
    public function store(Request $request)
    {
        $validationResult = MasterCountryValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $country = new MasterCountry();
            $country->country_name    = $request->country_name;
            $country->country_name_bn = $request->country_name_bn;
            $country->created_by      = (int) user_id();
            $country->updated_by      = (int) user_id();
            $country->save();

            save_log([
                'data_id'    => $country->id,
                'table_name' => 'master_countries',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $country
        ]);
    }

    /**
     * country update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterCountryValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $country = MasterCountry::find($id);

        if (!$country) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $country->country_name    = $request->country_name;
            $country->country_name_bn = $request->country_name_bn;
            $country->updated_by      = (int) user_id();
            $country->update();

            save_log([
                'data_id'        => $country->id,
                'table_name'     => 'master_countries',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $country
        ]);
    }

    /**
     * country status update
     */
    public function toggleStatus($id)
    {
        $country = MasterCountry::find($id);

        if (!$country) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $country->status = $country->status ? 0 : 1;
        $country->update();

        save_log([
            'data_id'        => $country->id,
            'table_name'     => 'master_countries',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $country
        ]);
    }

    /**
     * country destroy
     */
    public function destroy($id)
    {
        $country = MasterCountry::find($id);

        if (!$country) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $country->delete();

        save_log([
            'data_id'        => $country->id,
            'table_name'     => 'master_countries',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
