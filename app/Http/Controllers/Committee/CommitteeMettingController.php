<?php

namespace App\Http\Controllers\Committee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Committee\CommiteeMettingValidation;
use App\Models\Committee\CommitteeMetting;
use App\Helpers\GlobalFileUploadFunctoin;
use Illuminate\Support\Facades\DB;


class CommitteeMettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  commitee Metting
     */
    public function index(Request $request)
    {
        $query = DB::table('cmt_meeting_minutes')
                        ->join('master_org_profiless','cmt_meeting_minutes.org_id', '=','master_org_profiless.id')
                        ->join('cmt_committees','cmt_meeting_minutes.cmt_committee_id', '=','cmt_committees.id')
                        ->join('cmt_agenda','cmt_meeting_minutes.cmt_agenda_id', '=','cmt_agenda.id')
                        ->select('cmt_meeting_minutes.*',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                    'cmt_committees.committee_name','cmt_committees.committee_name_bn',
                                    'cmt_agenda.agenda_name','cmt_agenda.agenda_name_bn');

        if ($request->decision) {
            $query = $query->where('cmt_meeting_minutes.decision', 'like', "{$request->decision}%")
                            ->orWhere('cmt_meeting_minutes.decision_bn', 'like', "{$request->decision}%");
        }

        if ($request->cmt_agenda_id) {
            $query = $query->where('cmt_meeting_minutes.cmt_agenda_id', $request->cmt_agenda_id);
        }

        if ($request->org_id) {
            $query = $query->where('cmt_meeting_minutes.org_id', $request->org_id);
        }

        if ($request->cmt_committee_id) {
            $query = $query->where('cmt_meeting_minutes.cmt_committee_id', $request->cmt_committee_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }


        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('cmt_committees.committee_name', 'ASC')
                        ->orderBy('cmt_agenda.agenda_name', 'ASC')
                        ->orderBy('cmt_meeting_minutes.decision', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'committee Metting list',
            'data' => $list
        ]);
    }

    /**
     * committee Metting Store
     */
    public function store(Request $request)
    {
        $validationResult = CommiteeMettingValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path_attendance 		        = 'committee-meeting/attendance_attachment';
        $file_path_document 		        = 'committee-meeting/document_attachment';
        $attendance_attachment         	    =  $request->file('attendance_attachment');
        $document_attachment 	            =  $request->file('document_attachment');


        try {
            $committeeMetting                          = new CommitteeMetting();
            $committeeMetting->org_id                  = (int)$request->org_id;
            $committeeMetting->cmt_committee_id        = (int)$request->cmt_committee_id;
            $committeeMetting->cmt_agenda_id           = (int)$request->cmt_agenda_id;
            $committeeMetting->decision                = $request->decision;
            $committeeMetting->decision_bn             = $request->decision_bn;
            $committeeMetting->created_by              = (int)user_id();
            $committeeMetting->updated_by              = (int)user_id();

            if($attendance_attachment !=null && $attendance_attachment !=""){
                $attachment_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path_attendance,'attendance_attachment');
            }

            if($document_attachment !=null && $document_attachment !=""){
                $document_attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path_document,'document_attachment');
            }

			$committeeMetting->attendance_attachment       	=  $attachment_name ? $attachment_name : null;
			$committeeMetting->document_attachment       	=  $document_attachment ? $document_attachment : null;

			if($committeeMetting->save()){
				 GlobalFileUploadFunctoin::file_upload($request, $file_path_attendance, 'attendance_attachment', $attachment_name);
                GlobalFileUploadFunctoin::file_upload($request, $file_path_document, 'document_attachment', $document_attachment);
            }

            save_log([
                'data_id'    => $committeeMetting->id,
                'table_name' => 'cmt_meeting_minutes',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $committeeMetting
        ]);
    }

    /**
     *  Committee Metting  Update
     */
    public function update(Request $request, $id)
    {
        $validationResult = CommiteeMettingValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path_attendance 		        = 'committee-meeting/attendance_attachment';
        $file_path_document 		        = 'committee-meeting/document_attachment';
        $attendance_attachment         	    =  $request->file('attendance_attachment');
        $document_attachment 	            =  $request->file('document_attachment');

        $committeeMetting                 = CommitteeMetting::find($id);
        $old_attendance_attachment 		  = $committeeMetting->attendance_attachment;
        $old_document_attachment		  = $committeeMetting->document_attachment;

        if (!$committeeMetting) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $committeeMetting->org_id                  = (int)$request->org_id;
            $committeeMetting->cmt_committee_id        = (int)$request->cmt_committee_id;
            $committeeMetting->cmt_agenda_id           = (int)$request->cmt_agenda_id;
            $committeeMetting->decision                = $request->decision;
            $committeeMetting->decision_bn             = $request->decision_bn;
            $committeeMetting->updated_by              = (int)user_id();

            if($attendance_attachment !=null && $attendance_attachment !=""){
                $attendance_attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path_attendance,'attendance_attachment');
                $committeeMetting->attendance_attachment       	=  $attendance_attachment;
            }

            if($document_attachment !=null && $document_attachment !=""){
                $document_attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path_document,'document_attachment');
                $committeeMetting->document_attachment       	=  $document_attachment;
            }

			if($committeeMetting->update()){
				 GlobalFileUploadFunctoin::file_upload($request, $file_path_attendance, 'attendance_attachment', $attendance_attachment,  $old_attendance_attachment );
                GlobalFileUploadFunctoin::file_upload($request, $file_path_document, 'document_attachment', $document_attachment, $old_document_attachment);
            }

            save_log([
                'data_id' => $committeeMetting->id,
                'table_name' => 'cmt_meeting_minutes',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $committeeMetting
        ]);
    }


    /**
     * committeeMetting status update
     */
    public function toggleStatus($id)
    {
        $committeeMetting = CommitteeMetting::find($id);

        if (!$committeeMetting) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $committeeMetting->status = $committeeMetting->status ? 0 : 1;
        $committeeMetting->save();

        save_log([
            'data_id'        => $committeeMetting->id,
            'table_name'     => 'cmt_committees',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $committeeMetting
        ]);
    }


    /**
     * Committee  metting Delete
     */
    public function destroy($id)
    {
        $committeeMetting = CommitteeMetting::find($id);

        if (!$committeeMetting) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $committeeMetting->delete();

        save_log([
            'data_id' => $id,
            'table_name' => 'cmt_meeting_minutes',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
