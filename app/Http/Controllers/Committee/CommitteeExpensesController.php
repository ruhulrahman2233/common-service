<?php

namespace App\Http\Controllers\Committee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Committee\CommitteeExpensesValidation;
use App\Models\Committee\CommitteeExpenses;
use App\Helpers\GlobalFileUploadFunctoin;
use Illuminate\Support\Facades\DB;

class CommitteeExpensesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  commitee expenses
     */
    public function index(Request $request)
    {
        $query = DB::table('cmt_expenses')
                        ->join('master_org_profiless','cmt_expenses.org_id', '=','master_org_profiless.id')
                        ->join('cmt_committees','cmt_expenses.cmt_committee_id', '=','cmt_committees.id')
                        ->join('cmt_agenda','cmt_expenses.cmt_agenda_id', '=','cmt_agenda.id')
                        ->join('master_fiscal_years','cmt_expenses.fiscal_year', '=','master_fiscal_years.id')
                        ->select('cmt_expenses.*',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                    'cmt_committees.committee_name','cmt_committees.committee_name_bn',
                                    'master_fiscal_years.year',
                                    'cmt_agenda.agenda_name','cmt_agenda.agenda_name_bn');


        if ($request->cmt_committee_id) {
            $query = $query->where('cmt_expenses.cmt_committee_id', $request->cmt_committee_id);
        }

        if ($request->cmt_agenda_id) {
            $query = $query->where('cmt_expenses.cmt_agenda_id', $request->cmt_agenda_id);
        }




        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('cmt_committees.committee_name', 'ASC')
                        ->orderBy('cmt_agenda.agenda_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Committee Expenses list',
            'data' => $list
        ]);
    }

    /**
     * committee expenses Store
     */
    public function store(Request $request)
    {
        $validationResult = CommitteeExpensesValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path 		= 'commitee-expenses';
        $attachment 	=  $request->file('attachment');

        try {
            $comExpenses                          = new CommitteeExpenses();
            $comExpenses->org_id                  = (int)$request->org_id;
            $comExpenses->fiscal_year             = (int)$request->fiscal_year;
            $comExpenses->cmt_committee_id        = (int)$request->cmt_committee_id;
            $comExpenses->date                    = (new \DateTime($request->date))->format('Y-m-d');
            $comExpenses->cmt_agenda_id           = (int)$request->cmt_agenda_id;
            $comExpenses->amount                  = $request->amount;
            $comExpenses->description             = $request->description;
            $comExpenses->description_bn          = $request->description_bn;

            $comExpenses->created_by       = (int)user_id();
            $comExpenses->updated_by       = (int)user_id();

            if($attachment !=null && $attachment !=""){
                $attachment_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path,'attachment');
            }
            $comExpenses->attachment       	=  $attachment_name ? $attachment_name : null;

            if($comExpenses->save()){
                GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment_name);
           }

            save_log([
                'data_id' => $comExpenses->id,
                'table_name' => 'cmt_expenses',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $comExpenses
        ]);
    }

    /**
     *  Committee expenses  Update
     */
    public function update(Request $request, $id)
    {
        $validationResult = CommitteeExpensesValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }
        $file_path 		= 'commitee-expenses';
        $attachment 	=  $request->file('attachment');

        $comExpenses     = CommitteeExpenses::find($id);
        $old_file 		 = $comExpenses->attachment;

        if (!$comExpenses) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $comExpenses->org_id                  = (int)$request->org_id;
            $comExpenses->fiscal_year             = (int)$request->fiscal_year;
            $comExpenses->cmt_committee_id        = (int)$request->cmt_committee_id;
            $comExpenses->date                    = (new \DateTime($request->date))->format('Y-m-d');
            $comExpenses->cmt_agenda_id           = (int)$request->cmt_agenda_id;
            $comExpenses->amount                  = $request->amount;
            $comExpenses->description             = $request->description;
            $comExpenses->description_bn          = $request->description_bn;

            if($attachment !=null && $attachment !=""){
                $attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request,$file_path,'attachment');
				$comExpenses->attachment    =  $attachment;
            }

			if($comExpenses->save()){
				 GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment, $old_file );
            }

            $comExpenses->updated_by       = (int)user_id();
            $comExpenses->update();

            save_log([
                'data_id' => $comExpenses->id,
                'table_name' => 'cmt_expenses',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $comExpenses
        ]);
    }

        /**
     * Committee  Expenses status update
     */
    public function toggleStatus($id)
    {
        $comAgendata = CommitteeExpenses::find($id);

        if (!$comAgendata) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $comAgendata->status = $comAgendata->status ? 0 : 1;
        $comAgendata->save();

        save_log([
            'data_id'        => $comAgendata->id,
            'table_name'     => 'cmt_committees',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $comAgendata
        ]);
    }


    /**
     * Committee  Expenses
     */
    public function destroy($id)
    {
        $comExpenses = CommitteeExpenses::find($id);

        if (!$comExpenses) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $comExpenses->delete();

        save_log([
            'data_id' => $id,
            'table_name' => 'cmt_expenses',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
