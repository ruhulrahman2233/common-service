<?php

namespace App\Http\Controllers\Committee;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Helpers\GlobalFileUploadFunctoin;
use App\Models\Committee\CreateCommittee;
use App\Models\Committee\CreateCommitteeMember;
use App\Http\Validations\Committee\CreateCommitteeValidation;

class CreateCommitteeController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Committee
     */
    public function index(Request $request)
    {
        // $query = CreateCommittee::with(['organization','committee']);
        $query = DB::table('cmt_committees')                           
                        ->leftjoin('master_org_profiless','cmt_committees.org_id', '=','master_org_profiless.id')
                        ->select('cmt_committees.*',
                            'master_org_profiless.org_name','master_org_profiless.org_name_bn'
                        );

        if ($request->committee_name) {
            $query = $query->where('committee_name', 'like', "{$request->committee_name}%")
                            ->orWhere('committee_name_bn', 'like', "{$request->committee_name}%");
        }

        if ($request->purpose) {
            $query = $query->where('purpose', 'like', "{$request->purpose}%")
                            ->orWhere('purpose_bn', 'like', "{$request->purpose}%");
        }

        if ($request->document_name) {
            $query = $query->where('document_name', 'like', "{$request->document_name}%");
        }

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('cmt_committees.committee_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Committee list',
            'data' => $list
        ]);
    }

    /**
     * commitee get org data list
     */
    public function getindex(Request $request)
    {
        $query = DB::table('cmt_committees')
                    ->join('master_org_profiless','cmt_committees.org_id', '=','master_org_profiless.id')
                    ->select('cmt_committees.*',
                                'master_org_profiless.org_name',
                                'master_org_profiless.org_name_bn');

        $getlist = $query->paginate($request->per_page);

        return response()->json([
            'success'=>true,
            'data'=>$getlist
        ]);
    }

    /**
     * all commmitee list
     */
    public function allIndex()
    {
        $data = CreateCommittee::where('status',0)->get();

        if(!$data){
            return response()->json([
                'success'=>false,
                'message'=>'data not found'
            ]);
        } else {
            return response()->json([
                'success'=>true,
                'data'=>$data
            ]);
        }    

    }

    /**
     * master compoent get org data list
     */
    public function getlist()
    {
        $query = DB::table('master_components')
                    ->join('master_org_components','master_components.id', '=','master_org_components.component_id')
                    ->join('master_org_profiless','master_org_components.org_id', '=','master_org_profiless.id')
                    ->select('master_components.*',
                                'master_org_components.*',
                                'master_org_profiless.id','master_org_profiless.org_name');



        $detailslist = $query->paginate($request->per_page);

        return response()->json([
            'success'=>true,
            'data'=>$detailslist
        ]);

    }

    /**
     * committee store
     */
    public function store(Request $request)
    {
        // return $request;
        $validationResult = CreateCommitteeValidation::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path 		= 'create-commitee/';
        $attachment 	=  $request->file('attachment');

        DB::beginTransaction();

        try {

            $commitee                     = new CreateCommittee();
            $commitee->org_id             = (int)$request->org_id;
            $commitee->committee_name     = json_decode($request->committee_name);
            $commitee->committee_name_bn  = json_decode($request->committee_name_bn);
            $commitee->formation_date     = (new \DateTime(json_decode($request->formation_date)))->format('Y-m-d');
            $commitee->purpose            = json_decode($request->purpose);
            $commitee->purpose_bn         = json_decode($request->purpose_bn);
            $commitee->duration           = json_decode($request->duration);
            $commitee->document_name      = json_decode($request->document_name);
            $commitee->created_by         = (int)user_id();
            $commitee->updated_by         = (int)user_id();

            if($attachment !=null && $attachment !=""){
                $attachment= GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path,'attachment');
            }

            $commitee->attachment       	=  $attachment ? $attachment : null;

            if($commitee->save() && ($attachment !=null && $attachment !="")){
                GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment);
            }

            Cache::forget('dropdown_common_config');

            $members = json_decode($request->committee);

            foreach ($members as $member) {
                $commiteeMember                    = new CreateCommitteeMember;
                $commiteeMember->user_id           = $member->user_id;
                $commiteeMember->cmt_committee_id  = $commitee->id;
                $commiteeMember->save();
            }

            DB::commit();

            save_log([
                'data_id'        => $commitee->id,
                'table_name'     => 'cmt_committees',
            ]);

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => ' data save successfully',
            'data'    => $commitee
        ]);
    }

    /**
     * Committee update
     */
    public function update(Request $request, $id)
    {
        $validationResult = CreateCommitteeValidation::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path  = 'create-commitee/';
        $attachment =  $request->file('attachment');
        $commitee = CreateCommittee::find($id);
        $old_file = $commitee->attachment;

        if (!$commitee) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        DB::beginTransaction();

        try {

            $commitee->org_id             = (int)$request->org_id;
            $commitee->committee_name     = json_decode($request->committee_name);
            $commitee->committee_name_bn  = json_decode($request->committee_name_bn);
            $commitee->formation_date     = (new \DateTime(json_decode($request->formation_date)))->format('Y-m-d');
            $commitee->purpose            = json_decode($request->purpose);
            $commitee->purpose_bn         = json_decode($request->purpose_bn);
            $commitee->duration           = json_decode($request->duration);
            $commitee->document_name      = json_decode($request->document_name);
            $commitee->updated_by         = (int)user_id();

            if($attachment !=null && $attachment !=""){
                $attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request,$file_path,'attachment');
                $commitee->attachment    =  $attachment;
            }

            if($commitee->save() && ($attachment !=null && $attachment !="")){
				GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment, $old_file );
			}

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id'        => $commitee->id,
                'table_name'     => 'cmt_committees',
                'execution_type' => 1
            ]);

           CreateCommitteeMember::where('cmt_committee_id', $id)->delete();

           $members = json_decode($request->committee);

           foreach ($members as $member) {
               $commiteeMember                    = new CreateCommitteeMember;
               $commiteeMember->user_id           = $member->user_id;
               $commiteeMember->cmt_committee_id  = $commitee->id;
               $commiteeMember->save();
           }

            DB::commit();

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => ' Data update successfully',
            'data'    => $commitee
        ]);
    }

    /**
     * committee status update
     */
    public function toggleStatus($id)
    {
        $commitee = CreateCommittee::find($id);

        if (!$commitee) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $commitee->status = $commitee->status ? 0 : 1;
        $commitee->save();

        save_log([
            'data_id'        => $commitee->id,
            'table_name'     => 'cmt_committees',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $commitee
        ]);
    }

    /**
     * Committee destroy
     */
    public function destroy($id)
    {
        $commitee = CreateCommittee::find($id);

        if (!$commitee) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        CreateCommitteeMember::where('cmt_committee_id',$id)->delete();
        $commitee->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id'        => $id,
            'table_name'     => 'cmt_committees',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * Committee Member Delete
     */
    public function CommiteeMemberDelete($id)
    {
        $commiteeMember = CreateCommitteeMember::where('cmt_committee_id',$id);

        if (!$commiteeMember) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $commiteeMember->delete();


        save_log([
            'data_id'        => $id,
            'table_name'     => 'cmt_committee_members',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

    /**
     * Committee Member List
     */

     public function getCommitteemember($id)
     {
        $commiteeMemberlist = CreateCommitteeMember::where('cmt_committee_id',$id)->get();

        if(!$commiteeMemberlist) {
            return response()->json([
                'success' =>false,
                 'message' => 'Data Not Found'
            ]);
        } else {
            return response()->json([
                'success' =>True,
                'Data' => $commiteeMemberlist
            ]);
        }
     }
}
