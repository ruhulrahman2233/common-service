<?php

namespace App\Http\Controllers\Committee;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Helpers\GlobalFileUploadFunctoin;
use App\Models\Committee\CommitteeAgenda;
use App\Http\Validations\Committee\CommitteeAgendaValidation;

class CommitteeAgendaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  commitee agenda
     */
    public function index(Request $request)
    {
        $query = DB::table('cmt_agenda')
                        ->join('cmt_committees','cmt_agenda.cmt_committee_id', '=','cmt_committees.id')
                        ->join('master_org_profiless','cmt_agenda.org_id', '=','master_org_profiless.id')
                        ->select('cmt_agenda.*',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                    'cmt_committees.committee_name','cmt_committees.committee_name_bn');

        if ($request->agenda_name) {
            $query = $query->where('agenda_name', 'like', "{$request->agenda_name}%")
                            ->orWhere('agenda_name', 'like', "{$request->agenda_name}%");
        }

        if ($request->cmt_committee_id) {
            $query = $query->where('cmt_agenda.cmt_committee_id', $request->cmt_committee_id);
        }

        if ($request->org_id) {
            $query = $query->where('cmt_agenda.org_id', $request->org_id);
        }

        if ($request->meeting_number) {
            $query = $query->where('meeting_number', $request->meeting_number);
        }

        if ($request->meeting_date) {
            $query = $query->where('meeting_date', $request->meeting_date);
        }

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('cmt_committees.committee_name', 'ASC')
                        ->orderBy('cmt_agenda.agenda_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Document list',
            'data' => $list
        ]);
    }

    /**
     * committee agenda Store
     */
    public function store(Request $request)
    {
        $validationResult = CommitteeAgendaValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path 		= 'commitee-agenda';
        $attachment 	=  $request->file('attachment');

        try {
            $comAgendata                          = new CommitteeAgenda();
            $comAgendata->org_id                  = (int)$request->org_id;
            $comAgendata->cmt_committee_id        = (int)$request->cmt_committee_id;
            $comAgendata->meeting_number          = (int)$request->meeting_number;
            $comAgendata->memo_number             = $request->memo_number;
            $comAgendata->memo_issue_date         = (new \DateTime($request->memo_issue_date))->format('Y-m-d');
            $comAgendata->agenda_name             = $request->agenda_name;
            $comAgendata->agenda_name_bn          = $request->agenda_name_bn;
            $comAgendata->meeting_date            = (new \DateTime($request->meeting_date))->format('Y-m-d');
            $comAgendata->created_by              = (int)user_id();
            $comAgendata->updated_by              = (int)user_id();

            if($attachment !=null && $attachment !=""){
                $attachment_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path,'attachment');
            }

            $comAgendata->attachment   =  $attachment_name ? $attachment_name : null;

            if( $comAgendata->save()) {
                GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment_name);
            }

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $comAgendata->id,
                'table_name' => 'cmt_agenda',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $comAgendata
        ]);
    }

    /**
     *  Committee agenda  Update
     */
    public function update(Request $request, $id)
    {
        $validationResult = CommitteeAgendaValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path 		= 'commitee-agenda';
        $attachment 	=  $request->file('attachment');


        $comAgendata    = CommitteeAgenda::find($id);
        $old_file 		= $comAgendata->attachment;

        if (!$comAgendata) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $comAgendata->org_id                  = (int)$request->org_id;
            $comAgendata->cmt_committee_id        = (int)$request->cmt_committee_id;
            $comAgendata->meeting_number          = (int)$request->meeting_number;
            $comAgendata->memo_number             = $request->memo_number;
            $comAgendata->memo_issue_date         = (new \DateTime($request->memo_issue_date))->format('Y-m-d');
            $comAgendata->agenda_name             = $request->agenda_name;
            $comAgendata->agenda_name_bn          = $request->agenda_name_bn;
            $comAgendata->meeting_date            =(new \DateTime($request->meeting_date))->format('Y-m-d');
            $comAgendata->updated_by              = (int)user_id();

            if($attachment != null && $attachment != ""){
                $attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request,$file_path,'attachment');
				$comAgendata->attachment    =  $attachment;
            }

			if($comAgendata->update()){
				 GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment, $old_file );
			}

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $comAgendata->id,
                'table_name' => 'cmt_agenda',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $comAgendata
        ]);
    }


        /**
     * comAgendata status update
     */
    public function toggleStatus($id)
    {
        $comAgendata = CommitteeAgenda::find($id);

        if (!$comAgendata) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $comAgendata->status = $comAgendata->status ? 0 : 1;
        $comAgendata->save();

        save_log([
            'data_id'        => $comAgendata->id,
            'table_name'     => 'cmt_committees',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $comAgendata
        ]);
    }

    /**
     * Committee  Delete
     */
    public function destroy($id)
    {
        $comAgendata = CommitteeAgenda::find($id);

        if (!$comAgendata) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $comAgendata->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'cmt_agenda',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

      /**
     * all agenda list
     */
    public function allList()
    {
        $data = CommitteeAgenda::get();

        if(!$data){
            return response()->json([
                'success'=>false,
                'message'=>'data not found'
            ]);
        } else {
            return response()->json([
                'success'=>true,
                'data'=>$data
            ]);
        }
    }

}
