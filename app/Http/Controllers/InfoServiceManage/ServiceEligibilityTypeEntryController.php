<?php
namespace App\Http\Controllers\InfoServiceManage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\InfoServiceManage\ServiceEligibilityTypeEntryValidations;
use App\Models\InfoServiceManage\ServiceEligibilityTypeEntry;
use DB;

class ServiceEligibilityTypeEntryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Service Eligibility Type
     */
    public function index(Request $request)
    {
        $query = DB::table('master_eligibility_types')
                        ->select('master_eligibility_types.*');
        if ($request->type_name) {
            $query = $query->where('master_eligibility_types.type_name', 'like', "{$request->type_name}%")
                           ->orWhere('master_eligibility_types.type_name_bn', 'like', "{$request->type_name}%");
        }  

        $list = $query->orderBy('type_name', 'ASC')->paginate(request('per_page', config('app.per_page')));;

        if(count($list) > 0){
            return response([
                'success' => true,
                'message' => "Service Eligibility Type list",
                'data' => $list
            ]);

        }else{
            return response([
                'success' => false,
                'message' => "Data not found!!"
            ]);
        }
    }

    /**
     * Service Eligibility Type store
     */
    public function store(Request $request)
    {
        $validationResult = ServiceEligibilityTypeEntryValidations:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $serEliTypeEntry                  = new ServiceEligibilityTypeEntry();
            $serEliTypeEntry->type_name       = trim($request->type_name);   
            $serEliTypeEntry->type_name_bn    = $request->type_name_bn;    
            $serEliTypeEntry->created_by		 = user_id();
            $serEliTypeEntry->created_at      = date('Y-m-d');     
            $serEliTypeEntry->save();       

            save_log([
                'data_id'    => $serEliTypeEntry->id,
                'table_name' => 'master_eligibility_types'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $serEliTypeEntry
        ]);
    }

    /**
     * Service Eligibility Type update
     */
    public function update(Request $request, $id)
    {
        $validationResult = ServiceEligibilityTypeEntryValidations:: validate($request,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $serEliTypeEntry = ServiceEligibilityTypeEntry::find($id);

        if (!$serEliTypeEntry) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $serEliTypeEntry->type_name       = trim($request->type_name);   
            $serEliTypeEntry->type_name_bn    = $request->type_name_bn;
			$serEliTypeEntry->updated_by	  = user_id();            
            $serEliTypeEntry->updated_at      = date('Y-m-d');
            $serEliTypeEntry->update();

            save_log([
                'data_id'       => $serEliTypeEntry->id,
                'table_name'    => 'master_eligibility_types',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $serEliTypeEntry
        ]);
    }


    /**
     * Service Eligibility Type status update
     */
    public function toggleStatus($id)
    {
        $serEliTypeEntry = ServiceEligibilityTypeEntry::find($id);

        if (!$serEliTypeEntry) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $serEliTypeEntry->status = $serEliTypeEntry->status ? 0 : 1;
        $serEliTypeEntry->update();

        save_log([
            'data_id'       => $serEliTypeEntry->id,
            'table_name'    => 'master_eligibility_types',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $serEliTypeEntry
        ]);
    }
 
    /**
     * Service Eligibility Type destroy
     */
    public function destroy($id)
    {
        $serEliTypeEntry = ServiceEligibilityTypeEntry::find($id);

        if (!$serEliTypeEntry) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $serEliTypeEntry->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_eligibility_types',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
