<?php
namespace App\Http\Controllers\InfoServiceManage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\InfoServiceManage\ManageFaqValidations;
use App\Models\InfoServiceManage\ManageFaq;
use DB;

class ManageFaqController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  Manage Faq
     */
    public function index(Request $request)
    {
        $query = DB::table('master_faqs')
                        ->join('master_components','master_faqs.component_id','=','master_components.id')
                        ->join('master_services','master_faqs.service_id','=','master_services.id')
                        ->select("master_faqs.*",
                                'master_components.id as component_id', 'master_components.component_name', 'master_components.component_name_bn',
                                'master_services.service_name', 'master_services.service_name_bn'
                            );

        if ($request->component_id) {
            $query = $query->where('master_components.id', $request->component_id);
        } 

        if ($request->service_id) {
            $query = $query->where('service_id', $request->service_id);
        } 

        if ($request->question) {
            $query = $query->where('question', 'like', "{$request->question}%")
                           ->orWhere('question_bn', 'like', "{$request->question}%");
        } 

        if ($request->answer) {
            $query = $query->where('answer', 'like', "{$request->answer}%")
                           ->orWhere('answer_bn', 'like', "{$request->answer}%");
        }        

        // $query = $query->where('status', 0);       

        $list = $query->orderBy('master_components.component_name', 'ASC')
                        ->orderBy('master_services.service_name', 'ASC')
                        ->orderBy('master_faqs.id', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        if(count($list) > 0){
            return response([
                'success' => true,
                'message' => " Manage Faq list",
                'data' => $list
            ]);

        }else{
            return response([
                'success' => false,
                'message' => "Data not found!!"
            ]);
        }
       
    }

    /**
     *  Manage Faq store
     */
    public function store(Request $request)
    {
        
        $validationResult = ManageFaqValidations:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
            $componentId    = $request->component_id;   
            $serviceId      = $request->service_id; 
            $allQuestions = $request->question;
        try {
            foreach($allQuestions as  $value){
                $manageFaq                  = new ManageFaq();      
                $manageFaq->component_id    = $componentId;   
                $manageFaq->service_id      = $serviceId;
                $manageFaq->question        = $value['question'];   
                $manageFaq->question_bn     = $value['question_bn']; 
                $manageFaq->answer    		= $value['answer']; 
                $manageFaq->answer_bn       = $value['answer_bn'];
                $manageFaq->created_by      = user_id();
                $manageFaq->created_at      = date('Y-m-d');     
                $manageFaq->save();  

                save_log([
                    'data_id'    => $manageFaq->id,
                    'table_name' => 'master_faqs'
                ]);
            }           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $manageFaq
        ]);
    }

    /**
     *  Manage Faq update
     */
    public function update(Request $request, $id)
    {
        $validationResult = ManageFaqValidations:: validate($request,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $manageFaq = ManageFaq::find($id);

        if (!$manageFaq) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
            $componentId    = $request->component_id;   
            $serviceId      = $request->service_id;   
            $allQuestions = $request->question;

        try {
            foreach($allQuestions as $value){
                $manageFaq                  = new ManageFaq();      
                $manageFaq->component_id    = $componentId;   
                $manageFaq->service_id      = $serviceId;
                $manageFaq->question        = $value['question'];   
                $manageFaq->question_bn     = $value['question_bn']; 
                $manageFaq->answer    		= $value['answer']; 
                $manageFaq->answer_bn       = $value['answer_bn'];
                $manageFaq->updated_by      = user_id();            
                $manageFaq->updated_at      = date('Y-m-d');   
                $manageFaq->update();  

                save_log([
                    'data_id'    => $manageFaq->id,
                    'table_name' => 'master_faqs',
                    'execution_type'=> 1
                ]);
            }           

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $manageFaq
        ]);
    }
    /**
     *  Manage Faq update
     */
    public function updateSingle(Request $request, $id)
    {
        $validationResult = ManageFaqValidations:: validate($request,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $manageFaq = ManageFaq::find($id);

        if (!$manageFaq) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {      
                $manageFaq->component_id    = $request->component_id;   
                $manageFaq->service_id      = $request->service_id; 
                $manageFaq->question        = $request->question; 
                $manageFaq->question_bn     = $request->question_bn; 
                $manageFaq->answer    		= $request->answer; 
                $manageFaq->answer_bn       = $request->answer_bn; 
                $manageFaq->updated_by      = user_id();            
                $manageFaq->updated_at      = date('Y-m-d');   
                $manageFaq->update();  

                save_log([
                    'data_id'    => $manageFaq->id,
                    'table_name' => 'master_faqs',
                    'execution_type'=> 1
                ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $manageFaq
        ]);
    }


    /**
     *  Manage Faq status update
     */
    public function toggleStatus($id)
    {
        $manageFaq = ManageFaq::find($id);

        if (!$manageFaq) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $manageFaq->status = $manageFaq->status ? 0 : 1;
        $manageFaq->update();

        save_log([
            'data_id'       => $manageFaq->id,
            'table_name'    => 'master_faqs',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $manageFaq
        ]);
    }
 
    /**
     *  Manage Faq destroy
     */
    public function destroy($id)
    {
        $manageFaq = ManageFaq::find($id);

        if (!$manageFaq) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $manageFaq->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_faqs',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
