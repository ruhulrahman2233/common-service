<?php
namespace App\Http\Controllers\InfoServiceManage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\InfoServiceManage\ContentEntryValidations;
use App\Models\InfoServiceManage\ContentEntry;
use App\Models\InfoServiceManage\MasterContentEligibility;
use DB;

class ContentEntryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Master contents
     */
    public function index(Request $request)
    {
       $query = ContentEntry::with(['masterContentEligibility']);
       /* $query = DB::table('master_contents') 
                    ->leftJoin('master_content_eligibility','master_contents.id', '=','master_content_eligibility.master_content_id')
                    ->leftJoin('master_eligibility_types','master_content_eligibility.eligibility_criteria_id', '=','master_eligibility_types.id')
                    ->select('master_conte2nts.*',
                            'master_content_eligibility.eligibility_criteria_id',
                            'master_eligibility_types.type_name', 
                            'master_eligibility_types.type_name_bn'
                            );*/

        if ($request->component_id) {
            $query = $query->where('component_id', $request->component_id);
        } 

        if ($request->service_id) {
            $query = $query->where('service_id', $request->service_id);
        } 

        if ($request->description) {
            $query = $query->where('description', 'like', "{$request->description}%")
                           ->orWhere('description_bn', 'like', "{$request->description}%");
        }            

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if(count($list) > 0){
            return response([
                'success' => true,
                'message' => "Master contents list",
                'data' => $list
            ]);

        }else{
            return response([
                'success' => false,
                'message' => "Data not found!!"
            ]);
        }
       
    }

    /**
     * Master contents store
     */
    public function store(Request $request)
    {
        $validationResult = ContentEntryValidations:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        DB::beginTransaction();
        try {
            $masterContents                  = new ContentEntry();
            $masterContents->component_id    = $request->component_id;   
            $masterContents->service_id      = $request->service_id;   
            $masterContents->description     = $request->description;   
            $masterContents->description_bn  = $request->description_bn;
            $masterContents->created_by      = user_id()??null;
            $masterContents->created_at      = date('Y-m-d');  

            if($masterContents->save()){
                $eligiCritIds = $request->eligibility_criteria_id;
                foreach ($eligiCritIds as $key => $value) {
                    $masterContentEligi    = new MasterContentEligibility();
                    $masterContentEligi->master_content_id          = $masterContents->id;
                    $masterContentEligi->eligibility_criteria_id    = $value;
                    $masterContentEligi->created_at                 = date('Y-m-d'); 
                    $masterContentEligi->save();                  
                }
            } 

            save_log([
                'data_id'    => $masterContents->id,
                'table_name' => 'master_contents'
            ]);

            DB::commit(); 

        } catch (\Exception $ex) {

            DB::rollback();
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterContents
        ]);
    }

    /**
     * Master contents update
     */
    public function update(Request $request, $id)
    {
        $validationResult = ContentEntryValidations:: validate($request,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterContents = ContentEntry::find($id);

        if (!$masterContents) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        DB::beginTransaction();
        try {
            $masterContents->component_id    = $request->component_id;   
            $masterContents->service_id      = $request->service_id;   
            $masterContents->description     = $request->description;   
            $masterContents->description_bn  = $request->description_bn;
            $masterContents->updated_by      = user_id()??null;            
            $masterContents->updated_at      = date('Y-m-d');
             if($masterContents->update()){ 
                $oldMasContEligi = MasterContentEligibility::where('master_content_id',$id)->delete();
                $eligiCritIds = $request->eligibility_criteria_id;
                foreach ($eligiCritIds as $key => $value) {
                    $masterContentEligi    = new MasterContentEligibility();
                    $masterContentEligi->master_content_id          = $masterContents->id;
                    $masterContentEligi->eligibility_criteria_id    = $value;
                    $masterContentEligi->updated_at                 = date('Y-m-d');
                    $masterContentEligi->save();                    
                }
            }           

            save_log([
                'data_id'       => $masterContents->id,
                'table_name'    => 'master_contents',
                'execution_type'=> 1
            ]);

            DB::commit();    

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterContents
        ]);
    }


    /**
     * Master contents status update
     */
    public function toggleStatus($id)
    {
        $masterContents = ContentEntry::find($id);

        if (!$masterContents) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterContents->status = $masterContents->status ? 0 : 1;
        $masterContents->update();

        save_log([
            'data_id'       => $masterContents->id,
            'table_name'    => 'master_contents',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterContents
        ]);
    }
 
    /**
     * Master contents destroy
     */
    public function destroy($id)
    {
        $masterContents = ContentEntry::find($id);        

        if (!$masterContents) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        MasterContentEligibility::where('master_content_id',$id)->delete();

        $masterContents->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_contents',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
