<?php
namespace App\Http\Controllers\PaymentManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\PaymentManagement\PaymentServiceEntryValidations;
use App\Models\PaymentManagement\PaymentServiceEntry;
use DB;

class PaymentServiceEntryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all Payment service entry
     */
    public function index(Request $request)
    {
        $query = DB::table('master_payment_services')
                        ->select('master_payment_services.*');

        if ($request->org_id) {
            $query = $query->where('master_payment_services.org_id', $request->org_id);
        }

        if ($request->component_id) {
            $query = $query->where('master_payment_services.component_id', $request->component_id);
        } 

        if ($request->module_id) {
            $query = $query->where('master_payment_services.module_id', $request->module_id);
        }

        if ($request->service_id) {
            $query = $query->where('master_payment_services.service_id', $request->service_id);
        }


        $list = $query->paginate($request->per_page??10);

        if(count($list) > 0){
            return response([
                'success' => true,
                'message' => "Payment service entry list",
                'data' => $list
            ]);

        }else{
            return response([
                'success' => false,
                'message' => "Data not found!!"
            ]);
        }
       
    }

    /**
     * Payment service entry store
     */
    public function store(Request $request)
    {
        $validationResult = PaymentServiceEntryValidations:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $masterPaymentServices                  = new PaymentServiceEntry();
            $masterPaymentServices->org_id          = $request->org_id;   
            $masterPaymentServices->component_id    = $request->component_id;   
            $masterPaymentServices->module_id       = $request->module_id;   
            $masterPaymentServices->service_id      = $request->service_id;
            $masterPaymentServices->amount          = $request->amount;
            $masterPaymentServices->created_by      = user_id()??null;
            $masterPaymentServices->created_at      = date('Y-m-d');
            $masterPaymentServices->save();

            save_log([
                'data_id'    => $masterPaymentServices->id,
                'table_name' => 'master_payment_services'
            ]);

        } catch (\Exception $ex) {

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterPaymentServices
        ]);
    }

    /**
     * Payment service entry update
     */
    public function update(Request $request, $id)
    {
        $validationResult = PaymentServiceEntryValidations:: validate($request,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterPaymentServices = PaymentServiceEntry::find($id);

        if (!$masterPaymentServices) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
        try {
            $masterPaymentServices->org_id          = $request->org_id;   
            $masterPaymentServices->component_id    = $request->component_id;   
            $masterPaymentServices->module_id       = $request->module_id;   
            $masterPaymentServices->service_id      = $request->service_id;
            $masterPaymentServices->amount          = $request->amount;
            $masterPaymentServices->updated_by      = user_id()??null;            
            $masterPaymentServices->updated_at      = date('Y-m-d');
            $masterPaymentServices->update();

            save_log([
                'data_id'       => $masterPaymentServices->id,
                'table_name'    => 'master_payment_services',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {           

            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterPaymentServices
        ]);
    }


    /**
     * Payment service entry status update
     */
    public function toggleStatus($id)
    {
        $masterPaymentServices = PaymentServiceEntry::find($id);

        if (!$masterPaymentServices) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterPaymentServices->status = $masterPaymentServices->status ? 0 : 1;
        $masterPaymentServices->update();

        save_log([
            'data_id'       => $masterPaymentServices->id,
            'table_name'    => 'master_payment_services',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterPaymentServices
        ]);
    }
 
    /**
     * Payment service entry destroy
     */
    public function destroy($id)
    {
        $masterPaymentServices = PaymentServiceEntry::find($id);

        if (!$masterPaymentServices) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterPaymentServices->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_payment_services',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
