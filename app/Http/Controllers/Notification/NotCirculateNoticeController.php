<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Validations\Notification\NotCirculateNoticeValidation;
use App\Models\Notification\NotCirculateNotice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class NotCirculateNoticeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * get all  Circulate Notices
     */


    public function index(Request $request)
    {
        $query = DB::table('not_circulate_notices')
                        ->join('master_offices','not_circulate_notices.office_id', '=','master_offices.id')
                        ->join('master_designations','not_circulate_notices.designation_id', '=','master_designations.id')
                        ->join('master_org_profiless','not_circulate_notices.org_id', '=','master_org_profiless.id')
                        ->join('master_notification_types','not_circulate_notices.notification_type_id', '=','master_notification_types.id')
                        ->select('not_circulate_notices.*',
                                        'master_offices.office_name','master_offices.office_name_bn',
                                        'master_designations.designation','master_designations.designation_bn',
                                        'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                        'master_notification_types.not_type_name','master_notification_types.not_type_name_bn');

        if ($request->notice_title) {
            $query = $query->where('notice_title', 'like', "{$request->notice_title}%")
                            ->orWhere('notice_title_bn', 'like', "{$request->notice_title}%");
        }

        if ($request->notification_type_id) {
            $query = $query->where('not_circulate_notices.notification_type_id', $request->notification_type_id);
        }

         if ($request->designation_id) {
            $query = $query->where('not_circulate_notices.designation_id', $request->designation_id);
        }


        if ($request->org_id) {
            $query = $query->where('not_circulate_notices.org_id', $request->org_id);
        }

        if ($request->office_id) {
            $query = $query->where('not_circulate_notices.office_id', $request->office_id);
        }

        if ($request->notice_date) {
            $query = $query->where('not_circulate_notices.notice_date', $request->notice_date);
        }

        if ($request->notice_time) {
            $query = $query->where('not_circulate_notices.notice_time', $request->notice_time);
        }

        if ($request->status) {
            $query = $query->where('not_circulate_notices.status', $request->status);
        }



        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_notification_types.not_type_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Notification Setting list',
            'data' => $list
        ]);
    }

    /**
     * notifation Circulate Notices store
     */
    public function store(Request $request)
    {
        $validationResult = NotCirculateNoticeValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $NotCirculateNotice                       = new NotCirculateNotice();
            $NotCirculateNotice->notice_title         = $request->notice_title;
            $NotCirculateNotice->notice_title_bn      = $request->notice_title_bn;
            $NotCirculateNotice->description          = $request->description;
            $NotCirculateNotice->description_bn       = $request->description_bn;
            $NotCirculateNotice->notice_for           = $request->notice_for;
            $NotCirculateNotice->office_id            = (int)$request->office_id;
            $NotCirculateNotice->designation_id       = (int)$request->designation_id;
            $NotCirculateNotice->notice_date          = $request->notice_date;
            $NotCirculateNotice->notice_time          = $request->notice_time;
            $NotCirculateNotice->org_id               = (int)$request->org_id;
            $NotCirculateNotice->notification_type_id = (int)$request->notification_type_id;
            $NotCirculateNotice->created_by           = (int)user_id();
            $NotCirculateNotice->updated_by           = (int)user_id();
            $NotCirculateNotice->save();

            save_log([
                'data_id' => $NotCirculateNotice->id,
                'table_name' => 'not_circulate_notices',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $NotCirculateNotice
        ]);
    }

    /**
     * notification Circulate Notices update
     */
    public function update(Request $request, $id)
    {
        $validationResult = NotCirculateNoticeValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $NotCirculateNotice = NotCirculateNotice::find($id);

        if (!$NotCirculateNotice) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {

            $NotCirculateNotice->notice_title         = $request->notice_title;
            $NotCirculateNotice->notice_title_bn      = $request->notice_title_bn;
            $NotCirculateNotice->description          = $request->description;
            $NotCirculateNotice->description_bn       = $request->description_bn;
            $NotCirculateNotice->notice_for           = $request->notice_for;
            $NotCirculateNotice->office_id            = (int)$request->office_id;
            $NotCirculateNotice->designation_id       = (int)$request->designation_id;
            $NotCirculateNotice->notice_date          = $request->notice_date;
            $NotCirculateNotice->notice_time          = $request->notice_time;
            $NotCirculateNotice->org_id               = (int)$request->org_id;
            $NotCirculateNotice->notification_type_id = (int)$request->notification_type_id;
            $NotCirculateNotice->updated_by           = (int)user_id();
            $NotCirculateNotice->update();



            save_log([
                'data_id' => $NotCirculateNotice->id,
                'table_name' => 'not_circulate_notices',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $NotCirculateNotice
        ]);
    }

    /**
     * notification Circulate Notices status update
     */
    public function toggleStatus($id)
    {
        $NotCirculateNotice = NotCirculateNotice::find($id);

        if (!$NotCirculateNotice) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $NotCirculateNotice->status = $NotCirculateNotice->status ? 0 : 1;
        $NotCirculateNotice->update();

        save_log([
            'data_id' => $NotCirculateNotice->id,
            'table_name' => 'not_circulate_notices',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Not Circulate Notices toggle status updated successfully',
            'data'    => $NotCirculateNotice
        ]);
    }

    /**
     * Not Notification Circulate Notices destroy
     */
    public function destroy($id)
    {
        $NotCirculateNotice = NotCirculateNotice::find($id);

        if (!$NotCirculateNotice) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $NotCirculateNotice->delete();

        save_log([
            'data_id' => $id,
            'table_name' => 'not_circulate_notices',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
