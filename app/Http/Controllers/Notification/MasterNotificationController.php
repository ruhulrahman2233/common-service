<?php

namespace App\Http\Controllers\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\Notification\MasterNotification;
use App\Http\Validations\Notification\MasterNotificationValidation;

class MasterNotificationController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  master notofication
     */
    public function index(Request $request)
    {
        $query = MasterNotification::select("*");

        if ($request->not_type_name) {
            $query = $query->where('not_type_name', 'like', "{$request->not_type_name}%")
                            ->orWhere('not_type_name_bn', 'like', "{$request->not_type_name}%");
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('not_type_name', 'ASC')->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Notification list',
            'data' => $list
        ]);
    }

    /**
     * notifation store
     */
    public function store(Request $request)
    {
        $validationResult = MasterNotificationValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $notification = new MasterNotification();
            $notification->not_type_name    = $request->not_type_name;
            $notification->not_type_name_bn = $request->not_type_name_bn;
            $notification->created_by       = (int)user_id();
            $notification->updated_by       = (int)user_id();
            $notification->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $notification->id,
                'table_name' => 'master_notification_types',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $notification
        ]);
    }

    /**
     * notification update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterNotificationValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $notification = MasterNotification::find($id);

        if (!$notification) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $notification->not_type_name    = $request->not_type_name;
            $notification->not_type_name_bn = $request->not_type_name_bn;
            $notification->updated_by      = (int)user_id();
            $notification->update();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $notification->id,
                'table_name' => 'master_notification_types',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $notification
        ]);
    }

    /**
     * notification status update
     */
    public function toggleStatus($id)
    {
        $notification = MasterNotification::find($id);

        if (!$notification) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $notification->status = $notification->status ? 0 : 1;
        $notification->update();

        save_log([
            'data_id' => $notification->id,
            'table_name' => 'master_notification_types',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $notification
        ]);
    }

    /**
     * Notification destroy
     */
    public function destroy($id)
    {
        $notification = MasterNotification::find($id);

        if (!$notification) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $notification->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_notification_types',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
