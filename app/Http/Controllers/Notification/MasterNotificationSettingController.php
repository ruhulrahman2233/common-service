<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Models\Notification\MasterNotificationSetting;
use App\Http\Validations\Notification\MasterNotificationSettingValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterNotificationSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  master notofication setting
     */
    public function index(Request $request)
    {
        $query = DB::table('master_notification_settings')
                        ->join('master_org_profiless','master_notification_settings.org_id', '=','master_org_profiless.id')
                        ->join('master_components','master_notification_settings.component_id', '=','master_components.id')
                        ->join('master_modules','master_notification_settings.module_id', '=','master_modules.id')
                        ->Leftjoin('master_services','master_notification_settings.service_id', '=','master_services.id')
                        ->join('master_menus','master_notification_settings.menu_id', '=','master_menus.id')
                        ->join('master_notification_types','master_notification_settings.notification_type_id', '=','master_notification_types.id')
                        ->select('master_notification_settings.*',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                    'master_components.component_name','master_components.component_name_bn',
                                    'master_modules.module_name','master_modules.module_name_bn',
                                    'master_services.service_name','master_services.service_name_bn',
                                    'master_menus.menu_name','master_menus.menu_name_bn',
                                    'master_notification_types.not_type_name','master_notification_types.not_type_name_bn');

        if ($request->org_id) {
            $query = $query->where('master_notification_settings.org_id', $request->org_id);
        }

        if ($request->component_id) {
            $query = $query->where('master_notification_settings.component_id', $request->component_id);
        }

        if ($request->module_id) {
            $query = $query->where('master_notification_settings.module_id', $request->module_id);
        }

        if ($request->service_id) {
            $query = $query->where('master_notification_settings.service_id', $request->service_id);
        }

        if ($request->menu_id) {
            $query = $query->where('master_notification_settings.menu_id', $request->menu_id);
        }

        if ($request->notification_type_id) {
            $query = $query->where('master_notification_settings.notification_type_id', $request->notification_type_id);
        }

        if ($request->status) {
            $query = $query->where('master_notification_settings.status', $request->status);
        }



        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_components.component_name', 'ASC')
                        ->orderBy('master_modules.module_name', 'ASC')
                        ->orderBy('master_services.service_name', 'ASC')
                        ->orderBy('master_menus.menu_name', 'ASC')
                        ->orderBy('master_notification_types.not_type_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Notification Setting list',
            'data' => $list
        ]);
    }

    /**
     * notifation setting store
     */
    public function store(Request $request)
    {
        $validationResult = MasterNotificationSettingValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $notSetting                       = new MasterNotificationSetting();
            $notSetting->org_id               = (int)$request->org_id;
            $notSetting->component_id         = (int)$request->component_id;
            $notSetting->module_id            = (int)$request->module_id;
            $notSetting->service_id           = (int)$request->service_id;
            $notSetting->menu_id              = (int)$request->menu_id;
            $notSetting->notification_type_id = (int)$request->notification_type_id;
            $notSetting->created_by           = (int)user_id();
            $notSetting->updated_by           = (int)user_id();
            $notSetting->save();

            save_log([
                'data_id' => $notSetting->id,
                'table_name' => 'master_notification_settings',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $notSetting
        ]);
    }

    /**
     * notification setting update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterNotificationSettingValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $notSetting = MasterNotificationSetting::find($id);

        if (!$notSetting) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $notSetting->org_id               = (int)$request->org_id;
            $notSetting->component_id         = (int)$request->component_id;
            $notSetting->module_id            = (int)$request->module_id;
            $notSetting->service_id           = (int)$request->service_id;
            $notSetting->menu_id              = (int)$request->menu_id;
            $notSetting->notification_type_id = (int)$request->notification_type_id;
            $notSetting->updated_by           = (int)user_id();
            $notSetting->update();

            save_log([
                'data_id' => $notSetting->id,
                'table_name' => 'master_notification_settings',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $notSetting
        ]);
    }

    /**
     * notification setting status update
     */
    public function toggleStatus($id)
    {
        $notSetting = MasterNotificationSetting::find($id);

        if (!$notSetting) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $notSetting->status = $notSetting->status ? 0 : 1;
        $notSetting->update();

        save_log([
            'data_id' => $notSetting->id,
            'table_name' => 'master_notification_settings',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $notSetting
        ]);
    }

    /**
     * Notification setting destroy
     */
    public function destroy($id)
    {
        $notSetting = MasterNotificationSetting::find($id);

        if (!$notSetting) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $notSetting->delete();

        save_log([
            'data_id' => $notSetting->id,
            'table_name' => 'master_notification_settings',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
