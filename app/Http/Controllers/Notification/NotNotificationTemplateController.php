<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Validations\Notification\NotNotificationTemplateValidation;
use App\Models\Notification\NotNotificationTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotNotificationTemplateController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  notofication template
     */
    public function index(Request $request)
    {
        $query = DB::table('not_notification_templates')
                        ->join('master_org_profiless','not_notification_templates.org_id', '=','master_org_profiless.id')
                        ->join('master_components','not_notification_templates.component_id', '=','master_components.id')
                        ->join('master_modules','not_notification_templates.module_id', '=','master_modules.id')
                        ->Leftjoin('master_services','not_notification_templates.service_id', '=','master_services.id')
                        ->join('master_menus','not_notification_templates.menu_id', '=','master_menus.id')
                        ->join('master_notification_types','not_notification_templates.notification_type_id', '=','master_notification_types.id')
                        ->select('not_notification_templates.*',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                    'master_components.component_name','master_components.component_name_bn',
                                    'master_modules.module_name','master_modules.module_name_bn',
                                    'master_services.service_name','master_services.service_name_bn',
                                    'master_menus.menu_name','master_menus.menu_name_bn',
                                    'master_notification_types.not_type_name','master_notification_types.not_type_name_bn');

        if ($request->notification_type_id) {
            $query = $query->where('not_notification_templates.notification_type_id', $request->notification_type_id);
        }

        if ($request->title) {
            $query = $query->where('title', 'like', "{$request->title}%")
                            ->orWhere('title_bn', 'like', "{$request->title}%");
        }

        if ($request->message) {
            $query = $query->where('message', 'like', "{$request->message}%")
                            ->orWhere('message_bn', 'like', "{$request->message}%");
        }

        if ($request->org_id) {
            $query = $query->where('not_notification_templates.org_id', $request->org_id);
        }

        if ($request->component_id) {
            $query = $query->where('not_notification_templates.component_id', $request->component_id);
        }

        if ($request->module_id) {
            $query = $query->where('not_notification_templates.module_id', $request->module_id);
        }

        if ($request->service_id) {
            $query = $query->where('not_notification_templates.service_id', $request->service_id);
        }

        if ($request->menu_id) {
            $query = $query->where('not_notification_templates.menu_id', $request->menu_id);
        }

        if ($request->status) {
            $query = $query->where('not_notification_templates.status', $request->status);
        }



        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_components.component_name', 'ASC')
                        ->orderBy('master_modules.module_name', 'ASC')
                        ->orderBy('master_services.service_name', 'ASC')
                        ->orderBy('master_menus.menu_name', 'ASC')
                        ->orderBy('master_notification_types.not_type_name', 'ASC')
                        ->orderBy('not_notification_templates.title', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Notification Setting list',
            'data' => $list
        ]);
    }

    /**
     * notifation template store
     */
    public function store(Request $request)
    {
        $validationResult = NotNotificationTemplateValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $notNotificationTem                       = new NotNotificationTemplate();
            $notNotificationTem->notification_type_id = (int)$request->notification_type_id;
            $notNotificationTem->title                = $request->title;
            $notNotificationTem->title_bn             = $request->title_bn;
            $notNotificationTem->message              = $request->message;
            $notNotificationTem->message_bn           = $request->message_bn;
            $notNotificationTem->org_id               = (int)$request->org_id;
            $notNotificationTem->component_id         = (int)$request->component_id;
            $notNotificationTem->module_id            = (int)$request->module_id;
            $notNotificationTem->service_id           = (int)$request->service_id;
            $notNotificationTem->menu_id              = (int)$request->menu_id;
            $notNotificationTem->created_by           = (int)user_id();
            $notNotificationTem->updated_by           = (int)user_id();
            $notNotificationTem->save();

            save_log([
                'data_id' => $notNotificationTem->id,
                'table_name' => 'not_notification_templates',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $notNotificationTem
        ]);
    }

    /**
     * notification template update
     */
    public function update(Request $request, $id)
    {
        $validationResult = NotNotificationTemplateValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $notNotificationTem = NotNotificationTemplate::find($id);

        if (!$notNotificationTem) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $notNotificationTem->notification_type_id = (int)$request->notification_type_id;
            $notNotificationTem->title                = $request->title;
            $notNotificationTem->title_bn             = $request->title_bn;
            $notNotificationTem->message              = $request->message;
            $notNotificationTem->message_bn           = $request->message_bn;
            $notNotificationTem->org_id               = (int)$request->org_id;
            $notNotificationTem->component_id         = (int)$request->component_id;
            $notNotificationTem->module_id            = (int)$request->module_id;
            $notNotificationTem->service_id           = (int)$request->service_id;
            $notNotificationTem->menu_id              = (int)$request->menu_id;
            $notNotificationTem->updated_by           = (int)user_id();
            $notNotificationTem->update();

            save_log([
                'data_id' => $notNotificationTem->id,
                'table_name' => 'not_notification_templates',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $notNotificationTem
        ]);
    }

    /**
     * notification template status update
     */
    public function toggleStatus($id)
    {
        $notNotificationTem = NotNotificationTemplate::find($id);

        if (!$notNotificationTem) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $notNotificationTem->status = $notNotificationTem->status ? 0 : 1;
        $notNotificationTem->update();

        save_log([
            'data_id' => $notNotificationTem->id,
            'table_name' => 'not_notification_templates',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Not Notification toggle status updated successfully',
            'data'    => $notNotificationTem
        ]);
    }

    /**
     * Not Notification template destroy
     */
    public function destroy($id)
    {
        $notNotificationTem = NotNotificationTemplate::find($id);

        if (!$notNotificationTem) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $notNotificationTem->delete();

        save_log([
            'data_id' => $id,
            'table_name' => 'not_notification_templates',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
