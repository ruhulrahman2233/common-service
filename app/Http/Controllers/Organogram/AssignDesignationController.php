<?php

namespace App\Http\Controllers\Organogram;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organogram\AssignDesignation;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;
use Validator;
use DB;

class AssignDesignationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all assign designation
     */
    public function index(Request $request)
    {
        $query = DB::table('assign_designations')
                    ->join('master_org_profiless','assign_designations.org_id','=','master_org_profiless.id')
                    ->join('master_office_types','assign_designations.office_type_id','=','master_office_types.id')
                    ->join('master_offices','assign_designations.office_id','=','master_offices.id')
                    ->join('master_designations','assign_designations.designation_id','=','master_designations.id')
                    ->select("assign_designations.*",'master_org_profiless.org_name','master_office_types.office_type_name',
                            'master_offices.office_name','master_offices.office_name_bn','master_designations.designation','master_designations.designation_bn'
                    );

        if ($request->org_id) {
            $query = $query->where('assign_designations.org_id', $request->org_id);
        }

        if ($request->office_type_id) {
            $query = $query->where('assign_designations.office_type_id', $request->office_type_id);
        }

        if ($request->office_id) {
            $query = $query->where('assign_designations.office_id', $request->office_id);
        }

        if ($request->designation_id) {
            $query = $query->where('assign_designations.designation_id', $request->designation_id);
        }

        if ($request->status) {
            $query = $query->where('assign_designations.status', $request->status);
        }

        $list = $query->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Assign designation list',
            'data'    => $list
        ]);
    }
    
    /**
     * get all assign designation
     */
    public function listAll(Request $request)
    {
        $list = DB::table('assign_designations')
                    ->join('master_org_profiless','assign_designations.org_id','=','master_org_profiless.id')
                    ->join('master_office_types','assign_designations.office_type_id','=','master_office_types.id')
                    ->join('master_offices','assign_designations.office_id','=','master_offices.id')
                    ->join('master_designations','assign_designations.designation_id','=','master_designations.id')
                    ->select("assign_designations.*")
                    ->get();
        return response([
            'success' => true,
            'message' => 'Master assign designation list-all',
            'data'    => $list
        ]);
    }



    /**
     * assign designation store
     */
    public function store(Request $request)
    {
        $org_id     = $request->org_id;
        $office_id  = $request->office_id;
        $validator  = Validator::make($request->all(), [
            'designation_id' => [
                'required',
                Rule::unique('assign_designations')->where(function ($query) use($office_id, $org_id) {
                    return $query->where('office_id', $office_id)
                                 ->where('org_id', $org_id);
                }),
            ],
            'org_id'    => 'required',
            'office_id' => 'required',
            'office_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        try {
            $ad = new AssignDesignation();
            $ad->org_id         = $request->org_id;
            $ad->office_type_id = $request->office_type_id;
            $ad->office_id      = $request->office_id;
            $ad->designation_id = $request->designation_id;
            $ad->approved_post  = $request->approved_post;
            $ad->sorting_order  = $request->sorting_order;
            $ad->created_by     = (int)$request->created_by;
            $ad->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $ad->id,
                'table_name'     => 'assign_designations'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => $ex->getMessage()
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $ad
        ]);
    }

    /**
     * assign designation update
     */
    public function update(Request $request, $id)
    {
        $org_id     = $request->org_id;
        $office_id  = $request->office_id;
        $validator  = Validator::make($request->all(), [
            'designation_id' => [
                'required',
                Rule::unique('assign_designations')->where(function ($query) use($office_id, $org_id, $id) {
                    return $query->where('office_id', $office_id)
                                 ->where('org_id', $org_id)
                                 ->where('id', '!=', $id);
                }),
            ],
            'org_id'    => 'required',
            'office_id' => 'required',
            'office_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        $ad = AssignDesignation::find($id);

        if (!$ad) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $ad->org_id         = $request->org_id;
            $ad->office_type_id = $request->office_type_id;
            $ad->office_id      = $request->office_id;
            $ad->designation_id = $request->designation_id;
            $ad->approved_post  = $request->approved_post;
            $ad->sorting_order  = $request->sorting_order;
            $ad->updated_by     = (int)$request->updated_by;
            $ad->update();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $ad->id,
                'table_name'     => 'assign_designations',
                'execution_type' => 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => $ex->getMessage()
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $ad
        ]);
    }

    /**
     * assign designation status update
     */
    public function toggleStatus($id)
    {
        $ad = AssignDesignation::find($id);

        if (!$ad) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $ad->status = $ad->status ? 0 : 1;
        $ad->update();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $ad->id,
            'table_name'     => 'assign_designations',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $ad
        ]);
    }

    /**
     * assign designation destroy
     */
    public function destroy($id)
    {
        $ad = AssignDesignation::find($id);

        if (!$ad) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $ad->delete();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $id,
            'table_name'     => 'assign_designations',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
