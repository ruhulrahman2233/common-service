<?php

namespace App\Http\Controllers\Organogram;

use DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\Organogram\MasterDesignation;

class MasterDesignationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master designation
     */
    public function index(Request $request)
    {
        $query = DB::table('master_designations')
                    ->join('master_org_profiless','master_designations.org_id','=','master_org_profiless.id')
                    ->join('master_grades','master_designations.grade_id','=','master_grades.id')
                    ->select("master_designations.*",
                    'master_org_profiless.org_name',
                    'master_org_profiless.org_name_bn',
                    'master_grades.grade_name',
                    'master_grades.grade_name_bn');

        if ($request->designation) {
            $query = $query->where('designation', 'like', "{$request->designation}%")
                            ->orWhere('designation_bn', 'like', "{$request->designation}%");
        }

        if ($request->org_id) {
            $query = $query->where('master_designations.org_id', $request->org_id);
        }

        if ($request->direct_post) {
            $query = $query->where('direct_post', $request->direct_post);
        }

        if ($request->promotional_post) {
            $query = $query->where('promotional_post', $request->promotional_post);
        }

        if ($request->total_post) {
            $query = $query->where('total_post', $request->total_post);
        }

        if ($request->grade_id) {
            $query = $query->where('master_designations.grade_id', $request->grade_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        

        $list = $query->orderBy('master_designations.sorting_order', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Master designation list',
            'data' => $list
        ]);
    }

    public function listAll(Request $request)
    {
        $query = DB::table('master_designations')
                    ->join('master_org_profiless','master_designations.org_id','=','master_org_profiless.id')
                    ->join('master_grades','master_designations.grade_id','=','master_grades.id')
                    ->select("master_designations.*",
                    'master_org_profiless.org_name',
                    'master_org_profiless.org_name_bn',
                    'master_grades.grade_name',
                    'master_grades.grade_name_bn');

        if ($request->designation) {
            $query = $query->where('designation', 'like', "{$request->designation}%")
                            ->orWhere('designation_bn', 'like', "{$request->designation}%");
        }

        if ($request->org_id) {
            $query = $query->where('master_designations.org_id', $request->org_id);
        }

        if ($request->direct_post) {
            $query = $query->where('direct_post', $request->direct_post);
        }

        if ($request->promotional_post) {
            $query = $query->where('promotional_post', $request->promotional_post);
        }

        if ($request->total_post) {
            $query = $query->where('total_post', $request->total_post);
        }

        if ($request->grade_id) {
            $query = $query->where('master_designations.grade_id', $request->grade_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        

        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_designations.designation', 'ASC')->get();

        return response([
            'success' => true,
            'message' => 'Master designation list',
            'data' => $list
        ]);
    }

    /**
     * master designation store
     */
    public function store(Request $request)
    {
        $designation    = $request->designation;
        $org_id         = $request->org_id;
        $validator = Validator::make($request->all(), [
            'designation' => [
                'required',
                Rule::unique('master_designations')->where(function ($query) use($designation, $org_id) {
                    return $query->where('designation', $designation)
                                 ->where('org_id', $org_id);
                }),
            ],
            'designation_bn'  => 'required',
            'org_id'           => 'required',
            'direct_post' => 'required',
            'promotional_post' => 'required',
            'total_post' => 'required',
            'grade_id' => 'required',
            'sorting_order' => 'required',
        ]);

        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }

        try {
            $md = new MasterDesignation();
            $md->designation        = $request->designation;
            $md->designation_bn     = $request->designation_bn;
            $md->org_id             = $request->org_id;
            $md->direct_post        = $request->direct_post;
            $md->promotional_post   = $request->promotional_post;
            $md->total_post         = $request->total_post;
            $md->grade_id           = $request->grade_id;
            $md->sorting_order      = $request->sorting_order;
            $md->created_by         = (int)$request->created_by;
            $md->save();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $md->id,
                'table_name'     => 'master_designations',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => $ex->getMessage()
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $md
        ]);
    }

    /**
     * master designation update
     */
    public function update(Request $request, $id)
    {
        $designation    = $request->designation;
        $org_id         = $request->org_id;
        $validator = Validator::make($request->all(), [
            'designation' => [
                'required',
                Rule::unique('master_designations')->where(function ($query) use($designation, $org_id, $id) {
                    return $query->where('designation', $designation)
                                 ->where('org_id', $org_id)
                                 ->where('id', '!=', $id);
                }),
            ],
            'designation_bn'  => 'required',
            'org_id'           => 'required',
            'direct_post' => 'required',
            'promotional_post' => 'required',
            'total_post' => 'required',
            'grade_id' => 'required',
            'sorting_order' => 'required',
        ]);

        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        $md = MasterDesignation::find($id);

        if (!$md) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $md->designation        = $request->designation;
            $md->designation_bn     = $request->designation_bn;
            $md->org_id             = $request->org_id;
            $md->direct_post        = $request->direct_post;
            $md->promotional_post   = $request->promotional_post;
            $md->total_post         = $request->total_post;
            $md->grade_id           = $request->grade_id;
            $md->sorting_order      = $request->sorting_order;
            $md->updated_by         = (int)$request->updated_by;
            $md->update();

            Cache::forget('commonDropdown');

            save_log([
                'data_id'        => $md->id,
                'table_name'     => 'master_designations',
                'execution_type' => 1
            ]);
        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => $ex->getMessage()
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $md
        ]);
    }

    /**
     * master designation status update
     */
    public function toggleStatus($id)
    {
        $md = MasterDesignation::find($id);

        if (!$md) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $md->status = $md->status ? 0 : 1;
        $md->update();

        Cache::forget('commonDropdown');

        save_log([
            'data_id'        => $md->id,
            'table_name'     => 'master_designations',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $md
        ]);
    }

    /**
     * master designation destroy
     */
    public function destroy($id)
    {
        $md = MasterDesignation::find($id);

        if (!$md) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $md->delete();

        Cache::forget('commonDropdown');
        
        save_log([
            'data_id'        => $id,
            'table_name'     => 'master_designations',
            'execution_type' => 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
