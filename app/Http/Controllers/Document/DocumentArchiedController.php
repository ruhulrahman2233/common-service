<?php

namespace App\Http\Controllers\Document;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Document\AddDocument;
use DB;

class DocumentArchiedController extends Controller
{
    /**
     * Archived get data list
     */
    public function index()
    {
        // $archivedData= AddDocument::where('status', 2);
        $archivedData = DB::table('doc_doc_infos')
                        ->join('master_document_categories','doc_doc_infos.category_id', '=','master_document_categories.id')
                        ->join('master_org_profiless','doc_doc_infos.org_id', '=','master_org_profiless.id')
                        ->select('doc_doc_infos.*',
                                    'master_document_categories.category_name','master_document_categories.category_name_bn',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn')->where('doc_doc_infos.status',2);

        if(!$archivedData) {
            return response()->json([
                'success'=>false,
                 'message'=>'Data Not Found'
            ]);
        }


        $list = $archivedData->orderBy('master_org_profiless.org_name', 'ASC')
                                ->orderBy('master_document_categories.category_name', 'ASC')
                                ->orderBy('doc_doc_infos.doc_title', 'ASC')
                                ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Archived  list',
            'data' => $list
        ]);

    }

}
