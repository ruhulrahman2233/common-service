<?php

namespace App\Http\Controllers\Document;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Document\AddDocumentValidation;
use App\Models\Document\AddDocument;
use App\Helpers\GlobalFileUploadFunctoin;
use Illuminate\Support\Facades\DB;

class AddDocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  Document
     */
    public function index(Request $request)
    {
        $query = DB::table('doc_doc_infos')
                        ->join('master_org_profiless','doc_doc_infos.org_id', '=','master_org_profiless.id')
                        ->join('master_document_categories','doc_doc_infos.category_id', '=','master_document_categories.id')
                        ->select('doc_doc_infos.*',
                                    'master_org_profiless.org_name','master_org_profiless.org_name_bn',
                                    'master_document_categories.category_name','master_document_categories.category_name_bn');

        if ($request->doc_title) {
            $query = $query->where('doc_title', 'like', "{$request->doc_title}%")
                            ->orWhere('doc_title_bn', 'like', "{$request->doc_title}%");
        }

        if ($request->org_id) {
            $query = $query->where('org_id', $request->org_id);
        }

        if ($request->category_id) {
            $query = $query->where('category_id', $request->category_id);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }



        $list = $query->orderBy('master_org_profiless.org_name', 'ASC')
                        ->orderBy('master_document_categories.category_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Document list',
            'data' => $list
        ]);
    }

    /**
     * Document  Store
     */
    public function store(Request $request)
    {
        $validationResult = AddDocumentValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }
        $file_path 		= 'document';
        $attachment 	=  $request->file('attachment');

        try {
            DB::beginTransaction();

            $addDocument                   = new AddDocument();
            $addDocument->doc_title        = $request->doc_title;
            $addDocument->doc_title_bn     = $request->doc_title_bn;
            $addDocument->org_id           = $request->org_id;
            $addDocument->category_id      = $request->category_id;
            $addDocument->created_by       = (int)user_id();
            $addDocument->updated_by       = (int)user_id();

            if($attachment !=null && $attachment !=""){
                $attachment_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path,'attachment');
            }

            $addDocument->attachment   =  $attachment_name ? $attachment_name : null;

            if( $addDocument->save()) {
                GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment_name);
            }

            save_log([
                'data_id' => $addDocument->id,
                'table_name' => 'doc_doc_infos',
            ]);

            DB::commit();

        } catch (\Exception $ex) {

            DB::rollBack();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $addDocument
        ]);
    }

    /**
     *  Document  Update
     */
    public function update(Request $request, $id)
    {
        $validationResult = AddDocumentValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $file_path 		= 'document';
        $attachment 	=  $request->file('attachment');

        $addDocument = AddDocument::find($id);
        $old_file 		= $addDocument->attachment;

        if (!$addDocument) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $addDocument->doc_title        = $request->doc_title;
            $addDocument->doc_title_bn     = $request->doc_title_bn;
            $addDocument->org_id           = $request->org_id;
            $addDocument->category_id      = $request->category_id;
            $addDocument->updated_by       = (int)user_id();

            if($attachment != null && $attachment != ""){
                $attachment = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request,$file_path,'attachment');
				$addDocument->attachment    =  $attachment;
            }

			if($addDocument->update()){
				 GlobalFileUploadFunctoin::file_upload($request, $file_path, 'attachment', $attachment, $old_file );
			}

            save_log([
                'data_id' => $addDocument->id,
                'table_name' => 'doc_doc_infos',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $addDocument
        ]);
    }

    /**
     *  Document  status
     */
    public function toggleStatus($id)
    {
        $addDocument = AddDocument::find($id);

        if (!$addDocument) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $addDocument->status = $addDocument->status ? 0 : 1;
        $addDocument->update();

        save_log([
            'data_id' => $addDocument->id,
            'table_name' => 'doc_doc_infos',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $addDocument
        ]);
    }

    /**
     *  Archive Toggle Status
     */
    public function archivetoggleStatus($id)
    {
        $addDocument = AddDocument::find($id);

        if (!$addDocument) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $addDocument->status = $addDocument->status ? : 2;
        $addDocument->update();

        save_log([
            'data_id' => $addDocument->id,
            'table_name' => 'doc_doc_infos',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $addDocument
        ]);
    }

    /**
     * Document  Delete
     */
    public function destroy($id)
    {
        $addDocument = AddDocument::find($id);

        if (!$addDocument) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $addDocument->delete();

        save_log([
            'data_id' => $id,
            'table_name' => 'doc_doc_infos',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
