<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Document\AddDocument;
use App\Models\Document\MasterDocumentCategory;

class DocumentIndexingController extends Controller
{
    /**
     * ALL Category List
     */

     public function index()
     {
         $categoryList = MasterDocumentCategory::select('id','category_name','category_name_bn')->get();

         if(!$categoryList) {
            return response()->json([
                'success'=>false,
                 'messgae'=>'Data Not Found'
            ]);
         } else {
            return response()->json([
                 'success' =>true,
                 'data'=>$categoryList
            ]);
         }

     }

    /**
      * document Indexing show
    */

    public function getDocumentData(Request $request)
    {
        // return $request;
        if($request->org_id){
            $query = DB::table('doc_doc_infos')
            ->join('master_document_categories','doc_doc_infos.category_id', '=','master_document_categories.id')
            ->join('master_org_profiless','doc_doc_infos.org_id', '=','master_org_profiless.id')
            ->select('doc_doc_infos.*',
                'master_document_categories.category_name','master_document_categories.category_name_bn',
                'master_org_profiless.org_name','master_org_profiless.org_name_bn')->whereIn('doc_doc_infos.category_id', $request->category_id)
                ->where('doc_doc_infos.org_id', $request->org_id)
                ->orderBy('master_org_profiless.org_name', 'ASC')
                ->orderBy('master_document_categories.category_name', 'ASC')
                ->orderBy('doc_doc_infos.doc_title', 'ASC')
                ->get();

            return response([
                'success' => true,
                'message' => 'Archived  list',
                'data' => $query
            ]);
        }
    }
}
