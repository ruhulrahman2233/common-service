<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\Document\MasterDocumentCategory;
use App\Http\Validations\Document\MasterDocumentCategoryValidation;

class MasterDocumentCategoryController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all  Master Document Category
     */
    public function index(Request $request)
    {
        $query = MasterDocumentCategory::select("*");

        if ($request->category_name) {
            $query = $query->where('category_name', 'like', "{$request->category_name}%")
                            ->orWhere('category_name_bn', 'like', "{$request->category_name}%");
        }

        if ($request->sorting_order) {
            $query = $query->where('sorting_order', $request->sorting_order);
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        $list = $query->orderBy('category_name', 'ASC')->paginate(request('per_page', config('app.per_page')));

        return response([
            'success' => true,
            'message' => 'Document list',
            'data' => $list
        ]);
    }

    /**
     * Master Document Category Store
     */
    public function store(Request $request)
    {
        $validationResult = MasterDocumentCategoryValidation:: validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $MasDocCategory                   = new MasterDocumentCategory();
            $MasDocCategory->category_name    = $request->category_name;
            $MasDocCategory->category_name_bn = $request->category_name_bn;
            $MasDocCategory->sorting_order    = (int)$request->sorting_order;
            $MasDocCategory->created_by       = (int)user_id();
            $MasDocCategory->updated_by       = (int)user_id();
            $MasDocCategory->save();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $MasDocCategory->id,
                'table_name' => 'master_document_categories',
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $MasDocCategory
        ]);
    }

    /**
     * Master Document Category Update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterDocumentCategoryValidation:: validate($request ,$id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $MasDocCategory = MasterDocumentCategory::find($id);

        if (!$MasDocCategory) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $MasDocCategory->category_name    = $request->category_name;
            $MasDocCategory->category_name_bn = $request->category_name_bn;
            $MasDocCategory->sorting_order    = (int)$request->sorting_order;
            $MasDocCategory->updated_by       = (int)user_id();
            $MasDocCategory->update();

            Cache::forget('dropdown_common_config');

            save_log([
                'data_id' => $MasDocCategory->id,
                'table_name' => 'master_document_categories',
                'execution_type' => 1,
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $MasDocCategory
        ]);
    }

    /**
     * Master Document Category status
     */
    public function toggleStatus($id)
    {
        $MasDocCategory = MasterDocumentCategory::find($id);

        if (!$MasDocCategory) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $MasDocCategory->status = $MasDocCategory->status ? 0 : 1;
        $MasDocCategory->update();

        save_log([
            'data_id' => $MasDocCategory->id,
            'table_name' => 'master_document_categories',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $MasDocCategory
        ]);
    }

    /**
     * Master Document Category Delete
     */
    public function destroy($id)
    {
        $MasDocCategory = MasterDocumentCategory::find($id);

        if (!$MasDocCategory) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $MasDocCategory->delete();

        Cache::forget('dropdown_common_config');

        save_log([
            'data_id' => $id,
            'table_name' => 'master_document_categories',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }

}
