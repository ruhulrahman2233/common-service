<?php

namespace App\Http\Controllers\Wards;

use DB;
use Illuminate\Http\Request;
use App\Models\Wards\MasterWard;
use App\Http\Controllers\Controller;
use App\Models\Wards\MasterWardDetail;
use App\Http\Validations\Wards\MasterWardsValidations;

class MasterWardsController extends Controller
{	

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get all Seeds Stock
     */
    public function index(Request $request)
    {
        $query = MasterWard::with('masterWardDetails');
        
        if ($request->type) {
            $query = $query->where('type', $request->type);
        }

        if ($request->city_corporation_id) {
            $query = $query->where('city_corporation_id', $request->city_corporation_id);
        }

        if ($request->division_id) {
            $query = $query->where('division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query = $query->where('district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('upazilla_id', $request->upazilla_id);
        }

        if ($request->union_id) {
            $query = $query->where('union_id', $request->union_id);
        }

        if ($request->pauroshoba_id) {
            $query = $query->where('pauroshoba_id', $request->pauroshoba_id);
        }

        // $query->get();

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if( count($list)>0){
            return response([
                'success' => true,
                'message' => 'Seeds stock list',
                'data' => $list
            ]);
        }
        else
        {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }
    }

    // public function index(Request $request)
    // {
    //     $datas = MasterWard::with('masterWardDetails')->get();

    //     return $datas;
    // }

    /**
     * details
     */
    public function show($id)
    {
        $masterWards = MasterWard::find($id);

        if(!$masterWards){
        	return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);          
        }
        else
        {  
            return response([
                'success' => true,
                'message' => 'Ward details',
                'data' => $masterWards
            ]);
        }        
    }
    
    /**
     * Seeds Stock store
     */
    public function store(Request $request)
    {  
        $validationResult = MasterWardsValidations::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterWards = new MasterWard();

        $masterWards->type 					= (int)$request->type; 
		$masterWards->city_corporation_id 		= (int)$request->city_corporation_id ?? null;  	
		$masterWards->division_id 			= (int)$request->division_id ?? null;  	
		$masterWards->district_id 			= (int)$request->district_id ?? null;  	
		$masterWards->upazilla_id 			= (int)$request->upazilla_id ?? null; 	
		$masterWards->union_id 				= (int)$request->union_id ?? null; 	
		$masterWards->pauroshoba_id 			= (int)$request->pauroshoba_id ?? null;
        $masterWards->created_by     		= (int)user_id();
	    $masterWards->updated_by     		= (int)user_id();
        $masterWards->save();
        $master_ward_id = $masterWards->id;

        try {
        	foreach ($request->master_ward_details as $value) {
	        		$MasterWardDetails                  		= new MasterWardDetail();
					$MasterWardDetails->master_ward_id 			= $master_ward_id; 	
					$MasterWardDetails->ward_name 			= $value['ward_name']; 	
					$MasterWardDetails->ward_name_bn 		= $value['ward_name_bn'];			
					$MasterWardDetails->save();       		
		            
		            save_log([
		                'data_id'    => $masterWards->id,
		                'table_name' => 'master_wards'
		            ]);		            
        	}		         


        } catch (\Exception $ex) {

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterWards
        ]);
    }


     /**
     * Seeds Stock Update
     */
    public function update(Request $request, $id)
    {   
        // return $request;
        $validationResult = MasterWardsValidations::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        } 

	    MasterWardDetail::where('master_ward_id', $id)->delete();
 
        try {
            $masterWards = MasterWard::find($id);
            $masterWards->type 					= (int)$request->type; 
            $masterWards->city_corporation_id 		= (int)$request->city_corporation_id ?? null;  	
            $masterWards->division_id 			= (int)$request->division_id ?? null;  	
            $masterWards->district_id 			= (int)$request->district_id ?? null;  	
            $masterWards->upazilla_id 			= (int)$request->upazilla_id ?? null; 	
            $masterWards->union_id 				= (int)$request->union_id ?? null; 	
            $masterWards->pauroshoba_id 			= (int)$request->pauroshoba_id ?? null;
            $masterWards->created_by     		= (int)user_id();
            $masterWards->updated_by     		= (int)user_id();
            $masterWards->save();

	        	foreach ($request->master_ward_details as $value) {
	           	 	$masterWards                  		= new MasterWardDetail();
					$masterWards->master_ward_id 		= $id;
					$masterWards->ward_name 			= $value['ward_name'];
					$masterWards->ward_name_bn 			= $value['ward_name_bn']; 			
					$masterWards->save(); 
		            
		            save_log([
		                'data_id'    => $masterWards->id,
		                'table_name' => 'master_wards'
		            ]);
				} 

        } catch (\Exception $ex) {
            
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data Update successfully',
            'data'    => $masterWards
        ]);
    }

     /**
     * status update
     */
    public function toggleStatus($id)
    {
        $masterWards = MasterWard::find($id);

        if (!$masterWards) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterWards->status = $masterWards->status == 1 ? 0 : 1;
        $masterWards->update();

        save_log([
            'data_id'       => $masterWards->id,
            'table_name'    => 'master_wards',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterWards
        ]);
    }    

    /**
     *  destroy
    */
    public function destroy($id)
    {
        $masterWards = MasterWard::find($id);

        if (!$masterWards) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        $masterWards->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_wards',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
    
}
