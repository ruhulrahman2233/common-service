<?php

namespace App\Http\Controllers\Pauroshoba;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\Pauroshoba\MasterPauroshobasValidations;
use App\Models\Pauroshoba\MasterPauroshobas;
use DB;

class MasterPauroshobasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master Pauroshoba 
     */
    public function index(Request $request)
    {  
        $query = DB::table('master_pauroshobas') 
                        ->join('master_divisions','master_pauroshobas.division_id', '=','master_divisions.id')
                        ->join('master_districts','master_pauroshobas.district_id', '=','master_districts.id')
                        ->join('master_upazillas','master_pauroshobas.upazilla_id', '=','master_upazillas.id')
                        ->select('master_pauroshobas.*',
                            'master_divisions.division_name','master_divisions.division_name_bn',
                            'master_districts.district_name','master_districts.district_name_bn',
                            'master_upazillas.upazilla_name','master_upazillas.upazilla_name_bn');
  
        if ($request->pauroshoba_name) {
            $query = $query->where('master_pauroshobas.pauroshoba_name', 'like', "{$request->pauroshoba_name}%")
                            ->orWhere('master_pauroshobas.pauroshoba_name_bn', 'like', "{$request->pauroshoba_name}%");
        }

        if ($request->division_id) {
            $query = $query->where('master_pauroshobas.division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query = $query->where('master_pauroshobas.district_id', $request->district_id);
        }

        if ($request->upazilla_id) {
            $query = $query->where('master_pauroshobas.upazilla_id', $request->upazilla_id);
        }      

        if ($request->status) {
            $query = $query->where('master_pauroshobas.status', $request->status);
        }     

        $list = $query->paginate(request('per_page', config('app.per_page')));

        if(count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'Pauroshoba Info list',
	            'data' => $list
	        ]);
        }
        else
        {
        	return response([
	            'success' => false,
	            'message' => 'Data not found!!'
	        ]);
        }        
    }

    /**
     * master Pauroshoba show
     */
    public function show($id)
    {
        $masterPauroshobas = MasterPauroshobas::find($id);

        if (!$masterPauroshobas) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Pauroshobas details',
            'data'    => $masterPauroshobas
        ]);
    }

    /**
     * master Pauroshoba store
     */
    public function store(Request $request)
    {
        $validationResult = MasterPauroshobasValidations:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $masterPauroshobas 						= new MasterPauroshobas();
            $masterPauroshobas->pauroshoba_name     = $request->pauroshoba_name;
            $masterPauroshobas->pauroshoba_name_bn  = $request->pauroshoba_name_bn;
            $masterPauroshobas->division_id    	 	= (int)$request->division_id;
            $masterPauroshobas->district_id    		= (int)$request->district_id;
            $masterPauroshobas->upazilla_id    		= (int)$request->upazilla_id;
            $masterPauroshobas->created_by     		= (int)user_id();
            $masterPauroshobas->updated_by     		= (int)user_id();
            $masterPauroshobas->save();

            save_log([
                'data_id'    => $masterPauroshobas->id,
                'table_name' => 'master_pauroshobas'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterPauroshobas
        ]);
    }

    /**
     * master Pauroshoba update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterPauroshobasValidations:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterPauroshobas = MasterPauroshobas::find($id);

        if (!$masterPauroshobas) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $masterPauroshobas->pauroshoba_name     = $request->pauroshoba_name;
            $masterPauroshobas->pauroshoba_name_bn  = $request->pauroshoba_name_bn;
            $masterPauroshobas->division_id    	 	= (int)$request->division_id;
            $masterPauroshobas->district_id    		= (int)$request->district_id;
            $masterPauroshobas->upazilla_id    		= (int)$request->upazilla_id;
            $masterPauroshobas->updated_by     = (int)user_id();
            $masterPauroshobas->update();

            save_log([
                'data_id'       => $masterPauroshobas->id,
                'table_name'    => 'master_pauroshobas',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterPauroshobas
        ]);
    }

    /**
     * master Pauroshoba status update
     */
    public function toggleStatus($id)
    {
        $masterPauroshobas = MasterPauroshobas::find($id);

        if (!$masterPauroshobas) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterPauroshobas->status = $masterPauroshobas->status == 1 ? 0 : 1;
        $masterPauroshobas->update();

        save_log([
            'data_id'       => $masterPauroshobas->id,
            'table_name'    => 'master_pauroshobas',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterPauroshobas
        ]);
    }

    

    /**
     * master Pauroshoba destroy
     */
    public function destroy($id)
    {
        $masterPauroshobas = MasterPauroshobas::find($id);

        if (!$masterPauroshobas) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        $masterPauroshobas->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_pauroshobas',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
