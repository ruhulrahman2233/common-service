<?php

namespace App\Http\Controllers\CityCorporation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\CityCorporation\MasterCityCorporationsValidations;
use App\Models\CityCorporation\MasterCityCorporations;
use DB;

class MasterCityCorporationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all master city corporations 
     */
    public function index(Request $request)
    {  
        $query = DB::table('master_city_corporations') 
                        ->join('master_divisions','master_city_corporations.division_id', '=','master_divisions.id')
                        ->join('master_districts','master_city_corporations.district_id', '=','master_districts.id')
                        ->select('master_city_corporations.*',
                            'master_divisions.division_name','master_divisions.division_name_bn',
                            'master_districts.district_name','master_districts.district_name_bn');
  
        if ($request->city_corporation_name) {
            $query = $query->where('master_city_corporations.city_corporation_name', 'like', "{$request->city_corporation_name}%")
                            ->orWhere('master_city_corporations.city_corporation_name_bn', 'like', "{$request->city_corporation_name}%");
        }

        if ($request->division_id) {
            $query = $query->where('master_city_corporations.division_id', $request->division_id);
        }

        if ($request->district_id) {
            $query = $query->where('master_city_corporations.district_id', $request->district_id);
        }    

        if ($request->status) {
            $query = $query->where('master_city_corporations.status', $request->status);
        }     

        $list = $query->orderBy('master_divisions.division_name', 'ASC')
                        ->orderBy('master_districts.district_name', 'ASC')
                        ->orderBy('master_city_corporations.city_corporation_name', 'ASC')
                        ->paginate(request('per_page', config('app.per_page')));

        if(count($list)>0){
        	return response([
	            'success' => true,
	            'message' => 'City corporations info list',
	            'data' => $list
	        ]);
        }
        else
        {
        	return response([
	            'success' => false,
	            'message' => 'Data not found!!'
	        ]);
        }        
    }

    /**
     * master city corporations show
     */
    public function show($id)
    {
        $masterCityCorporations = MasterCityCorporations::find($id);

        if (!$masterCityCorporations) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'City corporationss details',
            'data'    => $masterCityCorporations
        ]);
    }

    /**
     * master city corporations store
     */
    public function store(Request $request)
    {
        $validationResult = MasterCityCorporationsValidations:: validate($request);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }
        
        try {
            $masterCityCorporations 						= new MasterCityCorporations();
            $masterCityCorporations->city_corporation_name     = $request->city_corporation_name;
            $masterCityCorporations->city_corporation_name_bn  = $request->city_corporation_name_bn;
            $masterCityCorporations->division_id    	 	= (int)$request->division_id;
            $masterCityCorporations->district_id    		= (int)$request->district_id;
            $masterCityCorporations->created_by     		= (int)user_id();
            $masterCityCorporations->updated_by     		= (int)user_id();
            $masterCityCorporations->save();

            save_log([
                'data_id'    => $masterCityCorporations->id,
                'table_name' => 'master_city_corporations'
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $masterCityCorporations
        ]);
    }

    /**
     * master city corporations update
     */
    public function update(Request $request, $id)
    {
        $validationResult = MasterCityCorporationsValidations:: validate($request ,$id);    
        
        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $masterCityCorporations = MasterCityCorporations::find($id);

        if (!$masterCityCorporations) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $masterCityCorporations->city_corporation_name     = $request->city_corporation_name;
            $masterCityCorporations->city_corporation_name_bn  = $request->city_corporation_name_bn;
            $masterCityCorporations->division_id    	 	= (int)$request->division_id;
            $masterCityCorporations->district_id    		= (int)$request->district_id;
            $masterCityCorporations->updated_by     = (int)user_id();
            $masterCityCorporations->update();

            save_log([
                'data_id'       => $masterCityCorporations->id,
                'table_name'    => 'master_city_corporations',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data update successfully',
            'data'    => $masterCityCorporations
        ]);
    }

    /**
     * master city corporations status update
     */
    public function toggleStatus($id)
    {
        $masterCityCorporations = MasterCityCorporations::find($id);

        if (!$masterCityCorporations) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $masterCityCorporations->status = $masterCityCorporations->status == 1 ? 0 : 1;
        $masterCityCorporations->update();

        save_log([
            'data_id'       => $masterCityCorporations->id,
            'table_name'    => 'master_city_corporations',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $masterCityCorporations
        ]);
    }

    

    /**
     * master city corporations destroy
     */
    public function destroy($id)
    {
        $masterCityCorporations = MasterCityCorporations::find($id);

        if (!$masterCityCorporations) {
            return response([
                'success' => false,
                'message' => 'Data not found!!'
            ]);
        }

        $masterCityCorporations->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'master_city_corporations',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Data deleted successfully'
        ]);
    }
}
