<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterUpazila extends Model
{
    protected $table="master_upazillas";

    protected $fillable = [
      'upazilla_name','upazilla_name_bn','district_id ','status'
    ];

}
