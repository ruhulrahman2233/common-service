<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;


class MasterBranch extends Model
{
    protected $table ="master_branchs";

    protected $fillable = [
         'branch_name','branch_name_bn','address','address_bn','bank_id'
    ];

    
}
