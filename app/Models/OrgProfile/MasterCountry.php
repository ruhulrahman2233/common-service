<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterCountry extends Model
{
  protected $table = "master_countries";

  protected $fillable = [
    'country_name', 'country_name_bn'
  ];
}
