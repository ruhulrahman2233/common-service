<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterOrgModule extends Model
{
    protected $table ="master_org_modules";

    protected $fillable = [
        'module_id','org_id','status'
    ];
}
