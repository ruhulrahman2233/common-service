<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;
use App\Models\MasterModule;

class MasterOrganizationProfile extends Model
{
    protected $table = "master_org_profiless";

    protected $fillable = [
        'org_name', 'status'
    ];

    // public function modules()
    // {
    //     return $this->belongsToMany(MasterModule::class, 'master_org_modules', 'module_id', 'org_id');
    // }
}
