<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterDivision extends Model
{
    protected $table = "master_divisions";
    protected $fillable = [
        'division_name', 'division_name_bn', 'country_id'
    ];
    public function office()
    {
        return $this->hasMany('App\Models\OrgProfile\MasterOffice', 'division_id', 'id')
                    ->select(
                        'id',
                        'org_id',
                        'office_type_id',
                        'division_id',
                        'district_id'
                    );
    }
}
