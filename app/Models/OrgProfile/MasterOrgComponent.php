<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterOrgComponent extends Model
{
    protected $table ='master_org_components';

    protected $fillable = [
         'org_id','component_id'
    ];
    
    public function component()
    {
        return $this->belongsTo('App\Models\OrgProfile\MasterComponent', 'component_id', 'id');
    }
}
