<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterFiscalYear extends Model
{
    protected $table="master_fiscal_years";
    
    protected $fillable =[
        'year',
        'start_date',
        'end_date',  
		'sorting_order',
		'created_by',	
		'updated_by',   
		'status'
    ];
}
