<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterDistrict extends Model
{
    protected $table = "master_districts";

    // protected $fillable = [
    //     'district_name', 'district_name_bn', 'division_id'
    // ];
    public function office()
    {
        return $this->hasMany('App\Models\OrgProfile\MasterOffice', 'district_id', 'id')
                    ->select(
                        'id',
                        'org_id',
                        'office_type_id',
                        'division_id',
                        'district_id'
                    );
    }
}
