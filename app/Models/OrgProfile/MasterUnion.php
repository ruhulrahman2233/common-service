<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterUnion extends Model
{
    protected $table="master_unions";

    protected $fillable = [
       'union_name','union_name_bn','upazilla_id','status'
    ];
}
