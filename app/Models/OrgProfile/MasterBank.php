<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrgProfile\MasterBranch;

class MasterBank extends Model
{
    protected $table ="master_banks";

    protected $fillable = [
        'bank_name','bank_name_bn','org_id','component_id'
    ];

    public function branch()
    {
        return $this->hasMany(MasterBranch::class,'bank_id','id');
    }
}
