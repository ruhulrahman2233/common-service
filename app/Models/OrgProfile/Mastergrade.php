<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class Mastergrade extends Model
{
    protected $table ="master_grades";

    protected $fillable =[
        'grade_name','grade_name_bn'
    ];
}
