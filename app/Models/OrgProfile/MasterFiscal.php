<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterFiscal extends Model
{
  protected $table = "master_fiscal_years";

  protected $fillable = [
    'year_name', 'year_name_bn'
  ];
}
