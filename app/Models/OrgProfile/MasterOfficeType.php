<?php

namespace App\Models\OrgProfile;

use Illuminate\Database\Eloquent\Model;

class MasterOfficeType extends Model
{
    protected $table = "master_office_types";

    protected $fillable = [
        'office_type_name', 'office_type_name_bn'
    ];
}
