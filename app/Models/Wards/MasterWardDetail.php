<?php

namespace App\Models\Wards;

use Illuminate\Database\Eloquent\Model;

class MasterWardDetail extends Model
{
    protected $table ="master_ward_details";

    protected $fillable = [
		'master_ward_id', 	
		'ward_name', 	
		'ward_name_bn', 
		'created_at', 
		'updated_at'
    ];
}
