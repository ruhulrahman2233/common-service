<?php

namespace App\Models\Wards;

use Illuminate\Database\Eloquent\Model;

class MasterWard extends Model
{
   	protected $table ="master_wards";

    protected $fillable = [
		'type',
		'city_corporation_id',
		'division_id',
		'district_id',
		'upazilla_id',
		'union_id',
		'pauroshoba_id',
		'ward_name',
		'ward_name_bn',
		'status',
		'created_by',
		'updated_by',
		'created_at',
		'updated_at'
    ];

	public function masterWardDetails() 
	{
        return $this->hasMany(MasterWardDetail::class, 'master_ward_id');
	}
}
