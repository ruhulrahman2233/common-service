<?php

namespace App\Models\InfoServiceManage;

use Illuminate\Database\Eloquent\Model;

class ServiceEligibilityTypeEntry extends Model
{
    protected $table ="master_eligibility_types";

    protected $fillable = [
    	'type_name',	
		'type_name_bn',	
		'status',	
		'created_by',	
		'updated_by',	
		'created_at',	
		'updated_at'
    ];
}