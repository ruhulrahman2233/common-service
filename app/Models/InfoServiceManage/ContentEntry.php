<?php

namespace App\Models\InfoServiceManage;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrgProfile\MasterService;

class ContentEntry extends Model
{
    protected $table ="master_contents";

    protected $fillable = [
    	'component_id',	
    	'service_id',	
    	'description',	
		'description_bn',	
		'status',	
		'created_by',	
		'updated_by',	
		'created_at',	
		'updated_at'
    ];

    public function masterContentEligibility()
    {
        return $this->hasMany(MasterContentEligibility::class, 'master_content_id', 'id');
    }

}

