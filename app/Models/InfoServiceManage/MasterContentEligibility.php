<?php

namespace App\Models\InfoServiceManage;

use Illuminate\Database\Eloquent\Model;

class MasterContentEligibility extends Model
{   
   protected $table ="master_content_eligibility";

    protected $fillable = [
    	'eligibility_criteria_id',	
    	'master_content_id',	
		'created_at',	
		'updated_at'
    ];

    public function eligibilityCriteria()
    {
       return $this->belongTo(ServiceEligibilityTypeEntry::class, 'eligibility_criteria_id', 'id');
    }
}
	