<?php

namespace App\Models\InfoServiceManage;

use Illuminate\Database\Eloquent\Model;

class ManageFaq extends Model
{
    protected $table ="master_faqs";

    protected $fillable = [
    	'component_id',	
		'service_id',	
		'question',	
		'question_bn',	
		'answer',	
		'answer_bn',		
		'status',	
		'created_by',	
		'updated_by',	
		'created_at',	
		'updated_at'
    ];
}