<?php

namespace App\Models\Organogram;

use Illuminate\Database\Eloquent\Model;

class MasterDesignation extends Model
{
    protected $table = "master_designations";

    protected $fillable = [
        'org_id', 'designation', 'designation_bn', 'direct_post', 'promotional_post', 'total_post', 'grade_id', 'sorting_order'
    ];
}
