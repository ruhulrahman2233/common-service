<?php

namespace App\Models\Organogram;

use Illuminate\Database\Eloquent\Model;

class AssignDesignation extends Model
{
    protected $table = "assign_designations";

    protected $fillable = [
        'org_id', 'office_type_id', 'office_id', 'designation_id'
    ];
    public function designation()
    {
        return $this->belongsTo('App\Models\Organogram\MasterDesignation');
    }
}
