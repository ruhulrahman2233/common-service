<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class NotNotificationTemplate extends Model
{
    protected $table ="not_notification_templates";

    protected $fillable = [
        'notification_type_id','title','title_bn','message','message_bn','org_id','component_id','module_id','service_id','menu_id'
    ];

}
