<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class MasterNotification extends Model
{
    protected $table ="master_notification_types";

    protected $fillable = [
        'not_type_name','not_type_name_bn','status'
    ];
}
