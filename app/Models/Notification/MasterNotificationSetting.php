<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class MasterNotificationSetting extends Model
{
    protected $table = "master_notification_settings";

    protected $fillable = [
        'org_id','component_id','module_id','service_id','menu_id','notification_type_id','status'
    ];
}
