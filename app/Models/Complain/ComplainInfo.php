<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class ComplainInfo extends Model
{
    protected $table = "com_complain_infos";

    protected $fillable = [
        'com_title', 'com_title_bn', 'description', 'description_bn', 'org_id', 'component_id', 'module_id', 'service_id', 'division_id', 'district_id', 'upazilla_id' ,'union_id', 'mobile_no', 'email', 'status'
    ];
}
