<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class MasterComplainType extends Model
{
    protected $table = "master_complain_types";

    protected $fillable = [
        'com_type_name', 'com_type_name_bn'
    ];
}
