<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class MasterComplainDesignation extends Model
{
    protected $table = "master_complain_designations";

    protected $fillable = [
        'org_id', 'designation_id'
    ];
}
