<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class ComplainCommitteeReport extends Model
{
    protected $table = "com_committee_reports";

    protected $fillable = [
        'complain_id', 'report_note'
    ];
}
