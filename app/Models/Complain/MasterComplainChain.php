<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class MasterComplainChain extends Model
{
    protected $table = "master_complain_chains";

    protected $fillable = [
        'org_id', 'designation_id', 'sorting_order'
    ];
}
