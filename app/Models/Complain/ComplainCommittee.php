<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class ComplainCommittee extends Model
{
    protected $table = "com_committees";

    protected $fillable = [
        'complain_id', 'role_id', 'user_id', 'designation_id'
    ];

}
