<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class ComplainReject extends Model
{
    protected $table = "com_reject";

    protected $fillable = [
        'complain_id', 'reject_note'
    ];
}
