<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class ComplainApprove extends Model
{
    protected $table = "com_approve";

    protected $fillable = [
        'complain_id', 'approve_note'
    ];
}
