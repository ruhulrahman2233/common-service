<?php

namespace App\Models\Complain;

use Illuminate\Database\Eloquent\Model;

class ComplainSend extends Model
{
    protected $table = "com_complain_cycle";

    protected $fillable = [
        'complain_id', 'sender_id', 'receiver_id', 'note', 'note_bn', 'status'
    ];
}
