<?php

namespace App\Models\CityCorporation;

use Illuminate\Database\Eloquent\Model;

class MasterCityCorporations extends Model
{
    protected $table ="master_city_corporations";

    protected $fillable = [
		'division_id',	
		'district_id', 	
		'city_corporation_name', 	
		'city_corporation_name_bn', 	
		'status', 
		'created_by', 	
		'updated_by' 
    ];
}
