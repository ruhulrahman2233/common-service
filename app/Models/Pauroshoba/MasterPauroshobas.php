<?php

namespace App\Models\Pauroshoba;

use Illuminate\Database\Eloquent\Model;

class MasterPauroshobas extends Model
{
    protected $table ="master_pauroshobas";

    protected $fillable = [
		'division_id',	
		'district_id', 	
		'upazilla_id', 	
		'pauroshoba_name', 	
		'pauroshoba_name_bn', 	
		'status', 
		'created_by', 	
		'updated_by' 
    ];
}
