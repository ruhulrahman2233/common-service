<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

class AddDocument extends Model
{
    protected $table ="doc_doc_infos";

    protected $fillable = [
       'doc_title','doc_title_bn','attachment','org_id','category_id'
    ];
}
