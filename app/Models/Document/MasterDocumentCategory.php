<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

class MasterDocumentCategory extends Model
{
    protected $table= "master_document_categories";

    protected $fillable = [
         'category_name','category_name_bn','sorting_order','status'
    ];
}
