<?php

namespace App\Models\Committee;

use Illuminate\Database\Eloquent\Model;
use App\Models\Committee\CreateCommittee;
use App\Models\Committee\CreateCommitteeMember;
use App\Models\OrgProfile\MasterOrganizationProfile;

class CreateCommittee extends Model
{
    protected $table ="cmt_committees";

    protected $fillable = [
        'org_id','committee_name','committee_name_bn','formation_date','purpose','purpose_bn',
        'duration','document_name'
    ];

   public function committee()
   {
    return $this->hasMany(CreateCommitteeMember::class,'cmt_committee_id','id');
   }

//    public function organization()
//     {
//         return $this->belongsToMany(MasterOrganizationProfile::class,'cmt_committees','id','org_id')
//                         ->select('org_name','org_name_bn')->orderBy('org_name', 'ASC');
//     }

  

}
