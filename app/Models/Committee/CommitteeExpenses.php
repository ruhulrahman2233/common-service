<?php

namespace App\Models\Committee;

use Illuminate\Database\Eloquent\Model;

class CommitteeExpenses extends Model
{
    protected $table = "cmt_expenses";

    protected $fillable = [
       'org_id','date','fiscal_year','cmt_committee_id','cmt_agenda_id','amount','description','description_bn'
    ];
}
