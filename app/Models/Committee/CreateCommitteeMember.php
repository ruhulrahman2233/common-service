<?php

namespace App\Models\Committee;

use Illuminate\Database\Eloquent\Model;

class CreateCommitteeMember extends Model
{
    protected $table ="cmt_committee_members";

    protected $fillable = [
        'user_id','cmt_committee_id'
    ];
}
