<?php

namespace App\Models\Committee;

use Illuminate\Database\Eloquent\Model;

class CommitteeAgenda extends Model
{
    protected $table = "cmt_agenda";

    protected $fillable = [
       'org_id','cmt_committee_id','meeting_number','memo_number','memo_issue_date','agenda_name',
       'agenda_name_bn','meeting_date','attachment'
    ];
}
