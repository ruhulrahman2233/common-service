<?php

namespace App\Models\Committee;

use Illuminate\Database\Eloquent\Model;

class CommitteeMetting extends Model
{
    protected $table = "cmt_meeting_minutes";

    protected $fillable = [
       'cmt_committee_id','org_id','cmt_agenda_id','decision','decision_bn','attendance_attachment','document_attachment'
    ];
}
