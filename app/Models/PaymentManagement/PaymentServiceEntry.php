<?php

namespace App\Models\PaymentManagement;

use Illuminate\Database\Eloquent\Model;

class PaymentServiceEntry extends Model
{
    protected $table ="master_payment_services";

    protected $fillable = [
    	'org_id',	
		'component_id',	
		'module_id',	
		'service_id',	
		'amount',	
		'created_by',	
		'updated_by',	
		'status'
    ];
}
