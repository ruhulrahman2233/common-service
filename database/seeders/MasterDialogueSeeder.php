<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrgProfile\DialogueInfo;

class MasterDialogueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DialogueInfo::create([
            'dialogue' => 'Bangladesh is self-sufficient in agriculture',
            'dialogue_bn' => 'কৃষিতে স্বনির্ভর বাংলাদেশ',
            'position' => 1,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
