<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrgProfile\MasterOrganizationProfile;

class MasterOrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterOrganizationProfile::create([
            'org_name' => 'Ministry of Agriculture',
            'org_name_bn' => 'কৃষি মন্ত্রণালয়',
            'abbreviation' => 'MoA',
            'abbreviation_bn' => 'এমওএ',
            'address' => null,
            'address_bn' => null,
            'website_url' => 'moa.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Department of Agricultural Extension (DAE)',
            'org_name_bn' => 'কৃষি সম্প্রসারণ অধিদপ্তর',
            'abbreviation' => 'DAE',
            'abbreviation_bn' => 'ডিএই',
            'address' => null,
            'address_bn' => 'খামারবাড়ী, ফার্মগেট, ঢাকা-১২১৫',
            'website_url' => 'www.dae.gov.bd/',
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Agricultural Development Corporation',
            'org_name_bn' => 'বাংলাদেশ কৃষি উন্নয়ন কর্পোরেশন',
            'abbreviation' => 'BADC',
            'abbreviation_bn' => 'বিএডিসি',
            'address' => null,
            'address_bn' => 'কৃষি ভবন ৪৯-৫১, দিলকুশা বাণিজ্যিক এলাকা, ঢাকা-১০০০',
            'website_url' => 'badc.gov.bd/',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Agricultural Research Council',
            'org_name_bn' => 'বাংলাদেশ কৃষি গবেষণা কাউন্সিল',
            'abbreviation' => 'BARC',
            'abbreviation_bn' => 'বিএআরসি',
            'address' => null,
            'address_bn' => 'ফার্মগেট, ঢাকা – ১২১৫',
            'website_url' => 'www.barc.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Agricultural Research Institute',
            'org_name_bn' => 'বাংলাদেশ কৃষি গবষেণা ইনস্টিটিউট ',
            'abbreviation' => 'BARI',
            'abbreviation_bn' => 'বারি',
            'address' => null,
            'address_bn' => 'জয়দেবপুর, গাজীপুর-১৭০১',
            'website_url' => 'www.bari.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Rice Research Institute',
            'org_name_bn' => 'বাংলাদেশ ধান গবেষণা ইনস্টিটউট',
            'abbreviation' => 'BRRI',
            'abbreviation_bn' => 'ব্রি',
            'address' => null,
            'address_bn' => 'জয়দেবপুর, গাজীপুর-১৭০১',
            'website_url' => 'www.brri.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Jute Research Institute',
            'org_name_bn' => 'বাংলাদেশ পাট গবেষণা ইনস্টিটিউট',
            'abbreviation' => 'BJRI',
            'abbreviation_bn' => 'বিজেআরআই',
            'address' => null,
            'address_bn' => 'মানিকমিয়া এভিনিউ, ঢাকা',
            'website_url' => 'www.bjri.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Sugarcrop Research Institute',
            'org_name_bn' => 'বাংলাদেশ সুগারক্রপ গবেষণা ইনস্টিটিউট',
            'abbreviation' => 'BSRI',
            'abbreviation_bn' => 'বিএসআরআই',
            'address' => null,
            'address_bn' => 'ঈশ্বরদি, পাবনা',
            'website_url' => 'www.bsri.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Institute of Nuclear Agriculture',
            'org_name_bn' => 'বাংলাদেশ পরমাণু কৃষি গবেষণা ইনস্টিটিউট',
            'abbreviation' => 'BINA',
            'abbreviation_bn' => 'বিনা',
            'address' => null,
            'address_bn' => 'বাংলাদেশ কৃষি বিশ্ববিদ্যালয় ক্যাম্পাস, ময়মনসিংহ',
            'website_url' => 'www.bina.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Cotton Development Board',
            'org_name_bn' => 'তুলা উন্নয়ন বোর্ড',
            'abbreviation' => 'CDB',
            'abbreviation_bn' => 'সিডিবি',
            'address' => null,
            'address_bn' => 'খামারবাড়ি, ঢাকা',
            'website_url' => 'www.cdb.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Agriculture Information Service',
            'org_name_bn' => 'কৃষি তথ্য সার্ভিস',
            'abbreviation' => 'AIS',
            'abbreviation_bn' => 'এআইএস',
            'address' => null,
            'address_bn' => 'খামারবাড়ি, ঢাকা',
            'website_url' => 'www.ais.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'National Agriculture Training Academy ',
            'org_name_bn' => 'জাতীয় কৃষি প্রশিক্ষণ একাডেমি',
            'abbreviation' => 'NATA',
            'abbreviation_bn' => 'নাটা',
            'address' => null,
            'address_bn' => 'জয়দেবপুর, গাজীপুর',
            'website_url' => 'www.nata.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Department of Agricultural Marketing',
            'org_name_bn' => 'কৃষি বিপণন অধিদপ্তর ',
            'abbreviation' => 'DAM',
            'abbreviation_bn' => 'ড্যাম',
            'address' => null,
            'address_bn' => 'খামারবাড়ি, ঢাকা',
            'website_url' => 'www.dam.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Seed Certification Agency',
            'org_name_bn' => 'বীজ প্রত্যয়ন এজেন্সী',
            'abbreviation' => 'SCA',
            'abbreviation_bn' => 'এসসিএ',
            'address' => null,
            'address_bn' => 'জয়দেবপুর, গাজীপুর',
            'website_url' => 'www.sca.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Barind Mutipurpose Development Authority',
            'org_name_bn' => 'বরেন্দ্র বহুমুখী উন্নয়ন কর্তৃপক্ষ',
            'abbreviation' => 'BMDA',
            'abbreviation_bn' => 'বিএমডিএ',
            'address' => null,
            'address_bn' => 'বরেন্দ্র ভবন, প্রধান কার্যালয়, সেনানিবাস সড়ক, আমবাগান, জিপিও-৬০০০, রাজশাহী।',
            'website_url' => 'www.bmda.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Institute of Research and Training on Applied Nutrition',
            'org_name_bn' => 'বাংলাদেশ ফলিত পুষ্টি গবেষণা ও প্রশিক্ষণ ইনস্টিটিউট ',
            'abbreviation' => 'BIRTAN',
            'abbreviation_bn' => 'বারটান',
            'address' => null,
            'address_bn' => 'মানিকমিয়া এভিনিউ, ঢাকা',
            'website_url' => 'www.birtan.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Soil Resource Development Institute',
            'org_name_bn' => 'মৃত্তিকা সম্পদ উন্নয়ন ইনস্টিটিউট',
            'abbreviation' => 'SRDI',
            'abbreviation_bn' => 'এসআরডিআই',
            'address' => null,
            'address_bn' => 'কৃষি খামার সড়ক, ফার্মগেট, ঢাকা',
            'website_url' => 'www.srdi.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Hortex Foundation',
            'org_name_bn' => 'হর্টিকালচার এক্সপোর্ট ডেভেলপমেন্ট ফাউন্ডেশন',
            'abbreviation' => 'HORTEX',
            'abbreviation_bn' => 'হর্টেক্স ফাউন্ডেশন',
            'address' => null,
            'address_bn' => 'মানিকমিয়া এভিনিউ, ঢাকা',
            'website_url' => 'hortex.portal.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterOrganizationProfile::create([
            'org_name' => 'Bangladesh Wheat and Maize Research Institute',
            'org_name_bn' => 'বাংলাদেশ গম ও ভুট্টা গবেষণা ইনস্টিটিউট',
            'abbreviation' => 'BWMRI',
            'abbreviation_bn' => 'বিডব্লিউএমআরআই',
            'address' => null,
            'address_bn' => 'নশিপুর, দিনাজপুর',
            'website_url' => 'www.bwmri.gov.bd',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
