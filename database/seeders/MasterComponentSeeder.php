<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrgProfile\MasterComponent;

class MasterComponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterComponent::create([
            'component_name' => 'Common Service Configuration',
            'component_name_bn' => 'কমন সার্ভিস কনফিগারেশন',
            'description' => 'Common Service Configuration',
            'description_bn' => 'কমন সার্ভিস কনফিগারেশন',
            'sorting_order' => 1,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'License & Registration',
            'component_name_bn' => 'লাইসেন্স & রেজিস্ট্রেশন',
            'description' => 'License, Registration, Clearance & Certification Management System',
            'description_bn' => 'লাইসেন্স, রেজিস্ট্রেশন, ক্লিয়ারেন্স & সার্টিফিকেশন ম্যানেজমেন্ট সিস্টেম',
            'sorting_order' => 2,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'Agri Research & Testing Management',
            'component_name_bn' => 'এগ্রি রিসার্চ & টেস্টিং ম্যানেজমেন্ট',
            'description' => 'Agri Research, Testing, Report & Data Repository Service',
            'description_bn' => 'এগ্রি রিসার্চ, টেস্টিং, রিপোর্ট & ডাটা রিপোসিটোরি সার্ভিস',
            'sorting_order' => 3,
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        MasterComponent::create([
            'component_name' => 'Incentive & Grant Management',
            'component_name_bn' => 'ইন্সেন্টিভ & গ্রান্ট ম্যানেজমেন্ট',
            'description' => 'Incentive & Grant Management System (Allowance, Incentive & Grant)',
            'description_bn' => 'ইন্সেন্টিভ & গ্রান্ট ম্যানেজমেন্ট সিস্টেম (এলাউন্স, ইন্সেন্টিভ & গ্রান্ট)',
            'sorting_order' => 4,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'Agri Marketing & Business Linkage Management',
            'component_name_bn' => 'এগ্রি মার্কেটিং & বিসনেস লিংকেজ ম্যানেজমেন্ট',
            'description' => 'Agri marketing & Business Linkage Management System',
            'description_bn' => 'এগ্রি মার্কেটিং & বিসনেস লিংকেজ ম্যানেজমেন্ট সিস্টেম',
            'sorting_order' => 5,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'Market Infrastructure & Warehouse Management',
            'component_name_bn' => 'মার্কেট ইনফ্রাস্ট্রাকচার & ওয়্যারহাউস ম্যানেজমেন্ট',
            'description' => 'Market Infrastructure & Warehouse Management System',
            'description_bn' => 'মার্কেট ইনফ্রাস্ট্রাকচার & ওয়্যারহাউস ম্যানেজমেন্ট সিস্টেম',
            'sorting_order' => 6,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'National Virtual Crop Pest Museum',
            'component_name_bn' => 'ন্যাশনাল ভার্চুয়াল ক্রপ পেস্ট মিউজিয়াম',
            'description' => 'National Virtual Crop Pest Museum',
            'description_bn' => 'ন্যাশনাল ভার্চুয়াল ক্রপ পেস্ট মিউজিয়াম',
            'sorting_order' => 7,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'Crop, Seed, Germplasm & Fertilizer Management',
            'component_name_bn' => 'ক্রপ, সীড, জার্মপ্লাজম & ফার্টিলাইজার ম্যানেজমেন্ট',
            'description' => 'Crop, Seed, Germplasm & Fertilizer Production, Development, Distribution & Service Management System',
            'description_bn' => 'ক্রপ, সীড, জার্মপ্লাজম & ফার্টিলাইজার প্রোডাকশন, ডেভেলপমেন্ট, ডিস্ট্রিবিউশন & সার্ভিস ম্যানেজমেন্ট সিস্টেম',
            'sorting_order' => 8,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'Irrigation & Scheme Management',
            'component_name_bn' => 'ইরিগেশন, ওয়াটার & স্কিম ম্যানেজমেন্ট',
            'description' => 'Irrigation, Water & Scheme Management',
            'description_bn' => 'ইরিগেশন & স্কিম ম্যানেজমেন্ট সিস্টেম',
            'sorting_order' => 9,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        MasterComponent::create([
            'component_name' => 'Training, e-Learning & Venue Management',
            'component_name_bn' => 'ট্রেইনিং, ই-লার্নিং & ভেনু ম্যানেজমেন্ট',
            'description' => 'Training, e-Learning & Venue Management',
            'description_bn' => 'ট্রেইনিং, ই-লার্নিং & ভেনু ম্যানেজমেন্ট',
            'sorting_order' => 10,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
