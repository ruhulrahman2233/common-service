<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('master_grades')->insert([
            'grade_name' => '1',
            'grade_name_bn' => '১',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '2',
            'grade_name_bn' => '২',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '3',
            'grade_name_bn' => '৩',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '4',
            'grade_name_bn' => '৪',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '5',
            'grade_name_bn' => '৫',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '6',
            'grade_name_bn' => '৬',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '7',
            'grade_name_bn' => '৭',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '8',
            'grade_name_bn' => '৮',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '9',
            'grade_name_bn' => '৯',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '10',
            'grade_name_bn' => '১০',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '11',
            'grade_name_bn' => '১১',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '12',
            'grade_name_bn' => '১২',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '13',
            'grade_name_bn' => '১৩',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '14',
            'grade_name_bn' => '১৪',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '15',
            'grade_name_bn' => '১৫',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '16',
            'grade_name_bn' => '১৬',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '17',
            'grade_name_bn' => '১৭',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '18',
            'grade_name_bn' => '১৮',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '19',
            'grade_name_bn' => '১৯',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        DB::table('master_grades')->insert([
            'grade_name' => '20',
            'grade_name_bn' => '২০',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
