<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrgProfile\MasterCountry;

class MasterCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterCountry::create([
            'country_name' => 'Bangladesh',
            'country_name_bn' => 'বাংলাদেশ',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
