<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MasterComponentSeeder::class);
        $this->call(MasterOrganizationSeeder::class);
        $this->call(MasterCountrySeeder::class);
        $this->call(MasterDivisionSeeder::class);
        $this->call(MasterDistrictSeeder::class);
        $this->call(MasterUpazilaSeeder::class);
        $this->call(MasterUnionSeeder::class);
        $this->call(MasterGradeSeeder::class);
        $this->call(MasterDialogueSeeder::class);
    }
}
