<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmtMeetingMinutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmt_meeting_minutes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('org_id');
            $table->unsignedBigInteger('cmt_committee_id');
            $table->unsignedBigInteger('cmt_agenda_id');
            $table->text('decision');
            $table->text('decision_bn');
            $table->string('attendance_attachment');
            $table->string('document_attachment');
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('cmt_committee_id')->references('id')->on('cmt_committees');
            $table->foreign('cmt_agenda_id')->references('id')->on('cmt_agenda');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_minutes');
    }
}
