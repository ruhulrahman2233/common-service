<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatMasterFiscalYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_fiscal_years', function (Blueprint $table) {
            $table->id();
            $table->string('year',100);
            $table->date('start_date'); 
            $table->date('end_date'); 
            $table->tinyInteger('sorting_order')->nullable(); 
            $table->unsignedBigInteger('created_by')->nullable();    
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_fiscal_years');
    }
}
