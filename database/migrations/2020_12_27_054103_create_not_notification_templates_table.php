<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotNotificationTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('not_notification_templates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('notification_type_id');
            $table->string('title');
            $table->string('title_bn');
            $table->text('message');
            $table->text('message_bn');
            $table->unsignedBigInteger('org_id');
            $table->unsignedBigInteger('component_id');            
            $table->unsignedBigInteger('module_id');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('menu_id');           
            $table->unsignedBigInteger('created_by')->nullable();            
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('not_notification_templates');
    }
}
