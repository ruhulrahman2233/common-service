<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComComplainInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('com_complain_infos', function (Blueprint $table) {
            $table->id();
            $table->string('com_title',100);
            $table->string('com_title_bn',100)->nullable();
            $table->text('description',100);
            $table->text('description_bn',100)->nullable();;
            $table->unsignedBigInteger('org_id');
            $table->unsignedBigInteger('component_id');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('module_id');
            $table->unsignedBigInteger('division_id'); 
            $table->unsignedBigInteger('district_id');
            $table->unsignedBigInteger('upazilla_id');
            $table->unsignedBigInteger('union_id');
            $table->string('mobile_no',100)->nullable();;
            $table->string('email',100)->nullable();;
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=pending, 1=processing,2=Presented,3=Rejected,4=Solved,5=Appealed'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_complain_infos');
    }
}
