<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmtAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmt_agenda', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('org_id');
            $table->unsignedBigInteger('cmt_committee_id');
            $table->integer('meeting_number');
            $table->string('memo_number');
            $table->date('memo_issue_date');
            $table->string('agenda_name');
            $table->string('agenda_name_bn');
            $table->date('meeting_date');
            $table->string('attachment')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('cmt_committee_id')->references('id')->on('cmt_committees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cmt_agenda');
    }
}
