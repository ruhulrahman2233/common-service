<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocDocInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_doc_infos', function (Blueprint $table) {
            $table->id();
            $table->string('doc_title',100);
            $table->string('doc_title_bn',100);
            $table->string('attachment');
            $table->unsignedBigInteger('org_id');
            $table->unsignedBigInteger('category_id');           
            $table->unsignedBigInteger('created_by')->nullable();            
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
            $table->foreign('org_id')->references('id')->on('master_org_profiless');
            $table->foreign('category_id')->references('id')->on('master_document_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_doc_infos');
    }
}
