<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameConutryIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_divisions', function (Blueprint $table) {
            $table->dropForeign('master_divisions_conutry_id_foreign');
            $table->renameColumn('conutry_id', 'country_id');
            $table->foreign('country_id')->references('id')->on('master_countries');
           
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_divisions', function (Blueprint $table) {
            //
        });
    }
}
