<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterUpazillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_upazillas', function (Blueprint $table) {
            $table->id();
            $table->string('upazilla_name',100);
            $table->string('upazilla_name_bn',100)->nullable();
            $table->unsignedBigInteger('district_id');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
            $table->foreign('district_id')->references('id')->on('master_districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_upazillas');
    }
}
