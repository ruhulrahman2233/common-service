<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_offices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('org_id')->nullable();
            $table->unsignedBigInteger('office_type_id')->nullable();
            $table->Integer('area_type_id')->comment('1=City Corpoation,2=Pauroshoba,3=Union');
            $table->Integer('city_corporation_id')->nullable();
            $table->Integer('pauroshoba_id')->nullable();
            $table->Integer('ward_id')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('division_id')->nullable();            
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('upazilla_id')->nullable();
            $table->unsignedBigInteger('union_id')->nullable();            
            $table->string('office_name',100);
            $table->string('office_name_bn',100);
            $table->string('office_code',100)->nullable();
            $table->unsignedBigInteger('parent_office_type_id')->nullable();
            $table->unsignedBigInteger('parent_office_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_offices');
    }
}
