<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComApproveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('com_approve', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('complain_id');
            $table->text('approve_note');
            $table->text('approve_note_bn')->nullable();
            $table->string('attachment')->nullable();          
            $table->unsignedBigInteger('created_by')->nullable();            
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive'); 
            $table->timestamps();
            $table->foreign('complain_id')->references('id')->on('com_complain_infos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_approve');
    }
}
