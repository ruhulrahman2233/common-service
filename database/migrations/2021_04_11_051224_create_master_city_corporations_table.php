<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterCityCorporationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_city_corporations', function (Blueprint $table) {
            $table->id();           
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('district_id');           
            $table->string('city_corporation_name')->nullable();
            $table->string('city_corporation_name_bn')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
            $table->foreign('division_id')->references('id')->on('master_divisions');
            $table->foreign('district_id')->references('id')->on('master_districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_city_corporations');
    }
}
