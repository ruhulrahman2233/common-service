<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_menus', function (Blueprint $table) {
            $table->id();           
            $table->string('menu_name');
            $table->string('menu_name_bn');
            $table->string('url');
            $table->integer('sorting_order');
            $table->unsignedBigInteger('component_id')->nullable();
            $table->unsignedBigInteger('module_id')->nullable(); 
            $table->unsignedBigInteger('service_id')->nullable();  
            $table->string('associated_urls')->nullable();        
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive'); 
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('master_modules');
            $table->foreign('component_id')->references('id')->on('master_components');
            $table->foreign('service_id')->references('id')->on('master_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_menus');
    }
}
