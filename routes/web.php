<?php

use Illuminate\Support\Facades\Route;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/helper-test', function () {
    return user_id();
});

/******************** Data Archive Module *********************/
Route::group(['prefix'=>'/data-archive'], function() {
    Route::get('/database-backup', 'DataArchiveController@dumpDB');
    //download file path from storage
    Route::get('download-backup-db', 'DataArchiveController@downloadBackupDb');
    Route::get('db-backup-files', 'DataArchiveController@getDbBackupFiles');
    Route::delete('db-backup-delete', 'DataArchiveController@deleteDbBackupFile');
});

Route::get('common-dropdowns', function () {
    // Caching commonly used dropdown for 24 hours = 86400s
    $list = [
        'divisionList' => \App\Library\DropDowns::divisionList(),
        'districtList' => \App\Library\DropDowns::districtList(),
        'upazilaList' => \App\Library\DropDowns::upazilaList(),
        'unionList' => \App\Library\DropDowns::unionList(),
        'officeTypeList' => \App\Library\DropDowns::officeTypeList(),
        'officeList' => \App\Library\DropDowns::officeList(),
        'designationList' => \App\Library\DropDowns::designationList(),
        'gradeList' => \App\Library\DropDowns::gradeList(),
        'fiscalYearList' => \App\Library\DropDowns::fiscalYearList(),
        'bankList' => \App\Library\DropDowns::bankList(),
        'branchList' => \App\Library\DropDowns::branchList(),
        'countryList' => \App\Library\DropDowns::countryList()
    ];

    // Caching should be enabled on live server only and that is why the following cache remeber is disabled
    // $value = \Illuminate\Support\Facades\Cache::remember('commonDropdown', 0, function () {
    // });

    return response()->json([
        'success' => true,
        'data' => $list
    ]);
});

Route::get('common/dropdowns', function () {
    return response([
        'success' => true,
        'data' => [
            'componentList' => \App\Library\DropDowns::componentList(),
            'moduleList' => \App\Library\DropDowns::moduleList(),
            'serviceList' => \App\Library\DropDowns::serviceList(),
            'serviceComList' => \App\Library\DropDowns::serviceComList(),
            'menuList' => \App\Library\DropDowns::menuList(),
            'bankList' => \App\Library\DropDowns::bankList(),
            'notificationTypeList' => \App\Library\DropDowns::notificationTypeList(),
            'cmtCommitteeList' => \App\Library\DropDowns::cmtCommitteeList(),
            'cmtAgendaList' => \App\Library\DropDowns::cmtAgendaList(),
            'branchList' => \App\Library\DropDowns::branchList(),
            'cityCorporationList' => \App\Library\DropDowns::cityCorporationList(),
            'pauroshobaList' => \App\Library\DropDowns::pauroshobaList(),
            'wardList' => \App\Library\DropDowns::wardList(),
            'serviceEligibiltyList' => \App\Library\DropDowns::serviceEligibiltyList(),
            'documentCategoryList' => \App\Library\DropDowns::documentCategoryList(),
        ]
    ]);
});

Route::get('external-user-dropdowns', 'CommonDropdownController@extenalUserDropdowns');

Route::get('bank-and-branch-by-component-id/{componentId}', function($componentId) {
    return response()->json([
        'success' => true,
        'data' => [
            'bankList' => \App\Library\DropDowns::bankByComponent($componentId),
            'branchList' => \App\Library\DropDowns::branchList($componentId),
        ]
    ]);
});


$router->get('/redis-test', 'RedisTestController@redisTest');
Route::get('download-attachment', 'DownloadController@downloadAttachment');
Route::get('auth-user-office-detail/{officeId}', 'OrgProfile\MasterOfficeController@getOfficeDetail');

Route::group(['middleware'  =>  'token'], function () {
    Route::get('/protected-route', function () {
        Log::info('Protected route executed');

        return response([
            'success' => true
        ]);
    });

    // Upazila Crud operation routes
    Route::group(['prefix'=>'/upazilla'], function(){
        Route::get('/list', 'OrgProfile\UpazilaController@index');
        Route::post('/store', 'OrgProfile\UpazilaController@store');
        Route::put('/update/{id}', 'OrgProfile\UpazilaController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\UpazilaController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\UpazilaController@destroy');
    });

    //Country crud operation routes
    Route::group(['prefix'=>'/country'], function(){
        Route::get('/list', 'OrgProfile\CountryController@index');
        Route::post('/store', 'OrgProfile\CountryController@store');
        Route::put('/update/{id}', 'OrgProfile\CountryController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\CountryController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\CountryController@destroy');
    });

    //Division crud operation routes
    Route::group(['prefix'=>'/division'], function(){
        Route::get('/list', 'OrgProfile\DivisionController@index');
        Route::post('/store', 'OrgProfile\DivisionController@store');
        Route::put('/update/{id}', 'OrgProfile\DivisionController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\DivisionController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\DivisionController@destroy');
        Route::get('/show/{id}', 'OrgProfile\DivisionController@show');
    });

    //District crud operation routes
    Route::group(['prefix'=>'/district'], function(){
        Route::get('/list', 'OrgProfile\DistrictController@index');
        Route::get('/list-all', 'OrgProfile\DistrictController@listAll');
        Route::post('/store', 'OrgProfile\DistrictController@store');
        Route::put('/update/{id}', 'OrgProfile\DistrictController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\DistrictController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\DistrictController@destroy');
        Route::get('/show/{id}', 'OrgProfile\DistrictController@show');
    });


    // Union Crud operation routes
    Route::group(['prefix'=>'/union'], function(){
        Route::get('/list', 'OrgProfile\UnionController@index');
        Route::post('/store', 'OrgProfile\UnionController@store');
        Route::put('/update/{id}', 'OrgProfile\UnionController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\UnionController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\UnionController@destroy');
    });

    // Component Crud operation routes
    Route::group(['prefix'=>'/component'], function(){
        Route::get('/list', 'OrgProfile\MasterComponentController@index');
        Route::get('/details/list', 'OrgProfile\MasterComponentController@getlist');
        Route::post('/store', 'OrgProfile\MasterComponentController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterComponentController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterComponentController@toggleStatus');
        Route::get('/org-wise-component', 'OrgProfile\MasterComponentController@orgWiseComponent');
        // Route::delete('/destroy/{id}', 'OrgProfile\MasterComponentController@destroy');
    });

    // Module Crud operation routes
    Route::group(['prefix'=>'/module'], function(){
        Route::get('/list', 'OrgProfile\MasterModuleController@index');
        Route::get('/details/list', 'OrgProfile\MasterModuleController@getlist');
        Route::post('/store', 'OrgProfile\MasterModuleController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterModuleController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterModuleController@toggleStatus');
        // Route::delete('/destroy/{id}', 'OrgProfile\MasterModuleController@destroy');
    });

    // Service Crud operation routes
    Route::group(['prefix'=>'/service'], function(){
        Route::get('/list', 'OrgProfile\MasterServiceController@index');
        Route::post('/store', 'OrgProfile\MasterServiceController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterServiceController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterServiceController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterServiceController@destroy');
    });

    // Master Office Types Crud operation routes
    Route::group(['prefix'=>'/master-office-types'], function(){
        Route::get('/list', 'OrgProfile\MasterOfficeTypeController@index');
        Route::post('/store', 'OrgProfile\MasterOfficeTypeController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterOfficeTypeController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterOfficeTypeController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterOfficeTypeController@destroy');
        Route::get('/warehouse-office-type-id', 'OrgProfile\MasterOfficeTypeController@getWraehouseOfficeType');
    });

    // Master Organization Profile Crud operation routes
    Route::group(['prefix'=>'/master-org-profile'], function(){
        Route::get('/list', 'OrgProfile\MasterOrganizationProfileController@index');
        Route::post('/store', 'OrgProfile\MasterOrganizationProfileController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterOrganizationProfileController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterOrganizationProfileController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterOrganizationProfileController@destroy');
    });

    // Master Office Crud operation routes
    Route::group(['prefix'=>'/master-office'], function(){
        Route::get('/organogram', 'OrgProfile\MasterOfficeController@organogram');
        Route::get('/office-wise-people', 'OrgProfile\MasterOfficeController@officeWisePeople');

        Route::get('/list', 'OrgProfile\MasterOfficeController@index');
        Route::post('/store', 'OrgProfile\MasterOfficeController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterOfficeController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterOfficeController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterOfficeController@destroy');
        Route::get('/parent-office', 'OrgProfile\MasterOfficeController@parentOffice');
        Route::get('/region', 'OrgProfile\MasterOfficeController@region');
        Route::get('/region-wise-division-or-district/{type}', 'OrgProfile\MasterOfficeController@regionWiseDivisionOrDistrict');
        Route::post('/create-update', 'OrgProfile\MasterOfficeController@createOrUpdate');
    });

    // Master Menu Crud operation routes
    Route::group(['prefix'=>'/master-menu'], function(){
        Route::post('/change-serial-order/{model}', 'OrgProfile\MasterMenuController@changeSerialOrder');

        Route::get('/all-menu-list', 'OrgProfile\MasterMenuController@allMenus');
        Route::get('/sidebar-menus/{roleId}', 'OrgProfile\MasterMenuController@menusByRole');

        Route::get('/list', 'OrgProfile\MasterMenuController@index');
        Route::post('/store', 'OrgProfile\MasterMenuController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterMenuController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterMenuController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterMenuController@destroy');
    });

    // Master Designation Crud operation routes
    Route::group(['prefix'=>'/master-designation'], function(){
        Route::get('/list', 'Organogram\MasterDesignationController@index');
        Route::get('/list-all', 'Organogram\MasterDesignationController@listAll');
        Route::post('/store', 'Organogram\MasterDesignationController@store');
        Route::put('/update/{id}', 'Organogram\MasterDesignationController@update');
        Route::delete('/toggle-status/{id}', 'Organogram\MasterDesignationController@toggleStatus');
        Route::delete('/destroy/{id}', 'Organogram\MasterDesignationController@destroy');
    });

    // Assign Designation Crud operation routes list
    Route::group(['prefix'=>'/assign-designation'], function(){
        Route::get('/list', 'Organogram\AssignDesignationController@index');
        Route::get('/list-all', 'Organogram\AssignDesignationController@listAll');
        Route::post('/store', 'Organogram\AssignDesignationController@store');
        Route::put('/update/{id}', 'Organogram\AssignDesignationController@update');
        Route::delete('/toggle-status/{id}', 'Organogram\AssignDesignationController@toggleStatus');
        Route::delete('/destroy/{id}', 'Organogram\AssignDesignationController@destroy');
    });

    Route::group(['prefix'=>'/fiscal'], function(){
        Route::get('/list', 'OrgProfile\FiscalController@index');
        Route::post('/store', 'OrgProfile\FiscalController@store');
        Route::put('/update/{id}', 'OrgProfile\FiscalController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\FiscalController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\FiscalController@destroy');
        Route::get('/show/{id}', 'OrgProfile\FiscalController@show');
    });

    // Master Complain Type Crud operation routes
    Route::group(['prefix'=>'/master-complain-type'], function(){
        Route::get('/list', 'Complain\MasterComplainTypeController@index');
        Route::post('/store', 'Complain\MasterComplainTypeController@store');
        Route::put('/update/{id}', 'Complain\MasterComplainTypeController@update');
        Route::delete('/toggle-status/{id}', 'Complain\MasterComplainTypeController@toggleStatus');
        Route::delete('/destroy/{id}', 'Complain\MasterComplainTypeController@destroy');
    });

    // Master Notification Type Crud operation routes
    Route::group(['prefix'=>'/master-notification-type'], function(){
        Route::get('/list', 'Notification\MasterNotificationController@index');
        Route::post('/store', 'Notification\MasterNotificationController@store');
        Route::put('/update/{id}', 'Notification\MasterNotificationController@update');
        Route::delete('/toggle-status/{id}', 'Notification\MasterNotificationController@toggleStatus');
        Route::delete('/destroy/{id}', 'Notification\MasterNotificationController@destroy');
    });

    // Master Notification Setting Crud operation routes
    Route::group(['prefix'=>'/master-notification-setting'], function(){
        Route::get('/list', 'Notification\MasterNotificationSettingController@index');
        Route::post('/store', 'Notification\MasterNotificationSettingController@store');
        Route::put('/update/{id}', 'Notification\MasterNotificationSettingController@update');
        Route::delete('/toggle-status/{id}', 'Notification\MasterNotificationSettingController@toggleStatus');
        Route::delete('/destroy/{id}', 'Notification\MasterNotificationSettingController@destroy');
    });

    // Notification template  Crud operation routes
    Route::group(['prefix'=>'/notification-template'], function(){
        Route::get('/list', 'Notification\NotNotificationTemplateController@index');
        Route::post('/store', 'Notification\NotNotificationTemplateController@store');
        Route::put('/update/{id}', 'Notification\NotNotificationTemplateController@update');
        Route::delete('/toggle-status/{id}', 'Notification\NotNotificationTemplateController@toggleStatus');
        Route::delete('/destroy/{id}', 'Notification\NotNotificationTemplateController@destroy');
    });

    // Master Complain Designation Crud operation routes
    Route::group(['prefix'=>'/master-complain-designation'], function(){
        Route::get('/list', 'Complain\MasterComplainDesignationController@index');
        Route::post('/store', 'Complain\MasterComplainDesignationController@store');
        Route::put('/update/{id}', 'Complain\MasterComplainDesignationController@update');
        Route::delete('/toggle-status/{id}', 'Complain\MasterComplainDesignationController@toggleStatus');
        Route::delete('/destroy/{id}', 'Complain\MasterComplainDesignationController@destroy');
    });

    // Master Complain Info Crud operation routes
    Route::group(['prefix'=>'/complain-info'], function(){
        Route::get('/list', 'Complain\ComplainInfoController@index');
        Route::post('/store', 'Complain\ComplainInfoController@store');
        Route::put('/update/{id}', 'Complain\ComplainInfoController@update');
        Route::delete('/toggle-status/{id}', 'Complain\ComplainInfoController@toggleStatus');
        Route::delete('/solve-status/{id}', 'Complain\ComplainInfoController@solveStatus');
        Route::delete('/destroy/{id}', 'Complain\ComplainInfoController@destroy');
    });

    // Complain Send Crud operation routes
    Route::group(['prefix'=>'/complain-send'], function(){
        Route::get('/list', 'Complain\ComplainSendController@index');
        Route::get('/detailList/{complain_id}', 'Complain\ComplainSendController@detailList');
        Route::post('/store', 'Complain\ComplainSendController@store');
        Route::put('/update/{id}', 'Complain\ComplainSendController@update');
        Route::delete('/toggle-status/{id}', 'Complain\ComplainSendController@toggleStatus');
        Route::delete('/destroy/{id}', 'Complain\ComplainSendController@destroy');
    });

    // Master Complain Chain Crud operation routes
    Route::group(['prefix'=>'/master-complain-chain'], function(){
        Route::get('/list', 'Complain\MasterComplainChainController@index');
        Route::post('/store', 'Complain\MasterComplainChainController@store');
        Route::put('/update/{id}', 'Complain\MasterComplainChainController@update');
        Route::delete('/toggle-status/{id}', 'Complain\MasterComplainChainController@toggleStatus');
        Route::delete('/destroy/{id}', 'Complain\MasterComplainChainController@destroy');
    });


    // Notification circulate notice  Crud operation routes
    Route::group(['prefix'=>'/notification-circulate-notice'], function(){
        Route::get('/list', 'Notification\NotCirculateNoticeController@index');
        Route::post('/store', 'Notification\NotCirculateNoticeController@store');
        Route::put('/update/{id}', 'Notification\NotCirculateNoticeController@update');
        Route::delete('/toggle-status/{id}', 'Notification\NotCirculateNoticeController@toggleStatus');
        Route::delete('/destroy/{id}', 'Notification\NotCirculateNoticeController@destroy');
    });


    // Complain manage operation routes
    Route::group(['prefix'=>'/complain-manage'], function(){
        Route::get('/complain-request', 'Complain\ComplainManageController@complainRequest');
        Route::post('/assign-committee', 'Complain\ComplainManageController@assignCommittee');
        Route::get('/get-committee/{complain_id}', 'Complain\ComplainManageController@getCommittee');
        Route::post('/report-submit', 'Complain\ComplainManageController@reportSubmit');
        Route::get('/get-committee-report/{complain_id}', 'Complain\ComplainManageController@getCommitteeReport');
        Route::post('/complain-approve/{id}', 'Complain\ComplainManageController@complainApprove');
        Route::post('/complain-reject/{id}', 'Complain\ComplainManageController@complainReject');
    });

    // Document Category  Crud operation routes
    Route::group(['prefix'=>'/document-category'], function(){
        Route::get('/list', 'Document\MasterDocumentCategoryController@index');
        Route::post('/store', 'Document\MasterDocumentCategoryController@store');
        Route::put('/update/{id}', 'Document\MasterDocumentCategoryController@update');
        Route::delete('/toggle-status/{id}', 'Document\MasterDocumentCategoryController@toggleStatus');
        Route::delete('/destroy/{id}', 'Document\MasterDocumentCategoryController@destroy');
    });

    // Add Document  Crud operation routes
    Route::group(['prefix'=>'/document'], function(){
        Route::get('/list', 'Document\AddDocumentController@index');
        Route::post('/store', 'Document\AddDocumentController@store');
        Route::put('/update/{id}', 'Document\AddDocumentController@update');
        Route::delete('/toggle-status/{id}', 'Document\AddDocumentController@toggleStatus');
        Route::delete('/archive-toggle-status/{id}', 'Document\AddDocumentController@archivetoggleStatus');
        Route::delete('/destroy/{id}', 'Document\AddDocumentController@destroy');
    });

    // Document Indexing  operation routes
      Route::group(['prefix'=>'/document-indexing'], function(){
        Route::get('/list', 'Document\DocumentIndexingController@index');
        Route::get('/show', 'Document\DocumentIndexingController@getDocumentData');

    });
     // Document Archived operation routes
     Route::group(['prefix'=>'/document-archived'], function(){
        Route::get('/list', 'Document\DocumentArchiedController@index');
    });

    // Master Bank Crud operation routes
    Route::group(['prefix'=>'/master-bank'], function(){
        Route::get('/list', 'OrgProfile\MasterBankController@index');
        Route::get('/list-all', 'OrgProfile\MasterBankController@listAll');
        Route::post('/store', 'OrgProfile\MasterBankController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterBankController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterBankController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterBankController@destroy');
    });

    // Master Branch Crud operation routes
    Route::group(['prefix'=>'/master-branch'], function(){
        Route::get('/list', 'OrgProfile\MasterBranchController@index');
        Route::post('/store', 'OrgProfile\MasterBranchController@store');
        Route::put('/update/{id}', 'OrgProfile\MasterBranchController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\MasterBranchController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\MasterBranchController@destroy');
    });

     // Create Committee routes
     Route::group(['prefix'=>'/committee-create'], function(){
        Route::get('/list', 'Committee\CreateCommitteeController@index');
        Route::get('/all-list', 'Committee\CreateCommitteeController@allIndex');
        Route::get('/getlist', 'Committee\CreateCommitteeController@getindex');
        Route::post('/store', 'Committee\CreateCommitteeController@store');
        Route::put('/update/{id}', 'Committee\CreateCommitteeController@update');
        Route::delete('/toggle-status/{id}', 'Committee\CreateCommitteeController@toggleStatus');
        Route::delete('/destroy/{id}', 'Committee\CreateCommitteeController@destroy');
        Route::delete('/commiteeMemberDelete/{id}','Committee\CreateCommitteeController@CommiteeMemberDelete');
        Route::get('/CommiteeMemberlist/{id}','Committee\CreateCommitteeController@getCommitteemember');
    });

     //  Committee  Agenda routes
     Route::group(['prefix'=>'/committee-agenda'], function(){
        Route::get('/list', 'Committee\CommitteeAgendaController@index');
        Route::get('/all-list', 'Committee\CommitteeAgendaController@allList');
        Route::post('/store', 'Committee\CommitteeAgendaController@store');
        Route::put('/update/{id}', 'Committee\CommitteeAgendaController@update');
        Route::delete('/toggle-status/{id}', 'Committee\CommitteeAgendaController@toggleStatus');
        Route::delete('/destroy/{id}', 'Committee\CommitteeAgendaController@destroy');
    });

    //  Committee  Metting minute routes
    Route::group(['prefix'=>'/committee-metting-minute'], function(){
        Route::get('/list', 'Committee\CommitteeMettingController@index');
        Route::post('/store', 'Committee\CommitteeMettingController@store');
        Route::put('/update/{id}', 'Committee\CommitteeMettingController@update');
        Route::delete('/toggle-status/{id}', 'Committee\CommitteeMettingController@toggleStatus');
        Route::delete('/destroy/{id}', 'Committee\CommitteeMettingController@destroy');
    });

     //  Committee  Expenses  routes
     Route::group(['prefix'=>'/committee-expenses'], function(){
        Route::get('/list', 'Committee\CommitteeExpensesController@index');
        Route::post('/store', 'Committee\CommitteeExpensesController@store');
        Route::put('/update/{id}', 'Committee\CommitteeExpensesController@update');
        Route::delete('/toggle-status/{id}', 'Committee\CommitteeExpensesController@toggleStatus');
        Route::delete('/destroy/{id}', 'Committee\CommitteeExpensesController@destroy');
    });

    // Dialogue Info crud routes
    Route::group(['prefix'=>'/master-dialogue-info-settings'], function(){
        Route::get('/list', 'OrgProfile\DialogueInfoController@index');
        Route::post('/store', 'OrgProfile\DialogueInfoController@store');
        Route::put('/update/{id}', 'OrgProfile\DialogueInfoController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\DialogueInfoController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\DialogueInfoController@destroy');
    });


    // Master Fiscal Year routes
    Route::group(['prefix'=>'/master-fiscal-year'], function(){
        Route::get('/list', 'OrgProfile\FiscalYearController@index');
        Route::get('/list-all', 'OrgProfile\FiscalYearController@listAll');
        Route::post('/store', 'OrgProfile\FiscalYearController@store');
        Route::put('/update/{id}', 'OrgProfile\FiscalYearController@update');
        Route::delete('/toggle-status/{id}', 'OrgProfile\FiscalYearController@toggleStatus');
        Route::delete('/destroy/{id}', 'OrgProfile\FiscalYearController@destroy');
        Route::get('/show/{id}', 'OrgProfile\FiscalYearController@show');
        Route::get('/prevFascalYear/{id}', 'OrgProfile\FiscalYearController@prevFascalYear');
        Route::get('/get-fiscal-year-by-date', 'OrgProfile\FiscalYearController@getFiscalYearByDate');
    });

});

//Common dropdown routes
Route::group(['prefix'=>'/common'], function(){
    Route::get('/country-list', 'CommonDropdownController@countryList');
    Route::get('/union-list', 'CommonDropdownController@unionList');
    Route::get('/office-type-list', 'CommonDropdownController@officeTypeList');
    Route::get('/org-and-org-component-list', 'CommonDropdownController@orgAndOrgComponentList');

    Route::get('/office-list', 'CommonDropdownController@officeList');
    Route::get('/component-list', 'CommonDropdownController@componentList');
    Route::get('/module-list', 'CommonDropdownController@moduleList');
    Route::get('/service-list', 'CommonDropdownController@serviceList');
    Route::get('/menu-list', 'CommonDropdownController@menuList');
    Route::get('/designation-list', 'CommonDropdownController@designationList');
    Route::get('/assign-designation-list', 'CommonDropdownController@assignDesignationList');
    Route::get('/notification-types', 'CommonDropdownController@notificationtypeList');
    Route::get('/bank-list', 'CommonDropdownController@banklist');
    Route::get('/branch-list', 'CommonDropdownController@branchlist');

});

Route::get('agri-dialogue', 'OrgProfile\DialogueInfoController@agriDialogue');
Route::post('division-district-matching', 'DivisionCheckController');


 Route::group(['prefix'=>'/log-report'], function(){
    Route::get('/list', 'LogReport\LogReportController@index');
});

//Service eligibility type routes
Route::group(['prefix'=>'/service-eligibility-type'], function(){
    Route::get('/list', 'InfoServiceManage\ServiceEligibilityTypeEntryController@index');
    Route::post('/store', 'InfoServiceManage\ServiceEligibilityTypeEntryController@store');
    Route::put('/update/{id}', 'InfoServiceManage\ServiceEligibilityTypeEntryController@update');
    Route::delete('/toggle-status/{id}', 'InfoServiceManage\ServiceEligibilityTypeEntryController@toggleStatus');
    Route::delete('/destroy/{id}', 'InfoServiceManage\ServiceEligibilityTypeEntryController@destroy');
});

//Manage Faq routes
Route::group(['prefix'=>'/faq'], function(){
    Route::get('/list', 'InfoServiceManage\ManageFaqController@index');
    Route::post('/store', 'InfoServiceManage\ManageFaqController@store');
    Route::put('/update/{id}', 'InfoServiceManage\ManageFaqController@updateSingle');
    Route::delete('/toggle-status/{id}', 'InfoServiceManage\ManageFaqController@toggleStatus');
    Route::delete('/destroy/{id}', 'InfoServiceManage\ManageFaqController@destroy');
});
//Content entry routes
Route::group(['prefix'=>'/master-contents'], function(){
    Route::get('/list', 'InfoServiceManage\ContentEntryController@index');
    Route::post('/store', 'InfoServiceManage\ContentEntryController@store');
    Route::put('/update/{id}', 'InfoServiceManage\ContentEntryController@update');
    Route::delete('/toggle-status/{id}', 'InfoServiceManage\ContentEntryController@toggleStatus');
    Route::delete('/destroy/{id}', 'InfoServiceManage\ContentEntryController@destroy');
});


//Payment service entry crud routes
Route::group(['prefix'=>'/payment-service-entry', 'namespace' => 'PaymentManagement'], function(){
    Route::get('/list', 'PaymentServiceEntryController@index');
    Route::post('/store', 'PaymentServiceEntryController@store');
    Route::put('/update/{id}', 'PaymentServiceEntryController@update');
    Route::delete('/toggle-status/{id}', 'PaymentServiceEntryController@toggleStatus');
    Route::delete('/destroy/{id}', 'PaymentServiceEntryController@destroy');
});

//Master pauroshoba crud routes
Route::group(['prefix'=>'/master-pauroshoba', 'namespace' => 'Pauroshoba'], function(){
    Route::get('/list', 'MasterPauroshobasController@index');
    Route::get('/details/{id}', 'MasterPauroshobasController@show');
    Route::post('/store', 'MasterPauroshobasController@store');
    Route::put('/update/{id}', 'MasterPauroshobasController@update');
    Route::delete('/toggle-status/{id}', 'MasterPauroshobasController@toggleStatus');
    Route::delete('/destroy/{id}', 'MasterPauroshobasController@destroy');
});

//Master city corporation crud routes
Route::group(['prefix'=>'/master-city-corporation', 'namespace' => 'CityCorporation'], function(){
    Route::get('/list', 'MasterCityCorporationsController@index');
    Route::get('/details/{id}', 'MasterCityCorporationsController@show');
    Route::post('/store', 'MasterCityCorporationsController@store');
    Route::put('/update/{id}', 'MasterCityCorporationsController@update');
    Route::delete('/toggle-status/{id}', 'MasterCityCorporationsController@toggleStatus');
    Route::delete('/destroy/{id}', 'MasterCityCorporationsController@destroy');
});


//Master Wards crud routes
Route::group(['prefix'=>'/master-wards', 'namespace' => 'Wards'], function(){
    Route::get('/list', 'MasterWardsController@index');
    Route::get('/details/{id}', 'MasterWardsController@show');
    Route::post('/store', 'MasterWardsController@store');
    Route::put('/update/{id}', 'MasterWardsController@update');
    Route::delete('/toggle-status/{id}', 'MasterWardsController@toggleStatus');
    Route::delete('/destroy/{id}', 'MasterWardsController@destroy');
});

//Auth User access
Route::group(['prefix'=>'/access-control'], function() {
    Route::get('/components-from-menu', 'AccessControlController@componentsOfSuperAdmin');
    Route::get('/sidebar-menus/{roleId}/{componentId}', 'AccessControlController@menusByRoleComponent');
});

